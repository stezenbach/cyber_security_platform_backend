﻿//
//  Challenges.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using ServiceStack;
using System.Collections.Generic;
using Hackathon.ServiceModel.Types;

namespace Hackathon.ServiceModel
{
	[Route ("/challenges/{id}", "GET")]
	public class FindChallenges : IReturn<FindChallengeResponse>
	{
		public int Id { get; set; }
	}

	public class FindChallengeResponse
	{
		public int Id { get; set; }

		public string Title { get; set; }

		public int BasePoints { get; set; }

		public SolutionType SolutionType { get; set; }

		/// <summary>
		/// May contain JSON encoded information about the solution, depending on the SolutionType
		/// </summary>
		/// <value>
		/// SolutionType.Static: empty
		/// SolutionType.MultipleChoice: JSON encoded List<string> containing the text of the choices
		/// SolutionType.MultiPart: JSON encoded information about the question (the backend doesn't care about the format)
		/// </value>
		public String SolutionInfo { get; set; }
	}

	[Route ("/challenges/{id}/description", "GET")]
	public class GetChallengeDescription : IReturn<GetChallengeDescriptionResponse>
	{
		public int Id { get; set; }
	}

	[Route ("/challenges/{id}/solution", "GET")]
	public class GetChallengeSolution : IReturn<GetChallengeSolutionResponse>
	{
		public int Id { get; set; }
	}

	public class GetChallengeSolutionResponse
	{
		public SolutionType SolutionType { get; set; }

		public string Solution { get; set; }

		public string SolutionExplanation { get; set; }
	}

	[Route ("/challenges/{id}/solution", "POST")]
	public class PostSolution : IReturnVoid
	{
		public int Id { get; set; }

		public SolutionType SolutionType { get; set; }

		/// <summary>
		/// The correct solution. The format depends on the SolutionType.
		/// </summary>
		/// <value>
		/// SolutionType.Static: plain string
		/// SolutionType.MultipleChoice: JSON encoded List<bool>
		/// SolutionType.MultiPart: JSON encoded Dictionary<string, string> (key: question identifier, values: answer)
		/// </value>
		public string Solution { get; set; }
	}

	[Route ("/challenges/{id}/solution/entered", "GET")]
	public class GetChallengeSolutionEntered : IReturn<GetChallengeSolutionEnteredResponse>
	{
		public int Id { get; set; }
	}

	public class GetChallengeSolutionEnteredResponse
	{
		public SolutionType SolutionType { get; set; }

		public String SolutionEntered { get; set; }
	}


	[Route ("/challenges/{id}/solution/mayenter", "GET")]
	public class GetChallengeSolutionMayEnter : IReturn<GetChallengeSolutionMayEnterResponse>
	{
		public int Id { get; set; }
	}

	public class GetChallengeSolutionMayEnterResponse 
	{
		public bool MayEnterSolution {get; set;}
		public bool Solved {get; set;}
		public bool Locked {get; set;}
	}

	[Route ("/challenges/{id}/hints", "GET")]
	public class GetChallengeHints : IReturn<List<int>>
	{
		public int Id { get; set; }
	}

	[Route ("/challenges/{id}/hints/{hintnumber}", "GET")]
	public class GetChallengeHint : IReturn<GetChallengeHintResponse>
	{
		public int Id { get; set; }

		public int HintNumber { get; set; }
	}

	[Route ("/challenges/{id}/hints/{hintnumber}/pointreduction", "GET")]
	public class GetHintPointReduction : IReturn<PointResponse>
	{
		public int Id { get; set; }

		public int HintNumber { get; set; }
	}

	public class PointResponse
	{
		public int Points { get; set; }
	}

	public class GetChallengeDescriptionResponse
	{
		public string Description { get; set; }
	}

	public class GetChallengeHintResponse
	{
		public int HintNumber { get; set; }

		public string Text { get; set; }
	}
}

