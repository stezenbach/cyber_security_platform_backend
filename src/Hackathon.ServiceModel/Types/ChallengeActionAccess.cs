﻿//
//  ChallengeActionAccess.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2017 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using ServiceStack.DataAnnotations;

namespace Hackathon.ServiceModel.Types
{
	public class ChallengeActionAccess
	{
		[AutoIncrement]
		[PrimaryKey]
		public int Id { get; set; }

		[Description ("References a ChallengeAction object")]
		[References (typeof(ChallengeAction))]
		[Required ()]
		public int ChallengeActionId { get; set; }

		[Description ("References a User object")]
		[References (typeof(User))]
		[Required ()]
		public int UserId { get; set; }

		[Required ()]
		public DateTime AccessTime { get; set; }

		[Required ()]
		[Index (Unique = true)]
		public UInt64 Nonce { get; set; }
	}
}

