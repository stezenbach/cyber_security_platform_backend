﻿//
//  Challenge.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using ServiceStack.DataAnnotations;
using System.Collections.Generic;
using ServiceStack.OrmLite.MySql.DataAnnotations;

namespace Hackathon.ServiceModel.Types
{

	/// <summary>
	/// Specifies the type of solution a challenge has
	/// </summary>
	public enum SolutionType
	{
		/// <summary>
		/// The solution of a challenge is some fixed text
		/// </summary>
		Static = 1,
		/// <summary>
		/// The solution of a multiple choice question
		/// </summary>
		MultipleChoice,
		/// <summary>
		/// A solution which consists of multiple free text fields
		/// </summary>
		MultiPart,
	}

	public class Challenge
	{
		[AutoIncrement]
		public int Id { get; set; }

		public string Title { get; set; }

		[Text]
		public string Description { get; set; }

		[Default ((int)SolutionType.Static)]
		public SolutionType SolutionType { get; set; }

		/// <summary>
		/// May contain JSON encoded information about the solution, depending on the SolutionType
		/// </summary>
		/// <value>
		/// SolutionType.Static: empty
		/// SolutionType.MultipleChoice: JSON encoded List<string> containing the text of the choices
		/// SolutionType.MultiPart: JSON encoded information about the question (the backend doesn't care about the format)
		/// </value>
		[Text]
		public String SolutionInfo { get; set; }

		/// <summary>
		/// The correct solution. The format depends on the SolutionType.
		/// </summary>
		/// <value>
		/// SolutionType.Static: plain string
		/// SolutionType.MultipleChoice: JSON encoded List<bool>
		/// SolutionType.MultiPart: JSON encoded Dictionary<string, string> (key: question identifier, values: answer)
		/// </value>
		public string Solution { get; set; }

		/// <summary>
		/// Some text explaining the correct solution
		/// </summary>
		[Text]
		public string SolutionExplanation { get; set; }

		public List<int> Attachments { get; set; }

		[Default (0)]
		public int BasePoints { get; set; }
	}
}

