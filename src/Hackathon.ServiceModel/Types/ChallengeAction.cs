﻿//
//  ChallengeAction.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2017 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using ServiceStack.DataAnnotations;
using System.Collections.Generic;
using ServiceStack.OrmLite.MySql.DataAnnotations;

namespace Hackathon.ServiceModel.Types
{
	public class ChallengeAction
	{
		[AutoIncrement]
		[PrimaryKey]
		public int Id { get; set; }

		[Description ("References a Challenge object")]
		[References (typeof(Challenge))]
		public int ChallengeId { get; set; }

		[Description ("A BASE64 encoded key used to authenticate this action")]
		public string Key { get; set; }

		[Description ("A text displayed to the user describing the cause of this action")]
		[Text]
		public string Description { get; set; }

		[Description ("Wether to lock the challenge (prevent further solution attempts) for the user")]
		public bool LockChallegenge { get; set; }

		[Description ("Wether to remove any (speed) bonus from the challenge for the user")]
		public bool RemoveBonus { get; set; }

		[Required ()]
		[Description ("Percentage value which will be deducted from a challenge's base points if this action is used. A value of '50' means 50%")]
		[Range (0, 100)]
		public int PointReduction { get; set; }
	}
}

