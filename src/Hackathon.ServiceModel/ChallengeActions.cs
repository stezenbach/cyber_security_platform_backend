﻿//
//  ChallengeActions.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2017 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using ServiceStack;
using System.Collections.Generic;

namespace Hackathon.ServiceModel
{
	[Route ("/users/{id}/challenges/{challengeId}/actions/{actionId}", "POST")]
	public class PostChallengeActions : IReturnVoid
	{
		public int Id { get; set; }

		public int ChallengeId { get; set; }

		public int ActionId { get; set; }

		public UInt64 Nonce { get; set; }

		/// <summary>
		/// Contains an authentication code for the action request authenticated by the key stored in the DB for this action
		/// </summary>
		/// <value>The value needed here ist BASE64(HMAC-SHA256(Key, {"UserId", "ChallengeId", "ActionId", "Nonce"}))</value>
		public string Mac { get; set; }
	}

	[Route ("/user/challenges/{challengeId}/actions/", "GET")]
	public class GetChallengeActions : IReturn<GetChallengeActionsResponse>
	{
		public int ChallengeId { get; set; }
	}

	public class GetChallengeActionsResponse
	{
		public class ChallengeActionInformation 
		{
			public string Description {get; set;}
			public bool LockChallenge {get; set;}
			public string AccessTime {get; set;}
			public bool RemoveBonus {get; set;}
			public int PointReduction {get; set;}
		}

		public List<ChallengeActionInformation> Actions { get; set;}
	}


}

