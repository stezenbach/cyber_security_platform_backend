﻿//
//  Multipart.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2017 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Hackathon.ServiceInterface.Points
{
	public static class MultiPart
	{
		/// <summary>
		/// Normalizes the answer.
		/// Remove whitespaces at the beginning and the end, turn to lower case
		/// Replace all special characters (non [a-zA-Z0-9]) with a space
		/// </summary>
		/// <returns>The normalized answer</returns>
		/// <param name="answer">An answer</param>
		public static string NormalizeAnswer (string answer)
		{
			answer = answer.Trim ().ToLower ();
			answer = Regex.Replace (answer, @"[^a-z0-9]", " ");
			return answer;				
		}

		/// <summary>
		/// Validates, if proposed answers can be compared to a solution
		/// The correct number of answers has to be present and the solution identifiers (keys of the dictionaries) have to match
		/// </summary>
		/// <returns><c>true</c>, if answer was possibled, <c>false</c> otherwise.</returns>
		/// <param name="answers">Answers.</param>
		/// <param name="solution">Solution.</param>
		/// <param name="maxPoints">Max points.</param>
		public static bool PossibleAnswer (Dictionary<string,string> answers, Dictionary<string,string> solution)
		{
			if (answers == null || solution == null)
				throw new ArgumentNullException ("Parameters must not be null");
			
			if (answers.Count () != solution.Count ())
				return false;

			//Check if the solution identifiers are the same
			foreach (string solutionIdentifier in solution.Keys)
				if (!answers.ContainsKey (solutionIdentifier))
					return false;
			//All checks passed
			return true;			
		}

		/// <summary>
		/// Calculate points for a multipart question
		/// Each answer is worth an equal share of the total points, each correct answer is awarded with that number of points
		/// Wrong answers are not penalized
		/// Differences in casing and white spaces at the beginning and end do not matter, All special characters (non [a-zA-Z0-9]) are replaced by a space
		/// </summary>
		/// <param name="answers">The answers provided by the user</param>
		/// <param name="solution">The correct answers</param>
		/// <param name="maxPoints">The total number of points available for this questions</param>
		public static int CalculatePoints (Dictionary<string,string> answers, Dictionary<string,string> solution, int maxPoints)
		{
			if (maxPoints <= 0)
				throw new ArgumentException ("Total points must be positive", "maxPoints");

			if (answers == null || solution == null)
				throw new ArgumentNullException ("Parameters must not be null");

			if (!PossibleAnswer (answers, solution))
				throw new ArgumentException ("The solution and the answers are not from the same question.");

			double points = 0;
			double pointsPerAnswer = (double)maxPoints / answers.Count ();
			foreach (string solutionIdentifier in solution.Keys) {
				string answer = NormalizeAnswer (answers [solutionIdentifier]);
				string expectedAnswer = NormalizeAnswer (solution [solutionIdentifier]);
				if (answer == expectedAnswer)
					points += pointsPerAnswer;
			}
		
			return (int)Math.Round (points);
		}
	}
}

