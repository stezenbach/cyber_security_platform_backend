﻿//
//  DbPoints.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Linq;
using ServiceStack.Data;
using ServiceStack;
using ServiceStack.OrmLite;
using Hackathon.ServiceModel;
using Hackathon.ServiceModel.Types;
using System.Collections.Generic;

namespace Hackathon.ServiceInterface.Points
{
	public class DbPoints : IPoints
	{
		public int GetPoints (Service service, int userId, int challengeId)
		{
			using (var userService = service.ResolveService<UsersService> ()) {
				var userChallenge = userService.Get (new GetUserChallenge () {
					UserId = userId,
					ChallengeId = challengeId
				});

				//First check, if the challenge has even been solved yet
				DateTime solvedDate = DateTime.MinValue;
				if (!userChallenge.SolvedDate.IsNullOrEmpty ())
					solvedDate = userChallenge.SolvedDate.FromEcmaScriptString ();

				DateTime accessDate = DateTime.MinValue;
				if (!userChallenge.AccessDate.IsNullOrEmpty ())
					accessDate = userChallenge.AccessDate.FromEcmaScriptString ();

				// Get all hint IDs from the database which have ben accessed
				var accessedHintIds = service.Db.Select<HintAccess> (h => (
				                          (h.ChallengeId == challengeId) &&
				                          (h.UserId == userId)
				                      ))
					.Where (h => h.AccessTime.ToUniversalTime () < solvedDate.ToUniversalTime ())  // We only want hints, which have been accessed before the challenge was solved. We do this here instead of the db query, because different timezones could lead to wrong results otherwise
					.Select (h => h.HintId);  // We only want the IDs of the hints

				//Get the triggered actions 
				var actionIds = ChallengesService.GetChallengeActionIds (service, challengeId, userId);

				return GetPoints (service, userId, challengeId, solvedDate, accessDate, accessedHintIds, actionIds);
			}
		}

		public int GetPoints (Service service, int userId, int challengeId, DateTime solvedDate, DateTime accessDate, IEnumerable<int> accessedHintIds, IEnumerable<int> usedActionIds)
		{
			/* Point calculation system:
            The Challenge table holds a BasePoints value
            A speed bonus is awarded, if the challenge is solved during the first half of its runtime (full days, rounded up)
            The maximal speed bonus is BasePoints*0.4. After n hours you get (BasePoints*0.4 * (max_days*24-floor(n)))/(max_days*24) points
            The Hint table gets a new column Value, which holds the percentage of BasePoints ('50' for 50%) the hint will subtract from the points
            Using any hints will completely remove the speed bonus
            The points can not be negative
            */
			using (var userService = service.ResolveService<UsersService> ()) {
				if (solvedDate == DateTime.MinValue)
					//Not solved yet -> return 0 points
					return 0;
				
				//Fetch full informations about the challenge from the database
				Challenge challenge = ChallengesService.GetChallengeById (service, challengeId, false);

				//Assign the points for the challenge
				int basePoints = challenge.BasePoints;
				int points = 0;
				if (challenge.SolutionType == SolutionType.Static) {
					//Static solutions do not have any special point calculations. Just assign the base points
					points = basePoints;
				} else if (challenge.SolutionType == SolutionType.MultipleChoice) {
					//multiple choice points are assigned by a seperate calculator
					//we need the solution for that
					var solutionsByUser = service.Db.Select<ChallengeSolution> (cs => cs.ChallengeId == challenge.Id && cs.UserId == userId);
					if (solutionsByUser.Count () == 1) {
						ChallengeSolution solutionByUser = solutionsByUser.First ();
						List<bool> answers = solutionByUser.SolutionEntered.FromJson<List<bool>> ();
						List<bool> solution = challenge.Solution.FromJson<List<bool>> ();
						points = MultipleChoice.CalculatePoints (answers, solution, challenge.BasePoints);
					} else
						// no solution posted yet but solvedDate is not DateTime.MinValue -> we are in a point calculation simulation -> assign the base points
						points = basePoints;
				} else if (challenge.SolutionType == SolutionType.MultiPart) {
					//multipart points are assigned by a seperate calculator
					//we need the solution for that
					var solutionsByUser = service.Db.Select<ChallengeSolution> (cs => cs.ChallengeId == challenge.Id && cs.UserId == userId);
					if (solutionsByUser.Count () == 1) {
						ChallengeSolution solutionByUser = solutionsByUser.First ();
						Dictionary<string,string> answers = solutionByUser.SolutionEntered.FromJson<Dictionary<string,string>> ();
						Dictionary<string,string> solution = challenge.Solution.FromJson<Dictionary<string,string>> ();
						points = MultiPart.CalculatePoints (answers, solution, challenge.BasePoints);
					} else
						// no solution posted yet but solvedDate is not DateTime.MinValue -> we are in a point calculation simulation -> assign the base points
						points = basePoints;
				}

				//If we have zero points at this stage, we can return immediately. No speed bonus should be awarded, no need to calculate any reductions
				if (points == 0)
					return 0;

				//Retrieve information about the challenge from the database
				using (var challengeSetsService = service.ResolveService<ChallengeSetsService> ()) {
					//Find out which challengeset the challenge belongs to
					var challengeSetsWithEvent = challengeSetsService.Get (new FindChallengeSets (){ ChallengeId = challengeId });
					if (!challengeSetsWithEvent.Any ())
						throw HttpError.NotFound ("Challenge is part of any ChallengeSet");
					//Find out which event the challengeset belongs to were the user is a member of that event

					// Get all events ths user is part of
					IEnumerable<int> userEventIds = service.Db.ColumnDistinct<int> (service.Db.From<EventUsers> ().Where (e => (e.UserId == userId)).Select (e => e.EventId));

					int eventId = -1;
					int challengeSetId = -1;
					bool eventFound = false;
					using (var eventsService = service.ResolveService<EventsService> ()) {
						foreach (int challengeSetCandidate in challengeSetsWithEvent) {
							var eventsWithChallengeSet = eventsService.Get (new FindEvents (){ ChallengeSetId = challengeSetCandidate });
							var eventsWithChallengeSetAndUser = eventsWithChallengeSet.Intersect (userEventIds);
							int eventCandidates = eventsWithChallengeSetAndUser.Count ();
							if (eventCandidates == 1) {
								if (eventFound)
									throw HttpError.Conflict ("This challenge is part of multiple events of that user. Cannot calculate the points.");
								else {
									eventId = eventsWithChallengeSetAndUser.First ();
									challengeSetId = challengeSetCandidate;
									eventFound = true;
								}
							} else if (eventCandidates > 1)
								throw HttpError.Conflict ("This challenge is part of multiple events of that user. Cannot calculate the points.");
						}
						if (!eventFound)
							throw HttpError.NotFound ("Challenge is not part of any event of that user.");

						EventChallengeSets challengeSetInEvent = service.Db.Single<EventChallengeSets> (e => (e.EventId == eventId) && (e.ChallengeSetId == challengeSetId));

						//Check if the solution was on time
						if (solvedDate.ToUniversalTime () > challengeSetInEvent.EndDate.ToUniversalTime ())
							//Solution came after the challenge expired
							return 0;

						//calulate the speed bonus (if any)
						TimeSpan maxTime = challengeSetInEvent.EndDate.Subtract (challengeSetInEvent.StartDate);
						int speedBonusDays = (int)Math.Round ((double)(maxTime.Days / 2));
						int speedBonusHours = speedBonusDays * 24;

						TimeSpan solvingTime = new TimeSpan (0);
						if (accessDate != DateTime.MinValue)
							solvingTime = solvedDate.ToUniversalTime ().Subtract (accessDate.ToUniversalTime ());
						int speedBonus = 0;
						if (solvingTime.TotalHours < speedBonusHours) {
							//User is eglible for a speed bonus
							int maxSpeedBonus = (int)(challenge.BasePoints * 0.4);
							speedBonus = (int)(maxSpeedBonus * (speedBonusHours - Math.Floor (solvingTime.TotalHours)) / speedBonusHours);
						}
						points += speedBonus;

						//Points might be reduced due to hints or actions
						bool removeSpeedBonus = false;
						List<int> pointReductions = new List<int> ();

						//Check which hints have been accessed by the user
						if (accessedHintIds != null && accessedHintIds.Any ()) {
							//if hints have been accessed, the speed bonus will be removed
							removeSpeedBonus = true;
							//deduct the defined percentage value for each hint
							foreach (var hintId in accessedHintIds) {
								Hint hint = service.Db.Single<Hint> (h => h.Id == hintId);
								pointReductions.Add (hint.PointReduction);
							}
						}

						//Check which actions have been triggered by the user
						if (usedActionIds != null && usedActionIds.Any ()) {
							List<ChallengeAction> actions = service.Db.Select<ChallengeAction> (ca => Sql.In (ca.Id, usedActionIds));
							foreach (ChallengeAction action in actions) {
								if (action.RemoveBonus)
									removeSpeedBonus = true;
								pointReductions.Add (action.PointReduction);
							}
						}

						if (removeSpeedBonus)
							points -= speedBonus;
						foreach (int pointReduction in pointReductions)
							points -= basePoints * pointReduction / 100;
					}
				}
				//Make sure, that the points are not negative
				if (points < 0)
					points = 0;

				return points;
			}
		}
	}
}

