﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hackathon.ServiceInterface.Points
{
	public static class MultipleChoice
	{
		/// <summary>
		/// Calculate points for a multiple choice question
		/// Each answer is worth an equal share of the total points, each correct answer (correctly ticked or correctly not ticked) is awarded with that number of points
		/// Wrong answers are not penalized
		/// </summary>
		/// <param name="answers">The answers provided by the user</param>
		/// <param name="solution">The correct answers</param>
		/// <param name="maxPoints">The total number of points available for this questions</param>
		public static int CalculatePoints (IEnumerable<bool> answers, IEnumerable<bool> solution, int maxPoints)
		{
			if (maxPoints <= 0)
				throw new ArgumentException ("Total points must be positive", "maxPoints");
			
			if (answers == null || solution == null)
				throw new ArgumentNullException ("Parameters must not be null");

			if (answers.Count () != solution.Count ())
				throw new ArgumentException ("Size of solution and answers must be the same.");

			int answerCount = answers.Count ();

			if (answerCount == 0)
				throw new ArgumentException ("No answers submitted.");
			
			double points = 0;
			double pointsPerAnswer = (double)maxPoints / answerCount;
			for (int i = 0; i < answerCount; ++i) {
				if (answers.ElementAt (i) == solution.ElementAt (i))
					points += pointsPerAnswer;
			}

			return (int)Math.Round (points);
		}
	}
}

