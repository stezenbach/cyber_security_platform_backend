﻿//
//  UsersService.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Linq;
using ServiceStack;
using Hackathon.ServiceModel;
using System.Collections.Generic;
using Hackathon.ServiceModel.Types;
using ServiceStack.OrmLite;
using ServiceStack.Auth;
using Hackathon.ServiceModel.Auth;
using Hackathon.ServiceInterface.Notification;
using Hackathon.ServiceInterface.Points;

namespace Hackathon.ServiceInterface
{
	public class UsersService : Service
	{
		internal static User GetUserById (Service service, int id)
		{
			var users = service.Db.Select<User> ().Where (u => u.Id == id);
			if (!users.Any ())
				throw HttpError.NotFound ("No such user found");
			else if (users.Count () > 1)
				throw HttpError.Conflict ("More than one user with that ID found.");
			else
				return users.First ();
		}

		private User GetUserByMailAddress (string mailAddress)
		{
			var users = Db.Select<User> ().Where (u => u.MailAddress == mailAddress);
			if (!users.Any ())
				throw HttpError.NotFound ("No such user found");
			else if (users.Count () > 1)
				throw HttpError.Conflict ("More than one user with that e-mail address found.");
			else
				return users.First ();
		}

		[Authenticate]
		public GetUserResponse Get (GetUser request)
		{
			int loggedInUserId;
			if (!Int32.TryParse (GetSession ().UserAuthId, out loggedInUserId))
				throw HttpError.Forbidden ("No valid authenticated user found.");

			// Information about the user may only be displayed, if the user shares an event with the logged in user. Information about the logged in user may be displayed, too
			if (loggedInUserId == request.Id)
				return GetUserById (this, request.Id).ConvertTo<GetUserResponse> ();
			
			//Check if the authenticated user shares an event with the requested user
			var eventsOfRequestedUser = Db.Select<EventUsers> ().Where (e => e.UserId == request.Id).Select (e => e.EventId).Distinct ();
			var eventsOfLoggedInUser = Db.Select<EventUsers> ().Where (e => e.UserId == loggedInUserId).Select (e => e.EventId).Distinct ();
			if (eventsOfLoggedInUser.Intersect (eventsOfRequestedUser).Any ()) {
				return GetUserById (this, request.Id).ConvertTo<GetUserResponse> ();
			} else
				throw HttpError.Forbidden ("You are not authorized to access this user");
		}

		[Authenticate]
		public GetUserResponse Get (GetLoggedInUser request)
		{
			int loggedInUserId;
			if (!Int32.TryParse (GetSession ().UserAuthId, out loggedInUserId))
				throw HttpError.Forbidden ("No valid authenticated user found.");

			//TODO: We could just redirect to /users/{loggedInUserId} here. Not sure how to do this, we return the same data, at least.
			return Get (new GetUser{ Id = loggedInUserId });
		}

		[Authenticate]
		public IList<int> Get (GetLoggedInUserEvents request)
		{
			int loggedInUserId;
			if (!Int32.TryParse (GetSession ().UserAuthId, out loggedInUserId))
				throw HttpError.Forbidden ("No valid authenticated user found.");
			
			return Db.Select<EventUsers> ().Where (e => e.UserId == loggedInUserId).Select (e => e.EventId).Distinct ().ToList ();
		}

		[Authenticate]
		public void Post (ChangePassword request)
		{
			var session = GetSession ();
			int loggedInUserId;
			if (!Int32.TryParse (session.UserAuthId, out loggedInUserId))
				throw HttpError.Forbidden ("No valid authenticated user found.");
			
			//Check if the old password is valid
			var authProvider = new DbAuthProvider ();
			if (!authProvider.TryAuthenticate (this, session.UserAuthName, request.OldPassword))
				throw HttpError.Unauthorized ("Old password was not correct");

			//Set the new password

			//TODO: Test the new password if has a good quality

			string newSalt = Guid.NewGuid ().ToString ().Substring (0, 12);
			string hashedPassword = DbAuthProvider.HashPassword (newSalt, request.NewPassword);
			Db.UpdateNonDefaults<User> (new User () {
				Salt = newSalt,
				Password = hashedPassword,
				PasswordChangeDate = DateTime.Now,
			}, u => (u.Id == loggedInUserId));
		}

		[Authenticate]
		public void Put (ChangeUser request)
		{
			int loggedInUserId;
			if (!Int32.TryParse (GetSession ().UserAuthId, out loggedInUserId))
				throw HttpError.Forbidden ("No valid authenticated user found.");

			User user = GetUserById (this, loggedInUserId);

			if (request.Avatar != null) {
				if (!request.Avatar.Any ())
					// No avatar is stored via a null value
					request.Avatar = null;
				user.Avatar = request.Avatar;
			}
			if (request.FirstName != null) {
				if (request.FirstName.Trim () == "")
					throw HttpError.Forbidden ("FirstName must not be empty.");
				user.FirstName = request.FirstName.Trim ();
			}
			if (request.LastName != null) {
				if (request.LastName.Trim () == "")
					throw HttpError.Forbidden ("LastName must not be empty.");
				user.LastName = request.LastName.Trim ();
			}
			Db.Update (user, u => u.Id == loggedInUserId);
		}

		public void Post (CreateResetCode request)
		{
			//Check if the user exists in the database
			User user;
			try { 
				user = GetUserByMailAddress (request.MailAddress);
			} catch (HttpError e) {
				//If no such user is found, we fail silently
				if (e.StatusCode == System.Net.HttpStatusCode.NotFound)
					return;
				else
					throw;
			}

			//Create a new reset code and store it in the database
			string resetCode = Guid.NewGuid ().ToString ();
			Db.UpdateNonDefaults<User> (new User () {
				ResetCode = resetCode,
				//The reset code should be valid for one day
				ResetCodeNotAfter = DateTime.Now.AddDays (1)
			}, u => (u.Id == user.Id));
			//Notify the user about the reset code
			var notifier = TryResolve<INotifyUser> ();
			notifier.Notify ("Password reset requested", 
				String.Format ("Hello {0}!\nA password reset for the Hackatohn was requested. If you really want to reset your password, you can use the rest code\n\n{1}\n\nRegards\nYour Hackathon team", user.UserName, resetCode), 
				user);
		}

		public void Post (ResetPassword request)
		{
			//Check if the user exists in the database
			User user;
			try { 
				user = GetUserByMailAddress (request.MailAddress);
			} catch (HttpError) {
				//If no such user is found, we fail with a generic error
				throw HttpError.Forbidden ("Password reset failed.");
			}
			//Validate the reset code
			if (request.ResetCode == null || request.ResetCode == "" ||
			    request.ResetCode != user.ResetCode || user.ResetCodeNotAfter < DateTime.Now)
				throw HttpError.Forbidden ("Password reset failed.");

			//Resetcode is good -> set the new password
			user.ResetCode = null;
			//TODO: Test the new password if has a good quality
			string newSalt = Guid.NewGuid ().ToString ().Substring (0, 12);
			string hashedPassword = DbAuthProvider.HashPassword (newSalt, request.NewPassword);
			user.Salt = newSalt;
			user.Password = hashedPassword;
			user.PasswordChangeDate = DateTime.Now;
			Db.Update (user);
		}

		public UserChallengeResponse Get (GetUserChallenge request)
		{
			//Check if the user exists in the database
			GetUserById (this, request.UserId);
			//Check if the challenge exists in the database
			Challenge challenge = ChallengesService.GetChallengeById (this, request.ChallengeId, false);

			UserChallengeResponse ret = new UserChallengeResponse ();

			//Set the time, when the challenge was accessed (if available). We take the first access, no matter what type it was (Description or Attachments)
			var challengeAccesses = Db.Select<ChallengeAccess> (c => c.UserId == request.UserId && c.ChallengeId == request.ChallengeId);
			DateTime accessDate = DateTime.MinValue;
			if (challengeAccesses.Any ()) {
				accessDate = challengeAccesses.First ().AccessTime;
				ret.AccessDate = accessDate.ToEcmaScriptString ();
				//Set the time and trycount of the solution, if one was posted yet
				var challengeSolutions = Db.Select<ChallengeSolution> (c => c.UserId == request.UserId && c.ChallengeId == request.ChallengeId);
				if (challengeSolutions.Any ()) {
					var solution = challengeSolutions.First ();
					ret.TryCount = solution.WrongEntries;
					//Set the Solved flag
					ret.Solved = solution.Solved;
					if (solution.SolutionTime != DateTime.MinValue) {
						// The challenge has been solved
						ret.SolvedDate = solution.SolutionTime.ToEcmaScriptString ();
						ret.TryCount++;
					}
				}
			}

			//Set the acces time of the hints
			var accessedHints = Db.Select<HintAccess> (h => h.ChallengeId == request.ChallengeId && h.UserId == request.UserId);
			if (accessedHints.Any ()) {
				ret.HintAccess = new List<HintAccessResponse> ();
				foreach (HintAccess hintAccess in accessedHints)
					ret.HintAccess.Add (new HintAccessResponse {
						HintId = hintAccess.HintId,
						AccessTime = hintAccess.AccessTime.ToEcmaScriptString ()
					});
			}

			ret.BasePoints = challenge.BasePoints;

			return ret;
		}

		public PointResponse Get (GetUserChallengePoints request)
		{
			// Try to access the cached points from the database, unless a forced calculaion was requested
			if (!request.ForceCalculation) {
				try {
					var solution = Db.Single<ChallengeSolution> (c => c.UserId == request.UserId && c.ChallengeId == request.ChallengeId);
					if (solution != null) {
						if (!solution.Solved)
							//not solved yet -> no points
							return new PointResponse{ Points = 0 };
						else {
							if (solution.Points != null)
								return new PointResponse{ Points = solution.Points.Value };
						}
					} else 
						//not solved yet -> no points
						return new PointResponse{ Points = 0 };
				} catch {
					//If an error occured we do not have a point value stored in the db. Ignore the error, the actual points will be calculated below
				}
			}
			// We need to calculate the points
			IPoints points = TryResolve<IPoints> ();
			var ret = new PointResponse (){ Points = points.GetPoints (this, request.UserId, request.ChallengeId) };
			//Cache the points in the database
			Db.UpdateNonDefaults<ChallengeSolution> (new ChallengeSolution{ Points = ret.Points }, c => c.UserId == request.UserId && c.ChallengeId == request.ChallengeId);
			return ret;
		}

		public List<PointsAtTimeResponse> Get (GetUserChallengePointsIfSolvedNow request)
		{
			//calculate the desired size of the response (default=3)
			int count = 3;
			if (request.Count.HasValue && request.Count.Value >= 1)
				count = request.Count.Value;

			//Retrieve when the user has accessed that challenge
			DateTime accessTime = DateTime.MinValue;
			var userChallenge = Get (new GetUserChallenge {
				ChallengeId = request.ChallengeId,
				UserId = request.UserId
			});
			if (!userChallenge.AccessDate.IsNullOrEmpty ())
				accessTime = userChallenge.AccessDate.FromEcmaScriptString ();

			//Get the triggered actions 
			var actionIds = ChallengesService.GetChallengeActionIds (this, request.ChallengeId, request.UserId);

			// Get all hint IDs from the database which have ben accessed
			var accessedHintIds = Db.Select<HintAccess> (h => (
			                          (h.ChallengeId == request.ChallengeId) && (h.UserId == request.UserId)
			                      ))
				.Select (h => h.HintId);

			// Calculate the points
			List<PointsAtTimeResponse> ret = new List<PointsAtTimeResponse> ();
			DateTime timeToCheck = DateTime.Now;
			IPoints points = TryResolve<IPoints> ();
			while (count > 0) {
				int pointsAtTime = points.GetPoints (this, request.UserId, request.ChallengeId, timeToCheck, accessTime, accessedHintIds, actionIds);
				ret.Add (new PointsAtTimeResponse () {
					Time = timeToCheck.ToEcmaScriptString (),
					Points = pointsAtTime
				});
				timeToCheck = timeToCheck.Add (TimeSpan.FromHours (1));
				--count;
			}
			return ret;
		}
	}
}

