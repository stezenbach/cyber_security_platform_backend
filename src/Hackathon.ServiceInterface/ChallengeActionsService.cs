﻿//
//  ChallengeActionsService.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2017 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using ServiceStack;
using Hackathon.ServiceModel;
using Hackathon.ServiceModel.Types;
using ServiceStack.OrmLite;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Generic;
using System.Net;

namespace Hackathon.ServiceInterface
{
	public class ChallengeActionsService : Service
	{
		/// <summary>
		/// The type whose JSON encoding has to be authenticated by a MAC when posting an action for a user
		/// </summary>
		public class PostActionAuthenticationData
		{
			public int UserId { get; set; }

			public int ChallengeId { get; set; }

			public int ActionId { get; set; }

			public UInt64 Nonce { get; set; }
		}

		public enum PostChallengeActionErrorCode
		{
			ActionAlreadyTriggered = 1,
			BadChallengeIdForAction,
			NonceReuse,
			ChallengeNotActiveForUser,
			MacValidationFailed,
		}

		internal static ChallengeAction GetChallengeActionById (Service service, int id)
		{
			var actions = service.Db.Select (service.Db.From<ChallengeAction> ().Where (a => a.Id == id));
			if (!actions.Any ())
				throw HttpError.NotFound ("No such action found");
			else if (actions.Count () > 1)
				throw HttpError.Conflict ("More than one action with that ID found.");
			else
				return actions.First ();
		}

		public void Post (PostChallengeActions request)
		{
			// Validate the IDs in the request first
			// We need to check if
			// 1. The user exists
			// 2. The action with ActionId exists
			// 3. That action has not already been  triggered for this user
			// 4. The challenge with ChallengeId exists and matches the action
			// 5. The challenge is in an active event for the user

			UsersService.GetUserById (this, request.Id);  //throws an error, if the user does not exist

			ChallengeAction action = GetChallengeActionById (this, request.ActionId);  //throws an error, if the action does not exist

			//Check if the action has not yet been triggered
			if (Db.Exists<ChallengeActionAccess> (c => c.ChallengeActionId == request.ActionId && c.UserId == request.Id))
				throw new HttpError (HttpStatusCode.Conflict, PostChallengeActionErrorCode.ActionAlreadyTriggered.ToString (), "This action has already been triggered for this user.");

			if (action.ChallengeId != request.ChallengeId)
				throw new HttpError (HttpStatusCode.Conflict, PostChallengeActionErrorCode.BadChallengeIdForAction.ToString (), "The challenge ID of the action and the request do not match");
			if (!Db.Exists<Challenge> (new {Id = action.ChallengeId}))
				throw HttpError.NotFound ("Challenge does not exist.");

			var userEventIds = Db.Select<int> (Db.From<EventUsers> ().Where (eu => eu.UserId == request.Id).GroupBy (eu => eu.EventId).Select (eu => eu.EventId));
			if (!ChallengesService.IsChallengeAvailableAndActive (this, request.ChallengeId, userEventIds, false, true))
				throw new HttpError (HttpStatusCode.Forbidden, PostChallengeActionErrorCode.ChallengeNotActiveForUser.ToString (), "Challenge is not currently active for this user.");
			

			//Check if the nonce has not been used before
			if (Db.Exists<ChallengeActionAccess> (new {Nonce = request.Nonce}))
				throw new HttpError (HttpStatusCode.Conflict, PostChallengeActionErrorCode.NonceReuse.ToString (), "Nonce has been used before");

			//Validate the MAC
			string authenticatedMessage = new PostActionAuthenticationData () {
				UserId = request.Id,
				ChallengeId = request.ChallengeId,
				ActionId = request.ActionId,
				Nonce = request.Nonce
			}.ToJson ();
			using (var macCalculator = new HMACSHA256 (Convert.FromBase64String (action.Key))) {
				var macBin = macCalculator.ComputeHash (UTF8Encoding.UTF8.GetBytes (authenticatedMessage));
				string expectedMac = Convert.ToBase64String (macBin);
				if (expectedMac != request.Mac)
					throw new HttpError (HttpStatusCode.Forbidden, PostChallengeActionErrorCode.MacValidationFailed.ToString (), "MAC validation failed.");
			}

			//Authentication worked. We can enter the action into the access table
			Db.Insert (new ChallengeActionAccess {
				AccessTime = DateTime.UtcNow,
				ChallengeActionId = request.ActionId,
				Nonce = request.Nonce,
				UserId = request.Id
			});
			//We have to trigger the point calculation, in case the challenge was already solved and the points need to get reduced now
			using (var userService = TryResolve<UsersService> ()) {
				//Points will get calculated and stored in the DB if we call the points. We can safely ignore the return value
				userService.Get (new GetUserChallengePoints () {
					ChallengeId = request.ChallengeId,
					UserId = request.Id,
					ForceCalculation = true
				});
			}
		}

		/// <summary>
		/// This is a type used in the join of the method below
		/// We cannot use GetChallengeActionsResponse.ChallengeActionInformation directly, because of the different types of the AccessTime property
		/// This is a dirty hack, and shoub be doable more elegant, but I do not have the time now
		/// </summary>
		public class GetChallengeActionsJoin
		{
			public string Description { get; set; }

			public bool LockChallegenge { get; set; }

			public DateTime AccessTime { get; set; }

			public bool RemoveBonus { get; set; }

			public int PointReduction { get; set; }
		}

		[Authenticate ()]
		public GetChallengeActionsResponse Get (GetChallengeActions request)
		{
			int loggedInUserId;
			if (!Int32.TryParse (GetSession ().UserAuthId, out loggedInUserId))
				throw HttpError.Forbidden ("No valid authenticated user found.");

			var query = Db.From<ChallengeAction> ()
				.Join<ChallengeAction, ChallengeActionAccess> ()
				.Where<ChallengeAction> (ca => ca.ChallengeId == request.ChallengeId)
				.And<ChallengeActionAccess> (c => c.UserId == loggedInUserId);
			
			var actions = Db.Select<GetChallengeActionsJoin> (query);

			List<GetChallengeActionsResponse.ChallengeActionInformation> dtoList = new List<GetChallengeActionsResponse.ChallengeActionInformation> ();
			foreach (var action in actions) {
				GetChallengeActionsResponse.ChallengeActionInformation dtoAction = action.ConvertTo<GetChallengeActionsResponse.ChallengeActionInformation> ();
				dtoAction.AccessTime = action.AccessTime.ToEcmaScriptString ();
				dtoAction.LockChallenge = action.LockChallegenge;
				dtoList.Add (dtoAction);
			}

			return new GetChallengeActionsResponse { Actions = dtoList };
		}
	}
}

