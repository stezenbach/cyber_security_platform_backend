﻿//
//  ChallengesService.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;
using System.Collections.Generic;
using Hackathon.ServiceModel.Types;
using ServiceStack;
using Hackathon.ServiceModel;
using ServiceStack.OrmLite;
using System.Net;
using System.Data;
using Hackathon.ServiceInterface.Points;
using System.Security.Cryptography;
using System.Numerics;
using System.Web;

namespace Hackathon.ServiceInterface
{
	public class ChallengesService : Service
	{
		/// <summary>
		/// Helper method to fetch a certain challenge from the database.
		/// Throws the appropriate HttpError Exceptions, in case of an error.
		/// </summary>
		/// <returns>The challenge from the database</returns>
		/// <param name="id">Id of the challenge</param>
		/// <param name="onlyIfAccesible">If set to <c>true</c>, a validation is done if the challenge is accesible to the logged in user (in an event the user is registered for and in a challengeset which is currently active)</param>
		/// <param name="onlyIfExpired">If true, will cause an error if the challenge is not expired for the user </param>
		private Challenge GetChallengeById (int id, bool onlyIfAccesible = false, bool onlyIfExpired = false)
		{
			return GetChallengeById (this, id, onlyIfAccesible, onlyIfExpired);
		}

		/// <summary>
		/// Helper method to fetch a certain challenge from the database.
		/// Throws the appropriate HttpError Exceptions, in case of an error.
		/// </summary>
		/// <returns>The challenge from the database</returns>
		/// <param name="service">The service which is requesting the data</param>
		/// <param name="id">Id of the challenge</param>
		/// <param name="onlyIfAccesible">If set to <c>true</c>, a validation is done if the challenge is accesible to the logged in user (in an event the user is registered for and in a challengeset which is currently active)</param>
		/// <param name="onlyIfExpired">If true, will cause an error if the challenge is not expired for the user </param>
		internal static Challenge GetChallengeById (Service service, int id, bool onlyIfAccesible, bool onlyIfExpired = false)
		{
			if (onlyIfAccesible && !IsChallengeAvailableAndActive (service, id, onlyIfExpired))
				throw HttpError.Forbidden ("This challenge is not available for you.");
			var challenges = service.Db.Select<Challenge> ().Where (c => c.Id == id);
			if (!challenges.Any ())
				throw HttpError.NotFound ("Challenge not found");
			if (challenges.Count () > 1)
				throw HttpError.Conflict ("More than one challenge with this ID found.");
			Challenge challenge = challenges.First ();
			return challenge;
		}



		internal static void LogChallengeAccess (IDbConnection Db, int challengeId, int userId, AccessType accessType)
		{
			var previousAccesses = Db.Select<ChallengeAccess> ().Where (c => (c.ChallengeId == challengeId && c.UserId == userId && c.Type == accessType));
			if (!previousAccesses.Any ()) {
				// Description has not been accessed before. We need to log the access
				Db.Insert<ChallengeAccess> (new ChallengeAccess () {
					UserId = userId,
					ChallengeId = challengeId,
					Type = accessType,
					AccessTime = DateTime.Now
				});
			}
		}

		internal static void LogHintAccess (IDbConnection Db, int challengeId, int userId, int hintNumber)
		{
			var previousAccesses = Db.Select<HintAccess> ().Where (c => (c.ChallengeId == challengeId && c.UserId == userId && c.HintId == hintNumber));
			if (!previousAccesses.Any ()) {
				// There has not been an access before. We need to log the access
				Db.Insert<HintAccess> (new HintAccess () {
					UserId = userId,
					ChallengeId = challengeId,
					HintId = hintNumber,
					AccessTime = DateTime.Now
				});
			}
		}

		/// <summary>
		/// Determines if a challenge is available in an active or expired event for the logged in user
		/// </summary>
		/// <returns><c>true</c> if it is, <c>false</c> otherwise.</returns>
		/// <param name="service">Service to query</param>
		/// <param name="challengeId">Id of the callenge to check</param>
		/// <param name="onlyExpired">If true, only challenges from expired events will be considered</param>
		internal static bool IsChallengeAvailableAndActive (Service service, int challengeId, bool onlyExpired = false)
		{
			using (var usersService = service.ResolveService<UsersService> ()) {
				//Get all events for the user
				var userEvents = usersService.Get (new GetLoggedInUserEvents ());
				return IsChallengeAvailableAndActive (service, challengeId, userEvents, onlyExpired);
			}
		}


		/// <summary>
		/// Determines if a challenge is available in an active or expired event
		/// </summary>
		/// <returns><c>true</c> if it is, <c>false</c> otherwise.</returns>
		/// <param name="service">Service to query</param>
		/// <param name="challengeId">Id of the callenge to check</param>
		/// <param name="userEventIds">A list of event IDs which are to be checked for the challenge</param>
		/// <param name="onlyExpired">If true, only challenges from expired events will be considered</param>
		/// <param name="noExpired"> If true, no expired challenge will be accepted</param>
		internal static bool IsChallengeAvailableAndActive (Service service, int challengeId, IEnumerable<int> userEventIds, bool onlyExpired = false, bool noExpired = false)
		{
			using (var eventsService = service.ResolveService<EventsService> ()) {
				using (var challengeSetsService = service.ResolveService<ChallengeSetsService> ()) {
					foreach (int eventId in userEventIds) {
						//Get all ChallengeSets in the current event
						var challengesets = eventsService.Get (new GetEvent {
							Id = eventId,
							OnlyCurrentAndPast = true,
							OnlyCurrent = noExpired,
						}).ChallengeSets;
						var expiredChallengeSets = challengesets.Where (c => c.EndDate.FromEcmaScriptString ().ToUniversalTime () < DateTime.UtcNow);
						//challengesets are only the active and expired ones. So we do not need to check the StartDate to get the running ones.
						var runningChallengeSets = challengesets.Except (expiredChallengeSets);
						foreach (int challengeSetId in runningChallengeSets.Select(c => c.ChallengeSetId)) {
							//Get all challenges in the current ChallengeSet
							var challengeIds = challengeSetsService.Get (new GetChallengeSet{ Id = challengeSetId }).Challenges;
							//Is the requested challenge part of the current challengeset?
							if (challengeIds != null && challengeIds.Contains (challengeId)) {
								//The challenge is part of a running ChallengeSets. If we are only looking for expired challenges, we must return false now, otherwise we can return true
								if (!onlyExpired)
									return true;
								else
									return false;
							}
						}
						foreach (int challengeSetId in expiredChallengeSets.Select(c => c.ChallengeSetId)) {
							//Get all challenges in the current ChallengeSet
							var challengeIds = challengeSetsService.Get (new GetChallengeSet{ Id = challengeSetId }).Challenges;
							//Is the requested challenge part of the current challengeset?
							if (challengeIds != null && challengeIds.Contains (challengeId)) {
								//The challenge is part of an expired ChallengeSet. We may return true now, for all values of onlyExpired
								return true;
							}
						}
					}
				}
			}
			//If we reach this point, the challenge is not available or inactive
			return false;
		}

		internal static bool IsChallengeLocked (Service service, int challengeId, int userId)
		{
			return service.Db.Exists (service.Db.From<ChallengeAction> ().Where (ca => ca.ChallengeId == challengeId && ca.LockChallegenge).Join<ChallengeActionAccess> ((ca, caa) => ca.Id == caa.ChallengeActionId && caa.UserId == userId));
		}

		internal static IEnumerable<int> GetChallengeActionIds (Service service, int challengeId, int userId)
		{
			return service.Db.Select (service.Db.From<ChallengeAction> ().Where (ca => ca.ChallengeId == challengeId).Join<ChallengeActionAccess> ((ca, caa) => ca.Id == caa.ChallengeActionId && caa.UserId == userId)).Select (ca => ca.Id);
		}

		private static bool MayPostSolution(Service service, int userId, int challengeId)
		{
			bool solved, locked;
			return MayPostSolution (service, userId, challengeId, out solved, out locked);
		}

		private static bool MayPostSolution(Service service, int userId, int challengeId, out bool solved, out bool locked)
		{
			//Check if the challenge has been locked for this user by an action
			locked = IsChallengeLocked (service, challengeId, userId);

			//Check if the user has already solved this challenge
			solved = service.Db.Exists<ChallengeSolution> (s => (s.UserId == userId && s.ChallengeId == challengeId && s.Solved));

			return (!locked && !solved);
		}

		[Authenticate]
		[CacheResponse (Duration = 1800, MaxAge = 600, VaryByUser = true)]
		public FindChallengeResponse Get (FindChallenges request)
		{
			int loggedInUserId;
			if (!Int32.TryParse (GetSession ().UserAuthId, out loggedInUserId))
				throw HttpError.Forbidden ("No valid authenticated user found.");
			
			// We need to validate, if the requested challenge is contained in some event the user has access to
			return GetChallengeById (request.Id, true).ConvertTo<FindChallengeResponse> ();
		}

		[Authenticate]
		public GetChallengeDescriptionResponse Get (GetChallengeDescription request)
		{
			int loggedInUserId;
			if (!Int32.TryParse (GetSession ().UserAuthId, out loggedInUserId))
				throw HttpError.Forbidden ("No valid authenticated user found.");

			GetUserResponse loggedInUser;
			using (var usersService = ResolveService<UsersService> ()) {
				loggedInUser = usersService.Get (new GetLoggedInUser ());
			}

			// We need to validate, if the requested challenge is contained in some event the user has access to
			Challenge challenge = GetChallengeById (request.Id, true);

			GetChallengeDescriptionResponse ret = challenge.ConvertTo<GetChallengeDescriptionResponse> ();

			//Check if we have to replace a user token
			Match userTokenMatch = Regex.Match (ret.Description, @"{{{UserToken-([a-z]+)}}}");
			if (userTokenMatch.Success) {
				string userTokenIdentifier = userTokenMatch.Groups [1].Value;
				HMAC hmac = TryResolve<HMAC> ();
				if (hmac == null)
					throw HttpError.NotFound ("Not possible to calculate a user token");
				string toMac = String.Format ("{0}{1}", userTokenIdentifier, loggedInUser.UserName.Trim ());
				string userToken = Convert.ToBase64String (hmac.ComputeHash (UTF8Encoding.UTF8.GetBytes (toMac)));
				userToken = userToken.ReplaceAll ("/", "");
				userToken = userToken.Replace ("+", "");
				userToken = userToken.Replace ("=", "");
				ret.Description = ret.Description.Replace (userTokenMatch.Groups [0].Value, userToken);
			}

			//Check if we have to replace a userId token
			Match userIdTokenMatch = Regex.Match (ret.Description, @"{{{UserIdToken-([a-z]+)}}}");
			if (userIdTokenMatch.Success) {
				string userIdTokenIdentifier = userIdTokenMatch.Groups [1].Value;
				//derive the encryption key, which is defined as K = HMAC-SHA256(usedIdTokenIdentifier, challengeId)
				HMAC kdf = new HMACSHA256 (UTF8Encoding.UTF8.GetBytes (userIdTokenMatch.Value));
				byte[] encryptionKey = kdf.ComputeHash (UTF8Encoding.UTF8.GetBytes (request.Id.ToString ()));
				// Convert the user ID to a binary array
				byte[] userId = new BigInteger (loggedInUserId).ToByteArray ();
				//Encrypt the user id with PKCS#7 padding in ECB mode (only one block)
				byte[] encryptedId = null;
				using (Aes encryptor = Aes.Create ()) {
					encryptor.Key = encryptionKey;
					encryptor.Mode = CipherMode.ECB;
					encryptor.Padding = PaddingMode.PKCS7;
					encryptedId = new byte[encryptor.BlockSize / 8];
					using (var e = encryptor.CreateEncryptor ()) {
						encryptedId = e.TransformFinalBlock (userId, 0, userId.Length);
					}
				}
				//calculate an authenticator for the encryptedId via HMAC
				byte[] tag;
				using (var authenticator = new HMACSHA256 (encryptionKey)) {
					tag = authenticator.ComputeHash (encryptedId);
				}

				byte[] userIdTokenBin = encryptedId.Concat (tag).ToArray ();
				string userIdToken = HttpUtility.UrlEncode (Convert.ToBase64String (userIdTokenBin));
				ret.Description = ret.Description.Replace (userIdTokenMatch.Groups [0].Value, userIdToken);
			}

			//log the access to the description
			LogChallengeAccess (Db, request.Id, loggedInUserId, AccessType.Description);

			return ret;
		}

		[Authenticate]
		public GetChallengeSolutionResponse Get (GetChallengeSolution request)
		{
			int loggedInUserId;
			if (!Int32.TryParse (GetSession ().UserAuthId, out loggedInUserId))
				throw HttpError.Forbidden ("No valid authenticated user found.");
		
			// We need to validate, if the requested challenge is contained in some event the user has access to and is expired
			Challenge challenge = GetChallengeById (request.Id, true, true);
		
			//log the access to the description
			LogChallengeAccess (Db, request.Id, loggedInUserId, AccessType.Solution);

			return challenge.ConvertTo<GetChallengeSolutionResponse> ();
		}

		[Authenticate]
		public GetChallengeSolutionEnteredResponse Get (GetChallengeSolutionEntered request)
		{
			int loggedInUserId;
			if (!Int32.TryParse (GetSession ().UserAuthId, out loggedInUserId))
				throw HttpError.Forbidden ("No valid authenticated user found.");

			// We need to validate, if the requested challenge is contained in some event the user has access to
			Challenge challenge = GetChallengeById (request.Id, true);

			string solutionEntered = Db.Select<ChallengeSolution> (cs => cs.ChallengeId == request.Id && cs.UserId == loggedInUserId).Select (cs => cs.SolutionEntered).FirstOrDefault ();

			return new GetChallengeSolutionEnteredResponse {
				SolutionEntered = solutionEntered,
				SolutionType = challenge.SolutionType
			};
		}

		[Authenticate]
		public GetChallengeSolutionMayEnterResponse Get (GetChallengeSolutionMayEnter request)
		{
			int loggedInUserId;
			if (!Int32.TryParse (GetSession ().UserAuthId, out loggedInUserId))
				throw HttpError.Forbidden ("No valid authenticated user found.");

			// We need to validate, if the requested challenge is contained in some event the user has access to
			GetChallengeById (request.Id, true);
			bool locked, solved;
			bool mayEnter = MayPostSolution (this, loggedInUserId, request.Id, out solved, out locked);
			return new GetChallengeSolutionMayEnterResponse {
				MayEnterSolution = mayEnter,
				Solved = solved,
				Locked = locked
			};
		}

		[Authenticate]
		public void Post (PostSolution request)
		{
			int loggedInUserId;
			if (!Int32.TryParse (GetSession ().UserAuthId, out loggedInUserId))
				throw HttpError.Forbidden ("No valid authenticated user found.");

			//Fetch the challenge from the database
			Challenge challenge = GetChallengeById (request.Id, true);

			//Check if the user may post a solution
			if(!MayPostSolution(this, loggedInUserId, request.Id))
				throw HttpError.Conflict ("You may not post a solution to this challenge.");
			
			//Check if the SolutionType of the posted solution matches the expected one in the challenge
			if (request.SolutionType != challenge.SolutionType)
				throw HttpError.Conflict ("The SolutionType is not as expected.");

			bool solutionIsCorrect = false;
			if (request.SolutionType == SolutionType.Static) {
				//Static solutions are unstructured. Just compare the string values
				solutionIsCorrect = (request.Solution == challenge.Solution);
			} else if (request.SolutionType == SolutionType.MultipleChoice) {
				// A MultipleChoice solution is expected to be a JSON encoded List<bool>
				List<bool> postedSolution, expectedSolution;
				try {
					postedSolution = request.Solution.FromJson<List<bool>> ();
				} catch {
					throw HttpError.Conflict ("The posted solution did not have the correct format.");
				}
				try {
					expectedSolution = challenge.Solution.FromJson<List<bool>> ();
				} catch {
					throw HttpError.Conflict ("The solution in the database did not have the correct format.");
				}
				//the number of solutions has to be equal to the expected ones. We throw an error, if this is not the case
				if (expectedSolution.Count != postedSolution.Count)
					throw HttpError.Conflict ("The number of solutions proposed is not correct.");
				
				solutionIsCorrect = postedSolution.SequenceEqual (expectedSolution);
			} else if (request.SolutionType == SolutionType.MultiPart) {
				// A MultiPart solution is expected to be a JSON encoded Dictionary<string,string>
				Dictionary<string, string> postedSolution, expectedSolution;
				try {
					postedSolution = request.Solution.FromJson<Dictionary<string, string>> ();
				} catch {
					throw HttpError.Conflict ("The posted solution did not have the correct format.");
				}
				try {
					expectedSolution = challenge.Solution.FromJson<Dictionary<string, string>> ();
				} catch {
					throw HttpError.Conflict ("The solution in the database did not have the correct format.");
				}
				// Check if the answers are for the correct question (same number, correct identifiers)
				if (!MultiPart.PossibleAnswer (postedSolution, expectedSolution))
					throw HttpError.Conflict ("The solution and the answers are not from the same question.");
				
				// Check the correctness of all answers by calculating the points. They should be the maximum number of points
				solutionIsCorrect = (MultiPart.CalculatePoints (postedSolution, expectedSolution, 1000) == 1000);
			} else {
				//unknown SolutionType
				throw HttpError.Forbidden ("I do not know how to handle this SolutionType.");
			}

			ChallengeSolution previousSolution = Db.Select<ChallengeSolution> ().Where (s => (s.UserId == loggedInUserId && s.ChallengeId == request.Id)).FirstOrDefault ();
			if (solutionIsCorrect) {
				//correct solution
				if (previousSolution == null)
					Db.Insert<ChallengeSolution> (new ChallengeSolution {
						ChallengeId = challenge.Id,
						UserId = loggedInUserId,
						Solved = true,
						SolutionTime = DateTime.Now,
						SolutionEntered = request.Solution,
					});
				else {
					previousSolution.Solved = true;
					previousSolution.SolutionEntered = request.Solution;
					previousSolution.SolutionTime = DateTime.Now;

					Db.Update (previousSolution);
				}

				// Calculate the points and store in in the database
				// Maybe we should caluclate the points via the point route instead of doing it ourselves??
				IPoints pointCalculator = TryResolve<IPoints> ();
				if (pointCalculator != null) {  //If there is no point calculator registered right now, we silently ignore this error, since the points can be calulated later on, too
					int points = pointCalculator.GetPoints (this, loggedInUserId, request.Id);
					Db.UpdateNonDefaults<ChallengeSolution> (new ChallengeSolution{ Points = points }, c => c.ChallengeId == challenge.Id && c.UserId == loggedInUserId);
				}
			} else {
				//bad solution
				if (previousSolution == null) {
					Db.Insert (new ChallengeSolution {	
						ChallengeId = challenge.Id,
						UserId = loggedInUserId,
						// Answers to multiple choice or multipart question should only be submittable once. Thus for such aquestion, we are setting `Solved` to true anyway.
						Solved = (challenge.SolutionType == SolutionType.MultipleChoice || challenge.SolutionType == SolutionType.MultiPart) ? true : false,   
						SolutionTime = (challenge.SolutionType == SolutionType.MultipleChoice || challenge.SolutionType == SolutionType.MultiPart) ? DateTime.Now : DateTime.MinValue,  
						WrongEntries = 1,
						SolutionEntered = request.Solution,
					});
				} else {
					previousSolution.WrongEntries++;
					previousSolution.SolutionEntered = request.Solution;
					Db.Update (previousSolution);
				}

				//Tell the client that the solution was bad
				throw new HttpError (HttpStatusCode.BadRequest);
			}
		}

		[Authenticate]
		[CacheResponse (Duration = 1800, MaxAge = 600)]
		public List<int> Get (GetChallengeHints request)
		{
			//Check if the challenge exits
			GetChallengeById (request.Id, true);

			return Db.Select<int> (Db.From<Hint> ()
				.Where (h => h.ChallengeId == request.Id)
				.Select (h => h.Id)
			);
		}

		[Authenticate]
		public GetChallengeHintResponse Get (GetChallengeHint request)
		{
			var session = this.GetSession ();
			int userId = Int32.Parse (session.UserAuthId);

			Challenge challenge = GetChallengeById (request.Id, true);
			var hints = Db.Select<Hint> (h => h.Id == request.HintNumber && h.ChallengeId == challenge.Id);
			if (!hints.Any () || hints.Count > 1)
				throw HttpError.NotFound ("No such hint available");
			
			LogHintAccess (Db, request.Id, userId, request.HintNumber);

			return new GetChallengeHintResponse () {
				HintNumber = request.HintNumber,
				Text = hints.First ().Text
			};
		}

		[Authenticate]
		public PointResponse Get (GetHintPointReduction request)
		{
			int loggedInUserId;
			if (!Int32.TryParse (GetSession ().UserAuthId, out loggedInUserId))
				throw HttpError.Forbidden ("No valid authenticated user found.");

			DateTime accessTime = DateTime.MinValue;
			using (var usersService = ResolveService<UsersService> ()) {
				var userChallenge = usersService.Get (new GetUserChallenge {
					ChallengeId = request.Id,
					UserId = loggedInUserId
				});

				if (!userChallenge.AccessDate.IsNullOrEmpty ())
					accessTime = userChallenge.AccessDate.FromEcmaScriptString ();
			}
			//Get the already used hints
			List<int> accessedHintIds = Db.Select<HintAccess> (h => ((h.ChallengeId == request.Id) && (h.UserId == loggedInUserId))).Select (h => h.HintId).ToList ();

			//Get the triggered actions 
			var actionIds = ChallengesService.GetChallengeActionIds (this, request.Id, loggedInUserId);

			// Calculate the points without the hint
			IPoints pointCalculator = TryResolve<IPoints> ();
			int pointsWithoutHint = pointCalculator.GetPoints (this, loggedInUserId, request.Id, DateTime.Now, accessTime, accessedHintIds, actionIds);

			// Calculate the points with the hint
			accessedHintIds.Add (request.HintNumber);
			int pointsWithHint = pointCalculator.GetPoints (this, loggedInUserId, request.Id, DateTime.Now, accessTime, accessedHintIds, actionIds);

			return new PointResponse (){ Points = (pointsWithoutHint - pointsWithHint) };
		}
	
	}
}

