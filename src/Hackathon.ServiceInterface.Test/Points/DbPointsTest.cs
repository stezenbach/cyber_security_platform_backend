﻿//
//  DbPointsTest.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using NUnit.Framework;
using System;
using Moq;
using ServiceStack.Testing;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using ServiceStack.Auth;
using ServiceStack;
using Hackathon.ServiceInterface.Points;
using Hackathon.ServiceModel.Auth;
using Hackathon.ServiceModel.Types;
using Hackathon.ServiceInterface.Notification;
using System.Collections.Generic;
using System.Linq;

namespace Hackathon.ServiceInterface.Test.Points
{
	[TestFixture ()]
	public class DbPointsTest
	{
		static Mock<INotifyUser> notifierMock = new Mock<INotifyUser> ();

		private class TestAppHost : BasicAppHost
		{
			public TestAppHost () : base (typeof(UsersService).Assembly)
			{
			}

			public override void Configure (Funq.Container container)
			{
				container.Register<IDbConnectionFactory> (c =>
					new OrmLiteConnectionFactory (":memory:", SqliteDialect.Provider)
				);

				container.Register<IAuthSession> (c => new AuthUserSession {
					UserAuthName = "foobar@mailinator.com",
					UserName = "foobar",
					UserAuthId = "1",
				});

				container.RegisterAs<DbPoints,IPoints> ();

				Plugins.Add (new AuthFeature (
					() => new AuthUserSession (),
					new IAuthProvider[] { new DbAuthProvider () }) {
					// Do not redirect when trying to access a service with an AuthenticatedAttribute (only valid for content type HTML), just return the error code instead
					HtmlRedirect = null,
				}
				);

				using (var db = container.TryResolve<IDbConnectionFactory> ().Open ()) {
					db.DropAndCreateTable<User> ();
					db.DropAndCreateTable<Event> ();
					db.DropAndCreateTable<EventUsers> ();
					db.DropAndCreateTable<EventChallengeSets> ();
					db.DropAndCreateTable<Challenge> ();
					db.DropAndCreateTable<ChallengeSet> ();
					db.DropAndCreateTable<Hint> ();
					db.DropAndCreateTable<ChallengeAccess> ();
					db.DropAndCreateTable<ChallengeSolution> ();
					db.DropAndCreateTable<HintAccess> ();
					db.DropAndCreateTable<ChallengeAction> ();
					db.DropAndCreateTable<ChallengeActionAccess> ();

					db.Insert (new Event{ Id = 1 });

					db.Insert (new User {
						Id = 1,
						UserName = "foobar",
						FirstName = "Foo",
						LastName = "bar",
						Avatar = @"data:image/png;base64, 1111111KGgoAAAANSUhEUgAAAAUA
							11111111AACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO
							11111111HwAAAABJRU5ErkJggg==",
						MailAddress = "foobar@mailinator.com",
						Salt = "foobar",
						//pw=123456, created with echo -n "foobar123456"|sha256sum, then base64 encoded
						Password = @"V7yttgjbKKO8rKp6OTiHqQ8BCSEKqjaGgBtun4lWjfs=",
					});

					db.Insert (new Challenge {
						Id = 1,
						Title = "A challenge",
						Solution = "A solution",
						SolutionType = SolutionType.Static,
						Description = "Info about the challenge",
						BasePoints = 100,
					});

					db.Insert (new ChallengeSet { Id = 1, Challenges = new List<int>{ 1 } });

					db.Insert (new EventChallengeSets {
						ChallengeSetId = 1,
						EventId = 1,
						StartDate = DateTime.Now.Subtract (new TimeSpan (1, 0, 0)),
						EndDate = DateTime.Now.Add (new TimeSpan (1, 0, 0))
					});

					db.Insert (new EventUsers { EventId = 1, UserId = 1 });

				}

				//Mock the user notifier
				container.Register<INotifyUser> (notifierMock.Object);
			}
		}

		private ServiceStackHost appHost;

		[SetUp]
		public void SetUpUsersServiceTest ()
		{
			try {
				appHost = new TestAppHost ();
				appHost.Init ();
			} catch (Exception e) {
				Console.WriteLine (e);
				Assert.Fail (e.ToString ());
			}
		}

		[TearDown]
		public void TestFixtureTearDown ()
		{
			appHost.Dispose ();
		}

		[Test ()]
		public void GetPointsUnsolvedTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				IPoints pointsCalculator = appHost.TryResolve<IPoints> ();
				Assert.That (pointsCalculator.GetPoints (service, 1, 1), Is.EqualTo (0));
			}
		}

		[Test ()]
		public void GetPointsBasePointsTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				IPoints pointsCalculator = appHost.TryResolve<IPoints> ();
				using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
					//prepare the database, such that the challenge is solved without a speed bouns
					db.UpdateNonDefaults (new EventChallengeSets {
						StartDate = new DateTime (2016, 01, 01, 12, 00, 00),
						EndDate = new DateTime (2016, 01, 11, 12, 00, 00),
					}, c => c.EventId == 1 && c.ChallengeSetId == 1);
					db.Insert (new ChallengeAccess () {
						AccessTime = new DateTime (2016, 01, 01, 12, 00, 00),
						ChallengeId = 1,
						Type = AccessType.Description,
						UserId = 1
					});
					db.Insert (new ChallengeSolution () {
						ChallengeId = 1,
						SolutionTime = new DateTime (2016, 01, 11, 11, 59, 00),
						Solved = true,
						UserId = 1,
						WrongEntries = 0
					});

				}
				Assert.That (pointsCalculator.GetPoints (service, 1, 1), Is.EqualTo (100));				
			}
		}

		[Test ()]
		public void GetPointsBasePointsMultipleChoiceTest ()
		{
			int userId = 1;
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				IPoints pointsCalculator = appHost.TryResolve<IPoints> ();
				using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
					//prepare the database, such that the challenge is solved without a speed bouns
					int challengeId = (int)db.Insert (new Challenge {
						Solution = new List<bool> { true, false }.ToJson (),
						SolutionType = SolutionType.MultipleChoice,
						BasePoints = 100,
					}, true);

					int challengeSetId = (int)db.Insert (new ChallengeSet { Challenges = new List<int>{ challengeId } }, true);
					int eventId = (int)db.Insert (new Event (), true);
					db.Insert (new EventChallengeSets {
						ChallengeSetId = challengeSetId,
						EventId = eventId,
						StartDate = DateTime.Now.Subtract (new TimeSpan (1, 0, 0)),
						EndDate = DateTime.Now.Add (new TimeSpan (1, 0, 0))
					});
					db.Insert (new EventChallengeSets {
						EventId = eventId,
						ChallengeSetId = challengeSetId,
						StartDate = new DateTime (2016, 01, 01, 12, 00, 00),
						EndDate = new DateTime (2016, 01, 11, 12, 00, 00),
					});
					db.Insert (new EventUsers { EventId = eventId, UserId = userId });
					db.Insert (new ChallengeAccess () {
						AccessTime = new DateTime (2016, 01, 01, 12, 00, 00),
						ChallengeId = challengeId,
						Type = AccessType.Description,
						UserId = userId,
					});
					db.Insert (new ChallengeSolution () {
						ChallengeId = challengeId,
						SolutionTime = new DateTime (2016, 01, 11, 11, 59, 00),
						Solved = true,
						SolutionEntered = new List<bool> { true, true }.ToJson (),
						UserId = userId,
						WrongEntries = 0
					});
					Assert.That (pointsCalculator.GetPoints (service, userId, challengeId), Is.EqualTo (50));				
				}
			}
		}

		[Test ()]
		public void GetPointsBasePointsMultiPartTest ()
		{
			int userId = 1;
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				IPoints pointsCalculator = appHost.TryResolve<IPoints> ();
				using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
					//prepare the database, such that the challenge is solved without a speed bouns
					int challengeId = (int)db.Insert (new Challenge {
						Solution = new Dictionary<string, string> {
							{ "a", "answer" }, {
								"b",
								"another"
							}
						}.ToJson (),
						SolutionType = SolutionType.MultiPart,
						BasePoints = 100,
					}, true);

					int challengeSetId = (int)db.Insert (new ChallengeSet { Challenges = new List<int>{ challengeId } }, true);
					int eventId = (int)db.Insert (new Event (), true);
					db.Insert (new EventChallengeSets {
						ChallengeSetId = challengeSetId,
						EventId = eventId,
						StartDate = DateTime.Now.Subtract (new TimeSpan (1, 0, 0)),
						EndDate = DateTime.Now.Add (new TimeSpan (1, 0, 0))
					});
					db.Insert (new EventUsers { EventId = eventId, UserId = userId });
					db.Insert (new ChallengeAccess () {
						AccessTime = DateTime.Now.Subtract (TimeSpan.FromMinutes (2)),
						ChallengeId = challengeId,
						Type = AccessType.Description,
						UserId = userId,
					});
					db.Insert (new ChallengeSolution () {
						ChallengeId = challengeId,
						SolutionTime = DateTime.Now.Subtract (TimeSpan.FromMinutes (1)),
						Solved = true,
						SolutionEntered = new Dictionary<string, string> {
							{ "b", "no idea" }, {
								"a",
								"answer"
							}
						}.ToJson (),
						UserId = userId,
						WrongEntries = 0
					});
					Assert.That (pointsCalculator.GetPoints (service, userId, challengeId), Is.EqualTo (50));				
				}
			}
		}

		[Test ()]
		public void GetPointsNoSpeedBonusForMultipleChoiceWithCompletelyWrongSolutionTest ()
		{
			int userId = 1;
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				IPoints pointsCalculator = appHost.TryResolve<IPoints> ();
				using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
					//prepare the database, such that the challenge is solved without a speed bouns
					int challengeId = (int)db.Insert (new Challenge {
						Solution = new List<bool> { true, false }.ToJson (),
						SolutionType = SolutionType.MultipleChoice,
						BasePoints = 100,
					}, true);

					int challengeSetId = (int)db.Insert (new ChallengeSet { Challenges = new List<int>{ challengeId } }, true);
					int eventId = (int)db.Insert (new Event (), true);
					db.Insert (new EventChallengeSets {
						ChallengeSetId = challengeSetId,
						EventId = eventId,
						StartDate = DateTime.Now.Subtract (TimeSpan.FromSeconds(3)),
						EndDate = DateTime.Now.Add (TimeSpan.FromDays(7))
					});

					db.Insert (new EventUsers { EventId = eventId, UserId = userId });
					db.Insert (new ChallengeAccess () {
						AccessTime = DateTime.Now.Subtract (TimeSpan.FromSeconds(2)),
						ChallengeId = challengeId,
						Type = AccessType.Description,
						UserId = userId,
					});
					//insert a completely false solution
					db.Insert (new ChallengeSolution () {
						ChallengeId = challengeId,
						SolutionTime = DateTime.Now.Subtract (TimeSpan.FromSeconds(1)),
						Solved = true,
						SolutionEntered = new List<bool> { false, true }.ToJson (),
						UserId = userId,
						WrongEntries = 0
					});
					Assert.That (pointsCalculator.GetPoints (service, userId, challengeId), Is.EqualTo (0));				
				}
			}
		}

		[Test ()]
		public void GetPointsNoSpeedBonusForMultiPartWithCompletelyWrongSolutionTest ()
		{
			int userId = 1;
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				IPoints pointsCalculator = appHost.TryResolve<IPoints> ();
				using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
					//prepare the database, such that the challenge is solved without a speed bouns
					int challengeId = (int)db.Insert (new Challenge {
						Solution = new Dictionary<string, string> { {"foo", "bar"} }.ToJson (),
						SolutionType = SolutionType.MultiPart,
						BasePoints = 100,
					}, true);

					int challengeSetId = (int)db.Insert (new ChallengeSet { Challenges = new List<int>{ challengeId } }, true);
					int eventId = (int)db.Insert (new Event (), true);
					db.Insert (new EventChallengeSets {
						ChallengeSetId = challengeSetId,
						EventId = eventId,
						StartDate = DateTime.Now.Subtract (TimeSpan.FromSeconds(3)),
						EndDate = DateTime.Now.Add (TimeSpan.FromDays(7))
					});

					db.Insert (new EventUsers { EventId = eventId, UserId = userId });
					db.Insert (new ChallengeAccess () {
						AccessTime = DateTime.Now.Subtract (TimeSpan.FromSeconds(2)),
						ChallengeId = challengeId,
						Type = AccessType.Description,
						UserId = userId,
					});
					//insert a completely false solution
					db.Insert (new ChallengeSolution () {
						ChallengeId = challengeId,
						SolutionTime = DateTime.Now.Subtract (TimeSpan.FromSeconds(1)),
						Solved = true,
						SolutionEntered = new Dictionary<string, string> { {"foo", "bla"} }.ToJson (),
						UserId = userId,
						WrongEntries = 0
					});
					Assert.That (pointsCalculator.GetPoints (service, userId, challengeId), Is.EqualTo (0));				
				}
			}
		}

		[Test ()]
		public void GetPointsSolutionTooLateTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				IPoints pointsCalculator = appHost.TryResolve<IPoints> ();
				using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
					//prepare the database, such that the challenge is solved without a speed bouns
					db.UpdateNonDefaults (new EventChallengeSets {
						StartDate = new DateTime (2016, 01, 01, 12, 00, 00, DateTimeKind.Local),
						EndDate = new DateTime (2016, 01, 11, 12, 00, 00, DateTimeKind.Local),
					}, c => c.EventId == 1 && c.ChallengeSetId == 1);
					db.Insert (new ChallengeAccess () {
						AccessTime = new DateTime (2016, 01, 01, 12, 00, 00, DateTimeKind.Local),
						ChallengeId = 1,
						Type = AccessType.Description,
						UserId = 1
					});
					db.Insert (new ChallengeSolution () {
						ChallengeId = 1,
						SolutionTime = new DateTime (2016, 01, 11, 12, 01, 00, DateTimeKind.Local),
						Solved = true,
						UserId = 1,
						WrongEntries = 0
					});
				}
				Assert.That (pointsCalculator.GetPoints (service, 1, 1), Is.EqualTo (0));				
			}
		}

		[Test ()]
		public void GetPointsSpeedBonusTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				IPoints pointsCalculator = appHost.TryResolve<IPoints> ();
				using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
					//prepare the database, such that the challenge is solved without a speed bouns
					db.UpdateNonDefaults (new EventChallengeSets {
						StartDate = new DateTime (2016, 01, 01, 12, 00, 00, DateTimeKind.Local),
						EndDate = new DateTime (2016, 01, 11, 12, 00, 00, DateTimeKind.Local),
					}, c => c.EventId == 1 && c.ChallengeSetId == 1);
					db.Insert (new ChallengeAccess () {
						AccessTime = new DateTime (2016, 01, 01, 12, 00, 00, DateTimeKind.Local),
						ChallengeId = 1,
						Type = AccessType.Description,
						UserId = 1
					});
					db.Insert (new ChallengeSolution () {
						ChallengeId = 1,
						SolutionTime = new DateTime (2016, 01, 01, 12, 00, 01, DateTimeKind.Local),
						Solved = true,
						UserId = 1,
						WrongEntries = 0
					});

					//After just 1 second we should get the full speed bonus (40%)
					Assert.That (pointsCalculator.GetPoints (service, 1, 1), Is.EqualTo (140));

					//After half the speed bonus time (which is half of the challenge's duration) we should still get half of the speed bonus
					db.UpdateNonDefaults (new ChallengeSolution () {
						SolutionTime = new DateTime (2016, 01, 04, 00, 00, 00, DateTimeKind.Local),
						Solved = true,
						UserId = 1,
						WrongEntries = 0
					}, c => c.ChallengeId == 1);
					Assert.That (pointsCalculator.GetPoints (service, 1, 1), Is.EqualTo (120));

					//After fifth of the speed bonus time (which is half of the challenge's duration) we should still get a fifth of the speed bonus
					db.UpdateNonDefaults (new ChallengeSolution () {
						SolutionTime = new DateTime (2016, 01, 05, 12, 00, 00, DateTimeKind.Local),
						Solved = true,
						UserId = 1,
						WrongEntries = 0
					}, c => c.ChallengeId == 1);
					Assert.That (pointsCalculator.GetPoints (service, 1, 1), Is.EqualTo (108));

					//After half of the challenge's duration is expired, we should not get any speed bonus
					db.UpdateNonDefaults (new ChallengeSolution () {
						SolutionTime = new DateTime (2016, 01, 06, 12, 00, 01, DateTimeKind.Local),
						Solved = true,
						UserId = 1,
						WrongEntries = 0
					}, c => c.ChallengeId == 1);
					Assert.That (pointsCalculator.GetPoints (service, 1, 1), Is.EqualTo (100));
				}
			}
		}

		[Test ()]
		public void GetPointsSpeedBonusManualDatesTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				IPoints pointsCalculator = appHost.TryResolve<IPoints> ();
				using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
					//prepare the database, such that the challenge is solved without a speed bouns
					db.UpdateNonDefaults (new EventChallengeSets {
						StartDate = new DateTime (2016, 01, 01, 12, 00, 00, DateTimeKind.Local),
						EndDate = new DateTime (2016, 01, 11, 12, 00, 00, DateTimeKind.Local),
					}, c => c.EventId == 1 && c.ChallengeSetId == 1);

					//After just 1 second we should get the full speed bonus (40%)
					Assert.That (pointsCalculator.GetPoints (service, 1, 1, new DateTime (2016, 01, 01, 12, 00, 01, DateTimeKind.Local), new DateTime (2016, 01, 01, 12, 00, 00, DateTimeKind.Local), new int[0], new int[0]), Is.EqualTo (140));

					//After half the speed bonus time (which is half of the challenge's duration) we should still get half of the speed bonus
					Assert.That (pointsCalculator.GetPoints (service, 1, 1, new DateTime (2016, 01, 04, 00, 00, 00, DateTimeKind.Local), new DateTime (2016, 01, 01, 12, 00, 00, DateTimeKind.Local), new int[0], new int[0]), Is.EqualTo (120));

					//After fifth of the speed bonus time (which is half of the challenge's duration) we should still get a fifth of the speed bonus
					Assert.That (pointsCalculator.GetPoints (service, 1, 1, new DateTime (2016, 01, 05, 12, 00, 00, DateTimeKind.Local), new DateTime (2016, 01, 01, 12, 00, 00, DateTimeKind.Local), new int[0], new int[0]), Is.EqualTo (108));

					//After half of the challenge's duration is expired, we should not get any speed bonus
					Assert.That (pointsCalculator.GetPoints (service, 1, 1, new DateTime (2016, 01, 06, 12, 00, 01, DateTimeKind.Local), new DateTime (2016, 01, 01, 12, 00, 00, DateTimeKind.Local), new int[0], new int[0]), Is.EqualTo (100));
				}
			}
		}

		[Test ()]
		public void GetPointsSpeedBonusManualDatesMultipartQuestionWithoutSolutionTest ()
		{
			int userId = 1;
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				IPoints pointsCalculator = appHost.TryResolve<IPoints> ();
				using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
					//prepare the database, such that the challenge is solved without a speed bouns
					int challengeId = (int)db.Insert (new Challenge {
						Solution = new Dictionary<string, string> ().ToJson (),
						SolutionType = SolutionType.MultiPart,
						BasePoints = 100,
					}, true);

					int challengeSetId = (int)db.Insert (new ChallengeSet { Challenges = new List<int>{ challengeId } }, true);
					int eventId = (int)db.Insert (new Event (), true);
					DateTime startDate = DateTime.Now.Subtract (TimeSpan.FromDays (12));
					DateTime endDate = DateTime.Now.Add (TimeSpan.FromHours (1));
					db.Insert (new EventChallengeSets {
						ChallengeSetId = challengeSetId,
						EventId = eventId,
						StartDate = startDate,
						EndDate = endDate,
					});
					db.Insert (new EventUsers { EventId = eventId, UserId = userId });

					//After just 1 second we should get the full speed bonus (40%)
					Assert.That (pointsCalculator.GetPoints (service, userId, challengeId, startDate.Add (TimeSpan.FromSeconds (2)), startDate.Add (TimeSpan.FromSeconds (1)), new int[0], new int[0]), Is.EqualTo (140));

					//After half the speed bonus time (which is half of the challenge's duration) we should still get half of the speed bonus
					Assert.That (pointsCalculator.GetPoints (service, userId, challengeId, startDate.Add (TimeSpan.FromDays (3)), startDate.Add (TimeSpan.FromSeconds (1)), new int[0], new int[0]), Is.EqualTo (120));

					//1 Second before the end, we should not get a speed bonus
					Assert.That (pointsCalculator.GetPoints (service, userId, challengeId, endDate.Subtract (TimeSpan.FromSeconds (1)), startDate.Add (TimeSpan.FromSeconds (1)), new int[0], new int[0]), Is.EqualTo (100));
				}
			}
		}


		[Test ()]
		public void GetPointsSpeedBonusManualDatesWithHintsTest ()
		{
			int challengeId = 1;
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				IPoints pointsCalculator = appHost.TryResolve<IPoints> ();
				using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
					//prepare the database, such that the challenge is solved without a speed bouns
					db.UpdateNonDefaults (new EventChallengeSets {
						StartDate = new DateTime (2016, 01, 01, 12, 00, 00, DateTimeKind.Local),
						EndDate = new DateTime (2016, 01, 11, 12, 00, 00, DateTimeKind.Local),
					}, c => c.EventId == 1 && c.ChallengeSetId == 1);

					int hintId = (int)db.Insert (new Hint { ChallengeId = challengeId }, selectIdentity: true);

					//After just 1 second we should get the full speed bonus (40%)
					Assert.That (pointsCalculator.GetPoints (service, 1, challengeId, new DateTime (2016, 01, 01, 12, 00, 01, DateTimeKind.Local), 
						new DateTime (2016, 01, 01, 12, 00, 00, DateTimeKind.Local), new int[0], new int[0]), Is.EqualTo (140));

					//After just 1 second but with 1 hint used, we should not get any speed bonus
					Assert.That (pointsCalculator.GetPoints (service, 1, challengeId, new DateTime (2016, 01, 01, 12, 00, 01, DateTimeKind.Local), 
						new DateTime (2016, 01, 01, 12, 00, 00, DateTimeKind.Local), new int[]{ hintId }, new int[0]), Is.EqualTo (100));
				}
			}
		}


		[Test ()]
		public void GetPointsNoSpeedBonusWithHintsTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				IPoints pointsCalculator = appHost.TryResolve<IPoints> ();
				using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
					//prepare the database, such that the challenge is solved without a speed bouns
					db.UpdateNonDefaults (new EventChallengeSets {
						StartDate = new DateTime (2016, 01, 01, 12, 00, 00),
						EndDate = new DateTime (2016, 01, 11, 12, 00, 00),
					}, c => c.EventId == 1 && c.ChallengeSetId == 1);
					db.Insert (new ChallengeAccess () {
						AccessTime = new DateTime (2016, 01, 01, 12, 00, 00),
						ChallengeId = 1,
						Type = AccessType.Description,
						UserId = 1
					});
					db.Insert (new ChallengeSolution () {
						ChallengeId = 1,
						SolutionTime = new DateTime (2016, 01, 01, 12, 00, 02),
						Solved = true,
						UserId = 1,
						WrongEntries = 0
					});

					db.Insert (new Hint () {
						Id = 1,
						ChallengeId = 1,
						PointReduction = 0,
						Text = "Hint without point reduction"
					});
					db.Insert (new HintAccess () {
						HintId = 1,
						ChallengeId = 1,
						AccessTime = new DateTime (2016, 01, 01, 12, 00, 01),
						UserId = 1,
					});

					//We should loose our (otherwise) full speed bonus, because we used a hint
					Assert.That (pointsCalculator.GetPoints (service, 1, 1), Is.EqualTo (100));
				}

			}
		}

		[Test ()]
		public void GetPointsHintReductionTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				IPoints pointsCalculator = appHost.TryResolve<IPoints> ();
				using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
					//prepare the database, such that the challenge is solved without a speed bouns
					db.UpdateNonDefaults (new EventChallengeSets {
						StartDate = new DateTime (2016, 01, 01, 12, 00, 00),
						EndDate = new DateTime (2016, 01, 11, 12, 00, 00),
					}, c => c.EventId == 1 && c.ChallengeSetId == 1);
					db.Insert (new ChallengeAccess () {
						AccessTime = new DateTime (2016, 01, 01, 12, 00, 00),
						ChallengeId = 1,
						Type = AccessType.Description,
						UserId = 1
					});
					db.Insert (new ChallengeSolution () {
						ChallengeId = 1,
						SolutionTime = new DateTime (2016, 01, 02, 12, 00, 00),
						Solved = true,
						UserId = 1,
						WrongEntries = 0
					});

					db.Insert (new Hint () { Id = 1, ChallengeId = 1, PointReduction = 20 });
					db.Insert (new Hint () { Id = 2, ChallengeId = 1, PointReduction = 30 });
					db.Insert (new HintAccess () {
						HintId = 1,
						ChallengeId = 1,
						AccessTime = new DateTime (2016, 01, 01, 12, 00, 01),
						UserId = 1,
					});

					//We should loose 20% because we used 1 hint
					Assert.That (pointsCalculator.GetPoints (service, 1, 1), Is.EqualTo (80));

					db.Insert (new HintAccess () {
						HintId = 2,
						ChallengeId = 1,
						AccessTime = new DateTime (2016, 01, 01, 12, 00, 02),
						UserId = 1,
					});

					//We should loose 50% because we used 2 hints
					Assert.That (pointsCalculator.GetPoints (service, 1, 1), Is.EqualTo (50));
				}

			}
		}

		[Test ()]
		public void GetPointsNoHintReductionAfterSolutionTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				IPoints pointsCalculator = appHost.TryResolve<IPoints> ();
				using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
					//prepare the database, such that the challenge is solved without a speed bouns
					db.UpdateNonDefaults (new EventChallengeSets {
						StartDate = new DateTime (2016, 01, 01, 12, 00, 00),
						EndDate = new DateTime (2016, 01, 11, 12, 00, 00),
					}, c => c.EventId == 1 && c.ChallengeSetId == 1);
					db.Insert (new ChallengeAccess () {
						AccessTime = new DateTime (2016, 01, 01, 12, 00, 00),
						ChallengeId = 1,
						Type = AccessType.Description,
						UserId = 1
					});
					db.Insert (new ChallengeSolution () {
						ChallengeId = 1,
						SolutionTime = new DateTime (2016, 01, 01, 12, 00, 01),
						Solved = true,
						UserId = 1,
						WrongEntries = 0
					});

					//Insert a hint that was consumed after the challenge was already solved
					db.Insert (new Hint () { Id = 1, ChallengeId = 1, PointReduction = 20 });
					db.Insert (new HintAccess () {
						HintId = 1,
						ChallengeId = 1,
						AccessTime = new DateTime (2016, 01, 03, 12, 00, 01),
						UserId = 1,
					});

					//We should not have lost any points
					Assert.That (pointsCalculator.GetPoints (service, 1, 1), Is.EqualTo (140));
				}
			}
		}

		[Test ()]
		public void GetPointsNoSpeedBonusWithActionTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				IPoints pointsCalculator = appHost.TryResolve<IPoints> ();
				using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
					//prepare the database, such that the challenge is solved without a speed bouns
					db.UpdateNonDefaults (new EventChallengeSets {
						StartDate = new DateTime (2016, 01, 01, 12, 00, 00),
						EndDate = new DateTime (2016, 01, 11, 12, 00, 00),
					}, c => c.EventId == 1 && c.ChallengeSetId == 1);
					db.Insert (new ChallengeAccess () {
						AccessTime = new DateTime (2016, 01, 01, 12, 00, 00),
						ChallengeId = 1,
						Type = AccessType.Description,
						UserId = 1
					});
					db.Insert (new ChallengeSolution () {
						ChallengeId = 1,
						SolutionTime = new DateTime (2016, 01, 01, 12, 00, 02),
						Solved = true,
						UserId = 1,
						WrongEntries = 0
					});

					int actionId = (int)db.Insert (new ChallengeAction () {
						ChallengeId = 1,
						PointReduction = 0,
						RemoveBonus = true,
					}, selectIdentity: true);
					db.Insert (new ChallengeActionAccess () {
						ChallengeActionId = actionId,
						AccessTime = new DateTime (2016, 01, 01, 12, 00, 01),
						Nonce = (UInt64)new Random ().Next (0, int.MaxValue),
						UserId = 1,
					});

					//We should loose our (otherwise) full speed bonus, because we have an action
					Assert.That (pointsCalculator.GetPoints (service, 1, 1), Is.EqualTo (100));

					//Alter the action, so that it does not remove the speed bonus anymore
					db.Update (new ChallengeAction { Id = actionId, RemoveBonus = false });
					Assert.That (pointsCalculator.GetPoints (service, 1, 1), Is.EqualTo (140));
				}
			}
		}

		[Test ()]
		public void GetPointsPointReductionWithActionTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				IPoints pointsCalculator = appHost.TryResolve<IPoints> ();
				using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
					//prepare the database, such that the challenge is solved without a speed bouns
					db.UpdateNonDefaults (new EventChallengeSets {
						StartDate = new DateTime (2016, 01, 01, 12, 00, 00),
						EndDate = new DateTime (2016, 01, 11, 12, 00, 00),
					}, c => c.EventId == 1 && c.ChallengeSetId == 1);
					db.Insert (new ChallengeAccess () {
						AccessTime = new DateTime (2016, 01, 01, 12, 00, 00),
						ChallengeId = 1,
						Type = AccessType.Description,
						UserId = 1
					});
					db.Insert (new ChallengeSolution () {
						ChallengeId = 1,
						SolutionTime = new DateTime (2016, 01, 01, 12, 00, 02),
						Solved = true,
						UserId = 1,
						WrongEntries = 0
					});

					int actionId = (int)db.Insert (new ChallengeAction () {
						ChallengeId = 1,
						PointReduction = 50,
						RemoveBonus = true,
					}, selectIdentity: true);
					db.Insert (new ChallengeActionAccess () {
						ChallengeActionId = actionId,
						AccessTime = new DateTime (2016, 01, 01, 12, 00, 01),
						Nonce = (UInt64)new Random ().Next (0, int.MaxValue),
						UserId = 1,
					});

					//We should loose our (otherwise) full speed bonus plus 50%
					Assert.That (pointsCalculator.GetPoints (service, 1, 1), Is.EqualTo (50));

					//Alter the action, so that it does not remove the speed bonus anymore. We will still lose 50% of the base points but keep the speed bonus
					db.Update<ChallengeAction> (new {
						RemoveBonus = false,
					}, ca => ca.Id == actionId);
					Assert.That (pointsCalculator.GetPoints (service, 1, 1), Is.EqualTo (90));

					// Reduce the points furthe by a second action
					int action2Id = (int)db.Insert (new ChallengeAction () {
						ChallengeId = 1,
						PointReduction = 25,
						RemoveBonus = true,
					}, selectIdentity: true);
					db.Insert (new ChallengeActionAccess () {
						ChallengeActionId = action2Id,
						AccessTime = new DateTime (2016, 01, 01, 12, 00, 01),
						Nonce = (UInt64)new Random ().Next (0, int.MaxValue),
						UserId = 1,
					});
					Assert.That (pointsCalculator.GetPoints (service, 1, 1), Is.EqualTo (25));
				}
			}
		}


		[Test ()]
		public void GetPointsChallengeInMultipleEventsTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				IPoints pointsCalculator = appHost.TryResolve<IPoints> ();
				using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
					// Add a second event which also contains challenge 1 but user 1 is not part of that event
					db.Insert<Event> (new Event{ Id = 2 });
					db.Insert<ChallengeSet> (new ChallengeSet {
						Challenges = new List<int>{ 1 },
						Id = 2
					});
					db.Insert<EventChallengeSets> (new EventChallengeSets {
						ChallengeSetId = 2,
						EventId = 2,
						StartDate = DateTime.Now.Subtract (new TimeSpan (1, 0, 0)),
						EndDate = DateTime.Now.Add (new TimeSpan (24, 0, 0))
					});
					//the challenge is solved by user 1 with the full speed bonus (140 points total)
					db.UpdateNonDefaults (new EventChallengeSets {
						StartDate = new DateTime (2016, 01, 01, 12, 00, 00),
						EndDate = new DateTime (2016, 01, 11, 12, 00, 00),
					}, c => c.EventId == 1 && c.ChallengeSetId == 1);
					db.Insert (new ChallengeAccess () {
						AccessTime = new DateTime (2016, 01, 01, 12, 00, 00),
						ChallengeId = 1,
						Type = AccessType.Description,
						UserId = 1
					});
					db.Insert (new ChallengeSolution () {
						ChallengeId = 1,
						SolutionTime = new DateTime (2016, 01, 01, 12, 00, 01),
						Solved = true,
						UserId = 1,
						WrongEntries = 0
					});

					//We should not have lost any points
					Assert.That (pointsCalculator.GetPoints (service, 1, 1), Is.EqualTo (140));
				}
			}
		}
	}
}

