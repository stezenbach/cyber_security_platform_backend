﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using Hackathon.ServiceInterface.Points;

namespace Hackathon.ServiceInterface.Test.Points
{
	[TestFixture ()]
	public class MultiPartTest
	{

		[Test ()]
		[TestCase ("test LOWER", "test lower")]
		[TestCase (" test trim  \n", "test trim")]
		[TestCase ("  test trim and LOWER\t\t  \n  ", "test trim and lower")]
		[TestCase ("test,special-charß", "test special char ")]
		public void NormalizeTest (string s, string expected)
		{
			Assert.That (MultiPart.NormalizeAnswer (s), Is.EqualTo (expected), String.Format ("Normalization of '{0}' should be '{1}', but it is actually '{2}'.", s, expected, MultiPart.NormalizeAnswer (s)));
		}



		[Test ()]
		public void CalculatePointsTest ()
		{
			var solution = new Dictionary<string, string> { {
					"a",
					"answer a"
				}
			};

			var answers = solution;
			int calculatedPoints = MultiPart.CalculatePoints (answers, solution, 100);
			Assert.That (calculatedPoints, Is.EqualTo (100));

			solution = new Dictionary<string, string> {
				{ "a", "answer a" }, { "b", "answer b" }
			};
			answers = new Dictionary<string, string> {
				{ "b", "answer b" }, { "a", "answer a" }
			};
			calculatedPoints = MultiPart.CalculatePoints (answers, solution, 100);
			Assert.That (calculatedPoints, Is.EqualTo (100));

			solution = new Dictionary<string, string> {
				{ "a", "answer a" }, { "b", "answer b" }
			};
			answers = new Dictionary<string, string> { 
				{ "b", "answER B    " }, { "a", "answer abc" }
			};
			calculatedPoints = MultiPart.CalculatePoints (answers, solution, 100);
			Assert.That (calculatedPoints, Is.EqualTo (50));


		}
	}
}


