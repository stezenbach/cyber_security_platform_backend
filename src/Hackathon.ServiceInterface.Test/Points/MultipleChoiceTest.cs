﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using Hackathon.ServiceInterface.Points;

namespace Hackathon.ServiceInterface.Test.Points
{
	[TestFixture ()]
	public class MultipleChoiceTest
	{
		[Test ()]
		[TestCase(new bool[]{true}, new bool[]{true}, 100, 100)]
		[TestCase (new bool [] { false }, new bool [] { true }, 100, 0)]
		[TestCase (new bool [] { true, true }, new bool [] { true, false }, 100, 50)]
		[TestCase (new bool [] { false, false }, new bool [] { true, false }, 100, 50)]
		[TestCase (new bool [] { true, false }, new bool [] { true, false }, 100, 100)]
		[TestCase (new bool [] { false, true }, new bool [] { true, false }, 100, 0)]
		[TestCase (new bool [] { true, false, true }, new bool [] { true, false, true }, 60, 60)]
		[TestCase (new bool [] { true, false, false }, new bool [] { true, false, true }, 60, 40)]
		[TestCase (new bool [] { true, true, false }, new bool [] { true, false, true }, 60, 20)]
		[TestCase (new bool [] { false, true, false }, new bool [] { true, false, true }, 60, 0)]
		public void CalculatePointsTest (IEnumerable<bool> answers, IEnumerable<bool> solution, int maxPoints, int expectedPoints)
		{
			int calculatedPoints = MultipleChoice.CalculatePoints (answers, solution, maxPoints);
			Assert.That (calculatedPoints, Is.EqualTo (expectedPoints));
		}
	}
}

