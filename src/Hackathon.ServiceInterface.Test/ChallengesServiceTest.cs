﻿//
//  ChallengesServiceTest.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using NUnit.Framework;
using System;
using System.Linq;
using ServiceStack.Data;
using Hackathon.ServiceModel.Types;
using ServiceStack;
using Hackathon.ServiceModel;
using ServiceStack.Testing;
using ServiceStack.OrmLite;
using ServiceStack.Auth;
using System.Collections.Generic;
using System.Net;
using Hackathon.ServiceInterface.Points;
using System.Text.RegularExpressions;
using System.Security.Cryptography;

namespace Hackathon.ServiceInterface.Test
{
	[TestFixture ()]
	public class ChallengesServiceTest
	{
		private class TestAppHost : BasicAppHost
		{
			public TestAppHost () : base (typeof(ChallengesService).Assembly)
			{
			}

			public override void Configure (Funq.Container container)
			{
				container.Register<IDbConnectionFactory> (c =>
					new OrmLiteConnectionFactory (":memory:", SqliteDialect.Provider)
				);

				container.Register<IAuthSession> (c => new AuthUserSession {
					UserAuthName = "foobar@mailinator.com",
					UserName = "foobar",
					UserAuthId = "1",
				});

				container.RegisterAs<DbPoints, IPoints> ();

				byte[] userTokenKey = new byte[]{ 0x31, 0x32, 0x33 };
				HMACSHA256 userTokenCalculator = new HMACSHA256 (userTokenKey);
				container.Register<HMAC> (userTokenCalculator);

				using (var db = container.TryResolve<IDbConnectionFactory> ().Open ()) {
					db.DropAndCreateTable<Challenge> ();
					db.DropAndCreateTable<ChallengeSet> ();
					db.DropAndCreateTable<Event> ();
					db.DropAndCreateTable<EventUsers> ();
					db.DropAndCreateTable<EventChallengeSets> ();
					db.DropAndCreateTable<Hint> ();
					db.DropAndCreateTable<User> ();
					db.DropAndCreateTable<ChallengeAccess> ();
					db.DropAndCreateTable<HintAccess> ();
					db.DropAndCreateTable<ChallengeSolution> ();
					db.DropAndCreateTable<ChallengeAction> ();
					db.DropAndCreateTable<ChallengeActionAccess> ();

					db.Insert (new Challenge {
						Id = 1,
						Title = "foobar",
						Description = "Foo bar foo bar.",
						SolutionType = SolutionType.Static,
						SolutionInfo = "",
						Solution = "This is it!",
						BasePoints = 100,
					});

					db.Insert (new Challenge {
						Id = 2,
						Title = "Another challenge",
					});

					db.Insert (new Challenge {
						Id = 3,
						Title = "A multiple choice challenge",
						SolutionType = SolutionType.MultipleChoice,
						SolutionInfo = new List<string>{ "Option 1", "Option 2", "Option 3" }.ToJson (),
						Solution = new List<bool>{ true, true, false }.ToJson (),
						BasePoints = 100,
					});

					db.Insert (new Challenge {
						Id = 4,
						Title = "A multipart challenge",
						SolutionType = SolutionType.MultiPart,
						SolutionInfo = new Dictionary<string, string> {
							{ "a", "foo" }, {
								"b",
								"bar"
							}
						}.ToJson (),
						Solution = new Dictionary<string, string> {
							{ "a", "answer" }, {
								"b",
								"answer2"
							}
						}.ToJson (),
						BasePoints = 100,
					});

					db.Insert (new ChallengeSet {
						Id = 1,
						Challenges = new List<int> { 1, 3, 4 }
					});

					db.Insert (new Event {
						Id = 1
					});

					db.Insert (new EventChallengeSets {
						ChallengeSetId = 1,
						EventId = 1,
						StartDate = DateTime.Now,
						EndDate = DateTime.Now.Add (new TimeSpan (1, 0, 0)),
					});

					db.Insert (new EventUsers {
						EventId = 1,
						UserId = 1,
					});

					db.Insert (new Hint {
						Id = 1,
						ChallengeId = 2,
						Text = "Hint for challenge 2"
					});

					db.Insert (new Hint {
						Id = 2,
						ChallengeId = 1,
						Text = "Hint #1"
					});

					db.Insert (new Hint {
						Id = 3,
						ChallengeId = 1,
						Text = "The second hint"
					});

					db.Insert (new User {
						Id = 1,
						UserName = "foobar",
						FirstName = "Foo",
						LastName = "bar",
						Avatar = null,
						Salt = "foobar",
						MailAddress = "foobar@mailinator.com",
						//pw=123456, created with echo -n "foobar123456"|sha256sum, then base64 encoded
						Password = @"V7yttgjbKKO8rKp6OTiHqQ8BCSEKqjaGgBtun4lWjfs=",
					});

				}
			}
		}

		private ServiceStackHost appHost;

		[SetUp]
		public void SetupChallengesServiceTest ()
		{
			try {
				appHost = new TestAppHost ();
				appHost.Init ();
			} catch (Exception e) {
				Assert.Fail ("Apphost startup failed: {0}", e.ToString ());
			}
		}

		//[TestFixtureTearDown]
		[TearDown]
		public void TestFixtureTearDown ()
		{
			appHost.Dispose ();
		}


		[Test ()]
		public void GetExistingChallengeTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				var challenge = service.Get (new FindChallenges (){ Id = 1 });

				Assert.That (challenge.Title, Is.EqualTo ("foobar"));
				Assert.That (challenge.Id, Is.EqualTo (1));
				Assert.That (challenge.SolutionType, Is.EqualTo (SolutionType.Static));
				Assert.That (challenge.SolutionInfo, Is.EqualTo (""));
				Assert.That (challenge.BasePoints, Is.EqualTo (100));
			}
		}

		[Test ()]
		public void GetExistingChallengeNotYetActiveTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				service.Db.Insert<Event> (new Event{ Id = 2 });
				int challengeId = (int)service.Db.Insert<Challenge> (new Challenge (), true);
				int challengeSetId = (int)service.Db.Insert<ChallengeSet> (new ChallengeSet {
					Challenges = new List<int>{ challengeId }
				}, true);
				int eventId = (int)service.Db.Insert<EventChallengeSets> (new EventChallengeSets {
					ChallengeSetId = challengeSetId,
					StartDate = DateTime.Now.AddDays (1),
					EndDate = DateTime.Now.AddDays (2)
				});
				service.Db.Insert<EventUsers> (new EventUsers {
					EventId = eventId,
					UserId = 1
				});

				try {
					service.Get (new FindChallenges (){ Id = challengeId });
					Assert.Fail ("Retrieving a challenge which is not active yet should fail.");
				} catch (HttpError e) {
					Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.Forbidden));
				}
			}
		}

		[Test ()]
		public void GetExistingChallengeExpiredTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				service.Db.Insert<Event> (new Event{ Id = 2 });
				service.Db.Insert<Challenge> (new Challenge{ Id = 3 });
				service.Db.Insert<ChallengeSet> (new ChallengeSet {
					Id = 2,
					Challenges = new List<int>{ 3 }
				});
				service.Db.Insert<EventChallengeSets> (new EventChallengeSets {
					EventId = 2,
					ChallengeSetId = 2,
					StartDate = DateTime.Now.Subtract (new TimeSpan (2, 0, 0, 0)),
					EndDate = DateTime.Now.Subtract (new TimeSpan (1, 0, 0, 0)),
				});
				service.Db.Insert<EventUsers> (new EventUsers{ EventId = 2, UserId = 1 });

				//Retrieving this challenge should work without a problem
				service.Get (new FindChallenges (){ Id = 3 });
			}
		}

		[Test ()]
		public void GetExistingChallengeNotInEventOfUser ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				try {
					service.Get (new FindChallenges (){ Id = 2 });
					Assert.Fail ("Accessing a challenge no availabe in a user event should not be allowed.");
				} catch (HttpError e) {
					Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.Forbidden));
				}
			}
		}

		[Test ()]
		public void GetChallengeDescriptionNotInEventOfUser ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				try {
					service.Get (new GetChallengeDescription (){ Id = 2 });
					Assert.Fail ("Accessing a challenge description no availabe in a user event should not be allowed.");
				} catch (HttpError e) {
					Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.Forbidden));
				}
			}
		}

		[Test ()]
		public void GetNonExistingChallengeTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				try {
					service.Get (new FindChallenges (){ Id = 666 });
					Assert.Fail ("Should throw on lookup up non existing ID");
				} catch (HttpError e) {
					// The return code will be Forbidden, since an inexisting event is definitely not part of a user's event
					Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.Forbidden));
				}
			}
		}

		[Test ()]
		public void GetChallengeDescriptionTest ()
		{
			int challengeId = 1;
			int userId = 1; //from the mocked session definition in the apphost's configure methode
			using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
				Assert.That (db.Select<ChallengeAccess> ().Where (a => (a.ChallengeId == challengeId && a.UserId == userId && a.Type == AccessType.Description)).Any (), Is.False);
				using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
					string description = service.Get (new GetChallengeDescription (){ Id = challengeId }).Description;
					Assert.That (description, Is.EqualTo ("Foo bar foo bar."));
					//Now check if the access time has been set
					DateTime accessTime = db.Select<ChallengeAccess> ().Where (a => (a.ChallengeId == challengeId && a.UserId == userId && a.Type == AccessType.Description)).First ().AccessTime;
					Assert.That (DateTime.Now.Subtract (accessTime).TotalSeconds, Is.LessThan (1));
					//Check that the access time does not change on another request
					service.Get (new GetChallengeDescription (){ Id = challengeId });
					DateTime secondAccessTime = db.Select<ChallengeAccess> ().Where (a => (a.ChallengeId == challengeId && a.UserId == userId && a.Type == AccessType.Description)).First ().AccessTime;
					Assert.That (accessTime, Is.EqualTo (secondAccessTime));
				}
			}
		}

		[Test ()]
		public void GetChallengeSolutionTest ()
		{
			int userId = 1; //from the mocked session definition in the apphost's configure methode
			var correctSolution = new List<bool>{ true, true };
			SolutionType correctSolutionType = SolutionType.MultipleChoice;
			using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
				// An expired ChallengeSet
				int activeChallengeId = (int)db.Insert (new Challenge {
					Title = "This challegene is still active",
					SolutionType = SolutionType.MultipleChoice,
					SolutionInfo = new List<string>{ "Option 1", "Option 2" }.ToJson (),
					Solution = new List<bool>{ false, true }.ToJson (),
				}, selectIdentity: true);

				int expiredChallengeId = (int)db.Insert (new Challenge {
					SolutionType = correctSolutionType,
					SolutionInfo = new List<string>{ "Option 1", "Option 2" }.ToJson (),
					Solution = correctSolution.ToJson (),
					SolutionExplanation = "This is true because ...",
				}, selectIdentity: true);

				int activeChallengeSetId = (int)db.Insert (new ChallengeSet {
					Challenges = new List<int> { activeChallengeId }
				}, selectIdentity: true);

				int expiredChallengeSetId = (int)db.Insert (new ChallengeSet {
					//inluce two challenges. One which is also available in another (running) event and one which is really expired
					Challenges = new List<int> { activeChallengeId, expiredChallengeId }
				}, selectIdentity: true);

				int eventId = (int)db.Insert (new Event (), selectIdentity: true);

				db.Insert (new EventChallengeSets {
					ChallengeSetId = expiredChallengeSetId,
					EventId = eventId,
					StartDate = DateTime.Now.Subtract (new TimeSpan (2, 0, 0)),
					EndDate = DateTime.Now.Subtract (new TimeSpan (1, 0, 0)),
				});

				db.Insert (new EventChallengeSets {
					ChallengeSetId = activeChallengeSetId,
					EventId = eventId,
					StartDate = DateTime.Now.Subtract (new TimeSpan (2, 0, 0)),
					EndDate = DateTime.Now.Add (new TimeSpan (1, 0, 0)),
				});

				db.Insert (new EventUsers {
					EventId = eventId,
					UserId = userId,
				});

				int unavailableChallengeId = (int)db.Insert (new Challenge {
					SolutionType = SolutionType.Static,
					Solution = "foo",
				}, selectIdentity: true);
				int unavailableChallengeSetId = (int)db.Insert (new ChallengeSet {
					Challenges = new List<int> { unavailableChallengeId }
				}, selectIdentity: true);
				int unavailableEventId = (int)db.Insert (new Event (), selectIdentity: true);

				db.Insert (new EventChallengeSets {
					ChallengeSetId = unavailableChallengeSetId,
					EventId = unavailableEventId,
					//Make it expired
					StartDate = DateTime.Now.Subtract (new TimeSpan (2, 0, 0)),
					EndDate = DateTime.Now.Subtract (new TimeSpan (1, 0, 0)),
				});

				using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
					//Retrieve the solution of the expired challenge now
					var solution = service.Get (new GetChallengeSolution (){ Id = expiredChallengeId });
					Assert.That (solution.SolutionType, Is.EqualTo (correctSolutionType), "Solution retrieved from service has bad type");
					Assert.That (solution.Solution.FromJson < List<bool>> ().SequenceEqual (correctSolution), "Solution retreived from service is not as expected.");
					Assert.That (solution.SolutionExplanation, Is.EqualTo ("This is true because ..."));
					//Now check if the access time has been set
					DateTime accessTime = db.Select<ChallengeAccess> ().Where (a => (a.ChallengeId == expiredChallengeId && a.UserId == userId && a.Type == AccessType.Solution)).First ().AccessTime;
					Assert.That (DateTime.Now.Subtract (accessTime).TotalSeconds, Is.LessThan (1));
					//Check that the access time does not change on another request
					service.Get (new GetChallengeSolution (){ Id = expiredChallengeId });
					DateTime secondAccessTime = db.Select<ChallengeAccess> ().Where (a => (a.ChallengeId == expiredChallengeId && a.UserId == userId && a.Type == AccessType.Solution)).First ().AccessTime;
					Assert.That (accessTime, Is.EqualTo (secondAccessTime));

					//Retrieving the soultion to a challenge which is also in an active challengeset should not work
					Assert.Throws<HttpError> (() => service.Get (new GetChallengeSolution () { Id = activeChallengeId }));
					//No access should have been logged for this challenge
					Assert.That (db.Select<ChallengeAccess> ().Where (a => (a.ChallengeId == activeChallengeId && a.UserId == userId && a.Type == AccessType.Solution)).Any (), Is.False);

					//Retrieving the soultion to a challenge which is no available in any challengeset
					Assert.Throws<HttpError> (() => service.Get (new GetChallengeSolution () { Id = unavailableChallengeId }));
					//No access should have been logged for this challenge
					Assert.That (db.Select<ChallengeAccess> ().Where (a => (a.ChallengeId == unavailableChallengeId && a.UserId == userId && a.Type == AccessType.Solution)).Any (), Is.False);
				}
			}
		}

		[Test ()]
		public void GetChallengeSolutionEnteredTest ()
		{
			int userId = 1; //from the mocked session definition in the apphost's configure methode
			using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
				// An expired ChallengeSet
				int activeChallengeId = (int)db.Insert (new Challenge {
					SolutionType = SolutionType.Static,
					Solution = "foo",
				}, selectIdentity: true);

				int activeChallengeSetId = (int)db.Insert (new ChallengeSet {
					Challenges = new List<int> { activeChallengeId }
				}, selectIdentity: true);

				int eventId = (int)db.Insert (new Event (), selectIdentity: true);

				db.Insert (new EventChallengeSets {
					ChallengeSetId = activeChallengeSetId,
					EventId = eventId,
					StartDate = DateTime.Now.Subtract (new TimeSpan (2, 0, 0)),
					EndDate = DateTime.Now.Add (new TimeSpan (1, 0, 0)),
				});

				db.Insert (new EventUsers {
					EventId = eventId,
					UserId = userId,
				});

				using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
					// Retrieve the solution entered without havin posted a solution, yet
					var solutionEntered = service.Get (new GetChallengeSolutionEntered (){ Id = activeChallengeId });
					Assert.That (solutionEntered.SolutionType, Is.EqualTo (SolutionType.Static), "Solution retrieved from service has bad type");
					Assert.That (solutionEntered.SolutionEntered, Is.Null);

					//Now post a bad solution
					string badSolution = "bla";
					Assert.Throws<HttpError> (() => service.Post (new PostSolution {
						Id = activeChallengeId,
						SolutionType = SolutionType.Static,
						Solution = badSolution
					}));
					solutionEntered = service.Get (new GetChallengeSolutionEntered (){ Id = activeChallengeId });
					Assert.That (solutionEntered.SolutionType, Is.EqualTo (SolutionType.Static), "Solution retrieved from service has bad type");
					Assert.That (solutionEntered.SolutionEntered, Is.EqualTo (badSolution));

					//Now post another bad solution
					string badSolution2 = "bla2";
					Assert.Throws<HttpError> (() => service.Post (new PostSolution {
						Id = activeChallengeId,
						SolutionType = SolutionType.Static,
						Solution = badSolution2
					}));
					solutionEntered = service.Get (new GetChallengeSolutionEntered (){ Id = activeChallengeId });
					Assert.That (solutionEntered.SolutionType, Is.EqualTo (SolutionType.Static), "Solution retrieved from service has bad type");
					Assert.That (solutionEntered.SolutionEntered, Is.EqualTo (badSolution2));

					//Now post the correct solution
					service.Post (new PostSolution {
						Id = activeChallengeId,
						SolutionType = SolutionType.Static,
						Solution = "foo",
					});
					solutionEntered = service.Get (new GetChallengeSolutionEntered (){ Id = activeChallengeId });
					Assert.That (solutionEntered.SolutionType, Is.EqualTo (SolutionType.Static), "Solution retrieved from service has bad type");
					Assert.That (solutionEntered.SolutionEntered, Is.EqualTo ("foo"));
				}
			}
		}

		[Test ()]
		public void GetChallengeSolutionEnteredChallengeNotAvailableTest ()
		{
			using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
				int unavailableChallengeId = (int)db.Insert (new Challenge {
					SolutionType = SolutionType.Static,
					Solution = "foo",
				}, selectIdentity: true);
				int unavailableChallengeSetId = (int)db.Insert (new ChallengeSet {
					Challenges = new List<int> { unavailableChallengeId }
				}, selectIdentity: true);
				int unavailableEventId = (int)db.Insert (new Event (), selectIdentity: true);

				db.Insert (new EventChallengeSets {
					ChallengeSetId = unavailableChallengeSetId,
					EventId = unavailableEventId,
					//Make it expired
					StartDate = DateTime.Now.Subtract (new TimeSpan (2, 0, 0)),
					EndDate = DateTime.Now.Subtract (new TimeSpan (1, 0, 0)),
				});

				using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
					// Retrieve the solution entered without havin posted a solution, yet
					try {
						service.Get (new GetChallengeSolutionEntered (){ Id = unavailableChallengeId });
						Assert.Fail ("Reading the entered solution for an unavailable challenge should not be allowed.");
					} catch (HttpError e) {
						Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.Forbidden));
					}
				}
			}
		}

		[Test ()]
		public void GetChallengeDescriptionWithUserTokenReplacementTest ()
		{
			int challengeId = 1;
			using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
				db.UpdateNonDefaults<Challenge> (new Challenge (){ Description = "foo {{{UserToken-abcde}}} bar" }, c => c.Id == challengeId);
				using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
					string description = service.Get (new GetChallengeDescription (){ Id = challengeId }).Description;
					Match findToken = Regex.Match (description, @"^foo ([0-9a-zA-Z]+) bar$");
					Assert.That (findToken.Success);
					// HMAC Key (defined in the apphost ist {0x31, 0x32, 0x33}, HMAC-SHA256 should be calculated over token||username -> "abcdefoobar"
					// echo -n "abcdefoobar" |openssl dgst -mac hmac -sha256 -macopt key:123 -binary|base64
					//     XfReLAdjo/6ZT3jIPakMl/JqFHRo2yqFr0kpb86MEHM=
					//remove special characters from the string
					Assert.That (findToken.Groups [1].Value, Is.EqualTo ("XfReLAdjo6ZT3jIPakMlJqFHRo2yqFr0kpb86MEHM"));
				}
			}
		}

		[Test ()]
		public void GetChallengeDescriptionWithUserIdTokenReplacementTest ()
		{
			/*Test values:
			 *
			 * Key derivation:
			 *    > $ echo -n "1"|openssl dgst -hmac "{{{UserIdToken-abcdef}}}" -sha256
			 *      (stdin)= c4804a655fd12a0b2f327e6a0cbe6fae8b0a650910c4a51486cbd3f3c2aa34fc
             *
             *Encryption:
             *   > $ echo -n "\x01"| openssl enc -e -aes-256-ecb  -K c4804a655fd12a0b2f327e6a0cbe6fae8b0a650910c4a51486cbd3f3c2aa34fc  -base64
                   iIB5q3HMGFBmlVZrjZSuFQ==

             * Tag:
             * > $ echo "iIB5q3HMGFBmlVZrjZSuFQ==" |base64 -d|openssl dgst -mac hmac -macopt hexkey:c4804a655fd12a0b2f327e6a0cbe6fae8b0a650910c4a51486cbd3f3c2aa34fc -binary|base64
                fXAyydOmvmRHpkrfFCjk2boUDKYfaCH5Hj0ExJhfhI4=
             *
             * > $ echo "iIB5q3HMGFBmlVZrjZSuFQ==fXAyydOmvmRHpkrfFCjk2boUDKYfaCH5Hj0ExJhfhI4="|base64 -d|base64
               iIB5q3HMGFBmlVZrjZSuFX1wMsnTpr5kR6ZK3xQo5Nm6FAymH2gh+R49BMSYX4SO

             * Urlencode (http://www.albionresearch.com/misc/urlencode.php):
             *  -> iIB5q3HMGFBmlVZrjZSuFX1wMsnTpr5kR6ZK3xQo5Nm6FAymH2gh%2BR49BMSYX4SO
             */

			int challengeId = 1;
			using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
				db.UpdateNonDefaults<Challenge> (new Challenge (){ Description = "foo {{{UserIdToken-abcdef}}} bar" }, c => c.Id == challengeId);
				using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
					string description = service.Get (new GetChallengeDescription (){ Id = challengeId }).Description;
					Match findToken = Regex.Match (description, @"^foo ([0-9a-zA-Z%]+) bar$");
					Assert.That (findToken.Success);
					Assert.That (findToken.Groups [1].Value, Is.EqualTo ("iIB5q3HMGFBmlVZrjZSuFX1wMsnTpr5kR6ZK3xQo5Nm6FAymH2gh%2bR49BMSYX4SO"));
				}
			}
		}

		[Test ()]
		public void GetChallengeDescriptionNonExistingTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				try {
					service.Get (new GetChallengeDescription (){ Id = 666 });
					Assert.Fail ("Should throw on lookup of non existing ID");
				} catch (HttpError e) {
					// The return code will be Forbidden, since an inexisting event is definitely not part of a user's event
					Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.Forbidden));
				}
			}
		}

		[Test ()]
		public void GetChallengeDescriptionExpiredTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				service.Db.Insert<Event> (new Event{ Id = 2 });
				int challengeId = (int)service.Db.Insert<Challenge> (new Challenge (){ Description = "The Description" }, selectIdentity: true);
				service.Db.Insert<ChallengeSet> (new ChallengeSet {
					Id = 2,
					Challenges = new List<int>{ challengeId }
				});
				service.Db.Insert<EventChallengeSets> (new EventChallengeSets {
					EventId = 2,
					ChallengeSetId = 2,
					StartDate = DateTime.Now.Subtract (new TimeSpan (2, 0, 0, 0)),
					EndDate = DateTime.Now.Subtract (new TimeSpan (1, 0, 0, 0)),
				});
				service.Db.Insert<EventUsers> (new EventUsers{ EventId = 2, UserId = 1 });

				//Retrieving this challenge's description should work without a problem
				service.Get (new GetChallengeDescription (){ Id = challengeId });
				//Check if the access time has been set
				DateTime accessTime = service.Db.Select<ChallengeAccess> ().Where (a => (a.ChallengeId == challengeId && a.UserId == 1 && a.Type == AccessType.Description)).First ().AccessTime;
				Assert.That (DateTime.Now.Subtract (accessTime).TotalSeconds, Is.LessThan (1));
			}
		}

		[Test ()]
		public void GetChallengeHintsTest ()
		{
			int challengeId = 1;
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				var hints = service.Get (new GetChallengeHints (){ Id = challengeId });
				Assert.That (hints.Count (), Is.EqualTo (2));
			}
		}

		[Test ()]
		public void GetChallengeHintsNonExistingChallengeTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				try {
					service.Get (new GetChallengeHints (){ Id = 666 });
					Assert.Fail ("Should throw on lookup up non existing ID");
				} catch (HttpError e) {
					// Return code is Forbidden, beacause a non-existing challenge is no accesible by the user
					Assert.That (e.StatusCode, Is.EqualTo (System.Net.HttpStatusCode.Forbidden));
				}
			}
		}

		[Test ()]
		public void GetChallengeHintTest ()
		{
			int challengeId = 1;
			int userId = 1; //from the mocked session definition in the apphost's configure methode
			using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
				using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
					int hintId = service.Get (new GetChallengeHints{ Id = challengeId }).Last (); //test the second hint
					Assert.That (db.Select<HintAccess> ().Where (a => (a.ChallengeId == challengeId && a.UserId == userId && a.HintId == hintId)).Any (), Is.False);
					string hint = service.Get (new GetChallengeHint () {
						Id = challengeId,
						HintNumber = hintId
					}).Text;
					Assert.That (hint, Is.EqualTo ("The second hint"));
					//Now check if the access time has been set
					DateTime accessTime = db.Select<HintAccess> ().Where (a => (a.ChallengeId == challengeId && a.UserId == userId && a.HintId == hintId)).First ().AccessTime;
					Assert.That (DateTime.Now.Subtract (accessTime).TotalSeconds, Is.LessThan (1));
					//Check that the access time does not change on another request
					service.Get (new GetChallengeHint () {
						Id = challengeId,
						HintNumber = hintId
					});
					DateTime secondAccessTime = db.Select<HintAccess> ().Where (a => (a.ChallengeId == challengeId && a.UserId == userId && a.HintId == hintId)).First ().AccessTime;
					Assert.That (accessTime, Is.EqualTo (secondAccessTime));
				}
			}
		}

		[Test ()]
		public void GetChallengeHintsNonExistingHintTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				try {
					service.Get (new GetChallengeHint (){ Id = 1, HintNumber = 666 });
					Assert.Fail ("Should throw on lookup up non existing ID");
				} catch (HttpError e) {
					Assert.That (e.StatusCode, Is.EqualTo (System.Net.HttpStatusCode.NotFound));
				}
			}
		}

		[Test ()]
		public void PostSolutionTest ()
		{
			string solution = "This is it!";
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				//store out first access to the challenge in the db
				service.Db.Insert<ChallengeAccess> (new ChallengeAccess {
					ChallengeId = 1,
					UserId = 1,
					Type = AccessType.Description,
					AccessTime = DateTime.Now
				});
				service.Post (new PostSolution {
					Id = 1,
					SolutionType = SolutionType.Static,
					Solution = solution
				});
				using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
					var solutionInDb = db.Select<ChallengeSolution> (c => (c.ChallengeId == 1 && c.UserId == 1)).First ();
					Assert.That (solutionInDb.Solved);
					Assert.That (DateTime.Now.Subtract (solutionInDb.SolutionTime).TotalSeconds, Is.LessThan (1));
					Assert.That (solutionInDb.Points.Value, Is.GreaterThan (0), "A positive point value should be stored in the database after posting a correct solution");
				}
			}
		}

		[Test ()]
		public void PostMultipleChoiceSolutionTest ()
		{
			List<bool> solution = new List<bool>{ true, true, false };
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				//store out first access to the challenge in the db
				service.Db.Insert<ChallengeAccess> (new ChallengeAccess {
					ChallengeId = 3,
					UserId = 1,
					Type = AccessType.Description,
					AccessTime = DateTime.Now
				});
				service.Post (new PostSolution {
					Id = 3,
					SolutionType = SolutionType.MultipleChoice,
					Solution = solution.ToJson (),
				});
				using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
					var solutionInDb = db.Select<ChallengeSolution> (c => (c.ChallengeId == 3 && c.UserId == 1)).First ();
					Assert.That (solutionInDb.Solved);
					Assert.That (DateTime.Now.Subtract (solutionInDb.SolutionTime).TotalSeconds, Is.LessThan (1));
					Assert.That (solutionInDb.Points.Value, Is.GreaterThan (0), "A positive point value should be stored in the database after posting a correct solution");
				}
			}
		}

		[Test ()]
		public void PostMultiPartSolutionTest ()
		{
			int challengeId = 4;
			var solution = new Dictionary<string, string> {
				{ "a", " ANSWER " }, {
					"b",
					"answer2"
				}
			};
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				//store out first access to the challenge in the db
				service.Db.Insert<ChallengeAccess> (new ChallengeAccess {
					ChallengeId = challengeId,
					UserId = 1,
					Type = AccessType.Description,
					AccessTime = DateTime.Now
				});
				service.Post (new PostSolution {
					Id = challengeId,
					SolutionType = SolutionType.MultiPart,
					Solution = solution.ToJson (),
				});
				using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
					var solutionInDb = db.Select<ChallengeSolution> (c => (c.ChallengeId == challengeId && c.UserId == 1)).First ();
					Assert.That (solutionInDb.Solved);
					Assert.That (DateTime.Now.Subtract (solutionInDb.SolutionTime).TotalSeconds, Is.LessThan (1));
					Assert.That (solutionInDb.Points.Value, Is.GreaterThan (0), "A positive point value should be stored in the database after posting a correct solution");
				}
			}
		}

		[Test ()]
		public void PostMultipleChoiceSolutionTwiceTest ()
		{
			List<bool> solution = new List<bool>{ true, true, false };
			List<bool> badSolution = new List<bool>{ true, false, false };
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				//store out first access to the challenge in the db
				service.Db.Insert<ChallengeAccess> (new ChallengeAccess {
					ChallengeId = 3,
					UserId = 1,
					Type = AccessType.Description,
					AccessTime = DateTime.Now
				});
				//We should be allowed to post a solution
				var mayEnter = service.Get (new GetChallengeSolutionMayEnter{ Id = 3 });
				Assert.That (mayEnter.MayEnterSolution, Is.True);
				Assert.That (mayEnter.Solved, Is.False);
				Assert.That (mayEnter.Locked, Is.False);
				//Post a bad solution first
				Assert.Throws<HttpError> (() => service.Post (new PostSolution {
					Id = 3,
					SolutionType = SolutionType.MultipleChoice,
					Solution = badSolution.ToJson (),
				}));
				//We should not be allowed to post a solution anymore
				mayEnter = service.Get (new GetChallengeSolutionMayEnter{ Id = 3 });
				Assert.That (mayEnter.MayEnterSolution, Is.False);
				Assert.That (mayEnter.Solved, Is.True);
				Assert.That (mayEnter.Locked, Is.False);
				//After having posted a bad solution to a multiple choice question, posting the correct solution should not work anymore
				try {
					service.Post (new PostSolution {
						Id = 3,
						SolutionType = SolutionType.MultipleChoice,
						Solution = solution.ToJson (),
					});
					Assert.Fail ("Posting a solution twice for a multiple choice question should not work.");
				} catch (HttpError e) {
					Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.Conflict));
				}
			}
		}

		[Test ()]
		public void PostMultiPartSolutionTwiceTest ()
		{
			int challengeId = 4;
			var solution = new Dictionary<string, string> {
				{ "a", " ANSWER " }, {
					"b",
					"answer2"
				}
			};
			var badSolution = new Dictionary<string, string> {
				{ "a", " wrong ANSWER " }, {
					"b",
					"answer2"
				}
			};
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				//store out first access to the challenge in the db
				service.Db.Insert<ChallengeAccess> (new ChallengeAccess {
					ChallengeId = challengeId,
					UserId = 1,
					Type = AccessType.Description,
					AccessTime = DateTime.Now
				});
				//Post a bad solution first
				Assert.Throws<HttpError> (() => service.Post (new PostSolution {
					Id = challengeId,
					SolutionType = SolutionType.MultiPart,
					Solution = badSolution.ToJson (),
				}));
				//We should not be allowed to post a solution anymore
				var mayEnter = service.Get (new GetChallengeSolutionMayEnter{ Id = challengeId });
				Assert.That (mayEnter.MayEnterSolution, Is.False);
				Assert.That (mayEnter.Solved, Is.True);
				Assert.That (mayEnter.Locked, Is.False);

				//After having posted a bad solution to a multipart question, posting the correct solution should not work anymore
				try {
					service.Post (new PostSolution {
						Id = challengeId,
						SolutionType = SolutionType.MultiPart,
						Solution = solution.ToJson (),
					});
					Assert.Fail ("Posting a solution twice for a multipart question should not work.");
				} catch (HttpError e) {
					Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.Conflict));
				}
			}
		}

		[Test ()]
		[TestCase (new bool[] { true, true }, "Missing value")]
		[TestCase (new bool[] { true, true, false, false }, "Additional value")]
		[TestCase (new bool[0], "Empty List")]
		public void PostBadMultipleChoiceSolutionWrongNumberOfSolutionsTest (bool[] badSolution, string badSolutionDescription)
		{
			// List<bool> correctSolution = new List<bool>{ true, true, false };
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				//store out first access to the challenge in the db
				service.Db.Insert<ChallengeAccess> (new ChallengeAccess {
					ChallengeId = 3,
					UserId = 1,
					Type = AccessType.Description,
					AccessTime = DateTime.Now
				});
				try {
					service.Post (new PostSolution {
						Id = 3,
						SolutionType = SolutionType.MultipleChoice,
						Solution = badSolution.ToJson (),
					});
					Assert.Fail ("Posting a multiple choice question with the wrong number of solutions should not work.");
				} catch (HttpError e) {
					Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.Conflict));
				}
				// The solution should not have been accepted, so solution should be stored in the db
				Assert.That (service.Db.Count<ChallengeSolution> (cs => cs.ChallengeId == 3 && cs.UserId == 1), Is.EqualTo (0));
			}
		}

		[Test ()]
		public void PostBadMultiPartSolutionBadFormatTest ()
		{
			int challengeId = 4;
			//var solution = new Dictionary<string, string>{ { "a", " ANSWER " }, { "b", "answer2" } };
			var badSolutions = new List<Dictionary<string,string>> {
				new Dictionary<string, string> { { "b", "answer2" } },
				new Dictionary<string, string> {
					{ "a", "answer" },
					{ "b", "" }, { "c", "" }
				},
				new Dictionary<string, string> (),
			};
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				//store out first access to the challenge in the db
				service.Db.Insert<ChallengeAccess> (new ChallengeAccess {
					ChallengeId = challengeId,
					UserId = 1,
					Type = AccessType.Description,
					AccessTime = DateTime.Now
				});
				foreach (var badSolution in badSolutions) {
					try {
						service.Post (new PostSolution {
							Id = challengeId,
							SolutionType = SolutionType.MultiPart,
							Solution = badSolution.ToJson (),
						});
						Assert.Fail ("Posting to a multipart question with the wrong format should not work.");
					} catch (HttpError e) {
						Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.Conflict));
					}
					// The solution should not have been accepted, so solution should be stored in the db
					Assert.That (service.Db.Count<ChallengeSolution> (cs => cs.ChallengeId == challengeId && cs.UserId == 1), Is.EqualTo (0));
				}
			}
		}

		[Test ()]
		[TestCase (new bool[] { true, true, true }, "Bad value, all true")]
		[TestCase (new bool[] { false, false, false }, "Bad value, all false")]
		[TestCase (new bool[] { true, false, true }, "Reordered values")]
		public void PostBadMultipleChoiceSolutionTest (bool[] badSolution, string badSolutionDescription)
		{
			// List<bool> correctSolution = new List<bool>{ true, true, false };
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				//store out first access to the challenge in the db
				service.Db.Insert<ChallengeAccess> (new ChallengeAccess {
					ChallengeId = 3,
					UserId = 1,
					Type = AccessType.Description,
					AccessTime = DateTime.Now
				});
				var solution = new PostSolution {
					Id = 3,
					SolutionType = SolutionType.MultipleChoice,
					Solution = badSolution.ToJson (),
				};
				Assert.Throws<HttpError> (() => service.Post (solution), badSolutionDescription);
				//Even after posting a bad solution (with the correct number of arguments, the challenge should be marked as solved with the date set
				var solutionInDb = service.Db.Select<ChallengeSolution> (cs => cs.ChallengeId == 3 && cs.UserId == 1).First ();
				Assert.That (solutionInDb.Solved);
				Assert.That (DateTime.Now.Subtract (solutionInDb.SolutionTime).TotalSeconds, Is.LessThan (1));
			}
		}

		[Test ()]
		public void PostBadMultiPartSolutionTest ()
		{
			int challengeId = 4;
			//var solution = new Dictionary<string, string>{ { "a", " ANSWER " }, { "b", "answer2" } };
			var badSolutions = new List<Dictionary<string,string>> {
				new Dictionary<string, string> { { "a", "answer2" }, { "b", "answer" } },
				new Dictionary<string, string> {
					{ "a", "answer" },
					{ "b", "other answer" }
				},
				new Dictionary<string, string> (),
			};
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				//store out first access to the challenge in the db
				service.Db.Insert<ChallengeAccess> (new ChallengeAccess {
					ChallengeId = challengeId,
					UserId = 1,
					Type = AccessType.Description,
					AccessTime = DateTime.Now
				});
				foreach (var badSolution in badSolutions) {
					var solutionToPost = new PostSolution {
						Id = challengeId,
						SolutionType = SolutionType.MultiPart,
						Solution = badSolution.ToJson (),
					};
					Assert.Throws<HttpError> (() => service.Post (solutionToPost));
					//Even after posting a bad solution (with the correct number of arguments, the challenge should be marked as solved with the date set
					var solutionInDb = service.Db.Select<ChallengeSolution> (cs => cs.ChallengeId == challengeId && cs.UserId == 1).First ();
					Assert.That (solutionInDb.Solved);
					Assert.That (DateTime.Now.Subtract (solutionInDb.SolutionTime).TotalSeconds, Is.LessThan (1));
				}
			}
		}

		[Test ()]
		public void PostBadSolutionsTest ()
		{
			string solution = "This is it!";
			string badSolution = "foobar";

			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				//store out first access to the challenge in the db
				service.Db.Insert<ChallengeAccess> (new ChallengeAccess {
					ChallengeId = 1,
					UserId = 1,
					Type = AccessType.Description,
					AccessTime = DateTime.Now
				});

				using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
					try {
						service.Post (new PostSolution {
							Id = 1,
							SolutionType = SolutionType.Static,
							Solution = badSolution
						});
						Assert.Fail ("Posting a bad solution should result in an error.");
					} catch (HttpError e) {
						Assert.That (e.StatusCode, Is.EqualTo (System.Net.HttpStatusCode.BadRequest));
					}
					var solutionInDb = db.Select<ChallengeSolution> (c => (c.ChallengeId == 1 && c.UserId == 1)).First ();
					Assert.That (!solutionInDb.Solved);
					Assert.That (solutionInDb.WrongEntries, Is.EqualTo (1));
					Assert.That (solutionInDb.Points, Is.Null, "No points should be stored in the database after posting a bad solution");
					//post the bad solution again
					try {
						service.Post (new PostSolution {
							Id = 1,
							SolutionType = SolutionType.Static,
							Solution = badSolution
						});
						Assert.Fail ("Posting a bad solution should result in an error.");
					} catch (HttpError e) {
						Assert.That (e.StatusCode, Is.EqualTo (System.Net.HttpStatusCode.BadRequest));
					}
					solutionInDb = db.Select<ChallengeSolution> (c => (c.ChallengeId == 1 && c.UserId == 1)).First ();
					Assert.That (!solutionInDb.Solved);
					Assert.That (solutionInDb.WrongEntries, Is.EqualTo (2));
					//now post the correct solution
					service.Post (new PostSolution {
						Id = 1,
						SolutionType = SolutionType.Static,
						Solution = solution
					});
					solutionInDb = db.Select<ChallengeSolution> (c => (c.ChallengeId == 1 && c.UserId == 1)).First ();
					Assert.That (solutionInDb.Solved);
					Assert.That (solutionInDb.WrongEntries, Is.EqualTo (2));
					Assert.That (DateTime.Now.Subtract (solutionInDb.SolutionTime).TotalSeconds, Is.LessThan (1));
					Assert.That (solutionInDb.Points.Value, Is.GreaterThan (0), "A positive point value should be stored in the database after posting a correct solution");
				}
			}
		}

		[Test ()]
		public void PostSolutionWithBadSolutionTypeTest ()
		{
			string solution = "This is it!";

			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
					try {
						service.Post (new PostSolution {
							Id = 1,
							SolutionType = (SolutionType)999,
							Solution = solution,
						});
						Assert.Fail ("Posting a bad solution should result in an error.");
					} catch (HttpError e) {
						Assert.That (e.StatusCode, Is.EqualTo (System.Net.HttpStatusCode.Conflict));
					}
				}
			}
		}

		[Test ()]
		public void PostSolutionToLockedChallengeTest ()
		{
			string solution = "This is it!";
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				int challengeId = (int)service.Db.Insert (new Challenge {
					SolutionType = SolutionType.Static,
					Solution = solution,
				}, selectIdentity: true);
				int challengeSetId = (int)service.Db.Insert (new ChallengeSet { Challenges = new List<int>{ challengeId } }, selectIdentity: true);
				int eventId = (int)service.Db.Insert (new Event (), selectIdentity: true);
				service.Db.Insert (new EventChallengeSets {
					ChallengeSetId = challengeSetId,
					EventId = eventId,
					StartDate = DateTime.Now.Subtract (TimeSpan.FromHours (1)),
					EndDate = DateTime.Now.Add (TimeSpan.FromHours (1))
				});
				service.Db.Insert (new EventUsers{ UserId = 1, EventId = eventId });

				int actionId = (int)service.Db.Insert (new ChallengeAction {
					ChallengeId = challengeId,
					LockChallegenge = true
				}, selectIdentity: true);
				service.Db.Insert (new ChallengeActionAccess {
					ChallengeActionId = actionId,
					UserId = 1,
					AccessTime = DateTime.Now
				});
				//store out first access to the challenge in the db
				service.Db.Insert<ChallengeAccess> (new ChallengeAccess {
					ChallengeId = 1,
					UserId = 1,
					Type = AccessType.Description,
					AccessTime = DateTime.Now
				});
				//We should not be allowed to post a solution
				var mayEnter = service.Get (new GetChallengeSolutionMayEnter{ Id = challengeId });
				Assert.That (mayEnter.MayEnterSolution, Is.False);
				Assert.That (mayEnter.Solved, Is.False);
				Assert.That (mayEnter.Locked, Is.True);

				try {
					service.Post (new PostSolution {
						Id = challengeId,
						SolutionType = SolutionType.Static,
						Solution = solution,
					});
					Assert.Fail ("Posting a solution to a challenge locked by an action should not work.");
				} catch (HttpError e) {
					Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.Conflict));
				}
			}
		}

		[Test ()]
		public void PostSolutionSolutionEnteredTest ()
		{
			string solution = "This is it!";
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				int challengeId = (int)service.Db.Insert (new Challenge {
					SolutionType = SolutionType.Static,
					Solution = solution,
				}, selectIdentity: true);
				int challengeSetId = (int)service.Db.Insert (new ChallengeSet { Challenges = new List<int>{ challengeId } }, selectIdentity: true);
				int eventId = (int)service.Db.Insert (new Event (), selectIdentity: true);
				service.Db.Insert (new EventChallengeSets {
					ChallengeSetId = challengeSetId,
					EventId = eventId,
					StartDate = DateTime.Now.Subtract (TimeSpan.FromHours (1)),
					EndDate = DateTime.Now.Add (TimeSpan.FromHours (1))
				});
				service.Db.Insert (new EventUsers{ UserId = 1, EventId = eventId });

				//We should be allowed to post a solution
				Assert.That (service.Get (new GetChallengeSolutionMayEnter{ Id = challengeId }).MayEnterSolution, Is.True);
				//Post a bad solution and check if it gets loggend in the db
				Assert.Throws<HttpError> (() => service.Post (new PostSolution {
					Id = challengeId,
					SolutionType = SolutionType.Static,
					Solution = "bad solution"
				}));
				string solutionEnteredInDb = service.Db.Select<ChallengeSolution> (cs => cs.ChallengeId == challengeId && cs.UserId == 1).Select (cs => cs.SolutionEntered).First ();
				Assert.That (solutionEnteredInDb, Is.EqualTo ("bad solution"));

				//We should still be allowed to post a solution
				Assert.That (service.Get (new GetChallengeSolutionMayEnter{ Id = challengeId }).MayEnterSolution, Is.True);
				//Post a second bas solution and check if it gets updated in the db
				Assert.Throws<HttpError> (() => service.Post (new PostSolution {
					Id = challengeId,
					SolutionType = SolutionType.Static,
					Solution = "bad solution 2"
				}));
				solutionEnteredInDb = service.Db.Select<ChallengeSolution> (cs => cs.ChallengeId == challengeId && cs.UserId == 1).Select (cs => cs.SolutionEntered).First ();
				Assert.That (solutionEnteredInDb, Is.EqualTo ("bad solution 2"));

				//Post the correct solution, now
				service.Post (new PostSolution {
					Id = challengeId,
					SolutionType = SolutionType.Static,
					Solution = solution
				});
				solutionEnteredInDb = service.Db.Select<ChallengeSolution> (cs => cs.ChallengeId == challengeId && cs.UserId == 1).Select (cs => cs.SolutionEntered).First ();
				Assert.That (solutionEnteredInDb, Is.EqualTo (solution));

				//We should not be able to post another solution now
				Assert.That (service.Get (new GetChallengeSolutionMayEnter{ Id = challengeId }).MayEnterSolution, Is.False);
				Assert.Throws<HttpError> (() => service.Post (new PostSolution {
					Id = challengeId,
					SolutionType = SolutionType.Static,
					Solution = solution,
				}));

				//delete the previous solution attempts and post the correct solution again
				service.Db.Delete<ChallengeSolution> (cs => cs.ChallengeId == challengeId);
				service.Post (new PostSolution {
					Id = challengeId,
					SolutionType = SolutionType.Static,
					Solution = solution
				});
				solutionEnteredInDb = service.Db.Select<ChallengeSolution> (cs => cs.ChallengeId == challengeId && cs.UserId == 1).Select (cs => cs.SolutionEntered).First ();
				Assert.That (solutionEnteredInDb, Is.EqualTo (solution));
			}
		}


		[Test ()]
		public void GetHintPointReductionTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
					//prepare the database, such that the challenge is solved without a speed bonus
					DateTime startDate = DateTime.Now.ToLocalTime ();
					db.UpdateNonDefaults (new EventChallengeSets {
						StartDate = startDate,
						EndDate = startDate.AddDays (10),
					}, c => c.EventId == 1 && c.ChallengeSetId == 1);

					db.Insert (new ChallengeAccess () {
						AccessTime = startDate.AddSeconds (1),
						ChallengeId = 1,
						Type = AccessType.Description,
						UserId = 1
					});

					db.Insert (new Hint () { Id = 4, ChallengeId = 1, PointReduction = 0 });
					db.Insert (new Hint () { Id = 5, ChallengeId = 1, PointReduction = 30 });

					int pointReduction = service.Get (new GetHintPointReduction {
						Id = 1,
						HintNumber = 4
					}).Points;
					// We should loose the full speed bonus (40%) but nothing further
					Assert.That (pointReduction, Is.EqualTo (40));

					pointReduction = service.Get (new GetHintPointReduction {
						Id = 1,
						HintNumber = 5
					}).Points;
					// We should loose the full speed bonus (40%) plus 30% for the hint
					Assert.That (pointReduction, Is.EqualTo (70));

					db.Insert (new HintAccess () {
						HintId = 4,
						ChallengeId = 1,
						AccessTime = startDate.AddSeconds (2),
						UserId = 1,
					});
					pointReduction = service.Get (new GetHintPointReduction {
						Id = 1,
						HintNumber = 5
					}).Points;
					// Speed bonus is already gone because of the used hint #1, we should loose just the 30% of hint 2 now
					Assert.That (pointReduction, Is.EqualTo (30));

				}
			}
		}
	}
}
