﻿//
//  EventsServiceTest.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Linq;
using NUnit.Framework;
using ServiceStack.Testing;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using ServiceStack;
using ServiceStack.Auth;
using Hackathon.ServiceModel.Types;
using System.Collections.Generic;
using Hackathon.ServiceModel;
using Moq;
using Hackathon.ServiceInterface.Points;

namespace Hackathon.ServiceInterface.Test
{
	[TestFixture ()]
	public class EventsServiceTest
	{
		private class TestAppHost : BasicAppHost
		{
			public TestAppHost () : base (typeof(EventsService).Assembly)
			{
			}

			public override void Configure (Funq.Container container)
			{
				container.Register<IDbConnectionFactory> (c =>
					new OrmLiteConnectionFactory (":memory:", SqliteDialect.Provider)
				);

				container.Register<IAuthSession> (c => new AuthUserSession {
					UserAuthName = "foobar@mailinator.com",
					UserName = "foobar",
					UserAuthId = "1",
				});

				//container.RegisterAs<DbEmailer, IEmailer>();

				using (var db = container.TryResolve<IDbConnectionFactory> ().Open ()) {
					db.DropAndCreateTable<User> ();
					db.DropAndCreateTable<Event> ();
					db.DropAndCreateTable<EventChallengeSets> ();
					db.DropAndCreateTable<EventUsers> ();
					db.DropAndCreateTable<ChallengeSet> ();
					db.DropAndCreateTable<Challenge> ();
					db.DropAndCreateTable<ChallengeSolution> ();

					db.Insert (new Challenge{ Id = 1 });
					db.Insert (new Challenge{ Id = 2 });
					db.Insert (new Challenge{ Id = 3 });
					db.Insert (new Challenge{ Id = 42 });

					db.Insert (new ChallengeSet {
						Id = 1,
						Title = "foobar",
						Description = "Foo bar foo bar.",
						Challenges = new List<int> () { 1, 2, 42 },
					});

					db.Insert (new ChallengeSet {
						Id = 2,
						Title = "bla",
						Description = "Bla bla bla.",
						Challenges = new List<int> () { 3 },
					});

					db.Insert (new Event {
						Id = 1,
						Title = "foobar",
						Description = "Foo bar foo bar.",
					});

					db.Insert (new Event {
						Id = 2,
						Title = "Empty event",
						Description = "Nothing to be done.",
					});

					db.Insert (new EventChallengeSets {
						EventId = 1,
						ChallengeSetId = 1,
						StartDate = new DateTime (2016, 9, 1),
						EndDate = new DateTime (2016, 9, 8)
					});

					db.Insert (new EventChallengeSets {
						EventId = 1,
						ChallengeSetId = 2,
						StartDate = new DateTime (2016, 10, 1),
						EndDate = new DateTime (2016, 10, 8)
					});

					#region Testdata to test the filtering of available challengesets
					db.Insert (new Event {
						Id = 3,
					});

					db.Insert (new ChallengeSet {
						Id = 3,
					});

					db.Insert (new ChallengeSet {
						Id = 4,
					});

					db.Insert (new ChallengeSet {
						Id = 5,
					});

					//ChallengeSet #3 is currently available
					db.Insert (new EventChallengeSets {
						EventId = 3,
						ChallengeSetId = 3,
						StartDate = DateTime.Now.Subtract (new TimeSpan (1, 0, 0)),
						EndDate = DateTime.Now.Add (new TimeSpan (1, 0, 0)),
					});

					//ChallengeSet #4 is expired
					db.Insert (new EventChallengeSets {
						EventId = 3,
						ChallengeSetId = 4,
						StartDate = DateTime.Now.Subtract (new TimeSpan (1, 0, 0)),
						EndDate = DateTime.Now.Subtract (new TimeSpan (0, 1, 0)),
					});

					//ChallengeSet #5 is not available yet
					db.Insert (new EventChallengeSets {
						EventId = 3,
						ChallengeSetId = 5,
						StartDate = DateTime.Now.Add (new TimeSpan (1, 0, 0)),
						EndDate = DateTime.Now.Add (new TimeSpan (2, 0, 0)),
					});

					#endregion

					db.Insert (new User {
						Id = 1,
						UserName = "foobar",
						FirstName = "Foo",
						LastName = "bar",
						Avatar = null,
						MailAddress = "foobar@mailinator.com",
						Salt = "foobar",
						//pw=123456, created with echo -n "foobar123456"|sha256sum, then base64 encoded
						Password = @"V7yttgjbKKO8rKp6OTiHqQ8BCSEKqjaGgBtun4lWjfs=",
					});

					db.Insert (new User {
						Id = 2,
						UserName = "bla",
					});

					db.Insert (new User {
						Id = 3,
						UserName = "blubb",
					});

					db.Insert (new User {
						Id = 4,
						UserName = "xxx",
					});

					db.Insert (new User {
						Id = 5,
						UserName = "MrNoPoint",
					});


					db.Insert (new EventUsers { EventId = 1, UserId = 1 });
					db.Insert (new EventUsers { EventId = 1, UserId = 3 });
					db.Insert (new EventUsers { EventId = 1, UserId = 4 });
					db.Insert (new EventUsers { EventId = 1, UserId = 5 });
					db.Insert (new EventUsers { EventId = 2, UserId = 2 });
					db.Insert (new EventUsers { EventId = 3, UserId = 1 });
				}
			}
		}

		private readonly ServiceStackHost appHost;

		public EventsServiceTest ()
		{
			try {
				appHost = new TestAppHost ();
				appHost.Init ();
			} catch (Exception e) {
				Assert.Fail ("Apphost startup failed: {0}", e.ToString ());
			}
		}

		[TestFixtureTearDown]
		public void TestFixtureTearDown ()
		{
			appHost.Dispose ();
		}

		[Test]
		public void FindEventsTest ()
		{
			using (var service = new EventsService { Request = new MockHttpRequest () }) {
				var EventIds = service.Get (new FindEvents ());
				Assert.That (EventIds.Count, Is.EqualTo (3));
				Assert.That (EventIds.Contains (1));
				Assert.That (EventIds.Contains (2));
				Assert.That (EventIds.Contains (3));
			}
		}

		[Test]
		public void FindEventsWithChallengeSetFilterTest ()
		{
			using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
				int csInOneEventId = (int)db.Insert (new ChallengeSet {
				}, selectIdentity: true);

				int csInTwoEventsId = (int)db.Insert (new ChallengeSet {
				}, selectIdentity: true);

				int csInNoEventId = (int)db.Insert (new ChallengeSet {
				}, selectIdentity: true);

				int event1Id = (int)db.Insert (new Event {
				}, selectIdentity: true);

				int event2Id = (int)db.Insert (new Event {
				}, selectIdentity: true);

				db.Insert (new EventChallengeSets {
					ChallengeSetId = csInOneEventId,
					EventId = event1Id,
				});

				db.Insert (new EventChallengeSets {
					ChallengeSetId = csInTwoEventsId,
					EventId = event1Id,
				});

				db.Insert (new EventChallengeSets {
					ChallengeSetId = csInTwoEventsId,
					EventId = event2Id,
				});
				using (var service = new EventsService { Request = new MockHttpRequest () }) {
					var eventIds = service.Get (new FindEvents (){ ChallengeSetId = csInOneEventId });
					Assert.That (eventIds.Count, Is.EqualTo (1));
					Assert.That (eventIds.Contains (event1Id));

					eventIds = service.Get (new FindEvents (){ ChallengeSetId = csInTwoEventsId });
					Assert.That (eventIds.Count, Is.EqualTo (2));
					Assert.That (eventIds.Contains (event1Id));
					Assert.That (eventIds.Contains (event2Id));

					eventIds = service.Get (new FindEvents (){ ChallengeSetId = csInNoEventId });
					Assert.That (eventIds.Count, Is.EqualTo (0));

					db.Delete<ChallengeSet> (cs => cs.Id == csInNoEventId);
					int fakeChallengeSetId = csInNoEventId;
					eventIds = service.Get (new FindEvents (){ ChallengeSetId = fakeChallengeSetId });
					Assert.That (eventIds.Count, Is.EqualTo (0));
				}
			}
		}

		[Test]
		public void FindEventsWithChallengeFilterTest ()
		{
			using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
				int cInOneEventId = (int)db.Insert (new Challenge {
				}, selectIdentity: true);

				int cInTwoEventsId = (int)db.Insert (new Challenge {
				}, selectIdentity: true);

				int cInNoEventId = (int)db.Insert (new Challenge {
				}, selectIdentity: true);

				int csInOneEventId = (int)db.Insert (new ChallengeSet {
					Challenges = new List<int> { cInOneEventId, cInTwoEventsId },
				}, selectIdentity: true);

				int csInTwoEventsId = (int)db.Insert (new ChallengeSet {
					Challenges = new List<int> { cInTwoEventsId },
				}, selectIdentity: true);

				int csInNoEventId = (int)db.Insert (new ChallengeSet {
				}, selectIdentity: true);

				int event1Id = (int)db.Insert (new Event {
				}, selectIdentity: true);

				int event2Id = (int)db.Insert (new Event {
				}, selectIdentity: true);

				db.Insert (new EventChallengeSets {
					ChallengeSetId = csInOneEventId,
					EventId = event1Id,
				});

				db.Insert (new EventChallengeSets {
					ChallengeSetId = csInTwoEventsId,
					EventId = event1Id,
				});

				db.Insert (new EventChallengeSets {
					ChallengeSetId = csInTwoEventsId,
					EventId = event2Id,
				});

				using (var service = new EventsService { Request = new MockHttpRequest () }) {
					var eventIds = service.Get (new FindEvents (){ ChallengeId = cInOneEventId });
					Assert.That (eventIds.Count, Is.EqualTo (1));
					Assert.That (eventIds.Contains (event1Id));

					eventIds = service.Get (new FindEvents (){ ChallengeId = cInTwoEventsId });
					Assert.That (eventIds.Count, Is.EqualTo (2));
					Assert.That (eventIds.Contains (event1Id));
					Assert.That (eventIds.Contains (event2Id));

					eventIds = service.Get (new FindEvents (){ ChallengeId = cInNoEventId });
					Assert.That (eventIds.Count, Is.EqualTo (0));

					db.Delete<Challenge> (c => c.Id == cInNoEventId);
					int fakeChallengeId = cInNoEventId;
					eventIds = service.Get (new FindEvents (){ ChallengeId = fakeChallengeId });
					Assert.That (eventIds.Count, Is.EqualTo (0));

					//try searching for an event with challengeset and filter
					eventIds = service.Get (new FindEvents () {
						ChallengeSetId = csInOneEventId,
						ChallengeId = cInTwoEventsId
					});
					Assert.That (eventIds.Count, Is.EqualTo (1));
					Assert.That (eventIds.Contains (event1Id));

					eventIds = service.Get (new FindEvents () {
						ChallengeSetId = csInNoEventId,
						ChallengeId = cInTwoEventsId
					});
					Assert.That (eventIds.Count, Is.EqualTo (0), "ChallengeSet matches but Challenge does not. Should not return a result");
				}
			}
		}

		[Test]
		public void GetEventTest ()
		{
			var expected = new GetEventResponse {
				Id = 1,
				Title = "foobar",
				Description = "Foo bar foo bar.",
				ChallengeSets = new List<ChallengeSetInEvent> {
					new ChallengeSetInEvent {
						ChallengeSetId = 1,
						StartDate = new DateTime (2016, 9, 1, 0, 0, 0, DateTimeKind.Local).ToLocalTime ().ToEcmaScriptString (),
						EndDate = new DateTime (2016, 9, 8, 0, 0, 0, DateTimeKind.Local).ToLocalTime ().ToEcmaScriptString ()
					},
					new ChallengeSetInEvent {
						ChallengeSetId = 2,
						StartDate = new DateTime (2016, 10, 1, 0, 0, 0, DateTimeKind.Local).ToLocalTime ().ToEcmaScriptString (),
						EndDate = new DateTime (2016, 10, 8, 0, 0, 0, DateTimeKind.Local).ToLocalTime ().ToEcmaScriptString ()
					},
				}
			};
			using (var service = new EventsService { Request = new MockHttpRequest () }) {
				var event_ = service.Get (new GetEvent (){ Id = 1 });
				Assert.That (event_.Id, Is.EqualTo (expected.Id));
				Assert.That (event_.Title, Is.EqualTo (expected.Title));
				Assert.That (event_.Description, Is.EqualTo (expected.Description));
				Assert.That (expected.ChallengeSets.SequenceEqual (event_.ChallengeSets));
			}
		}

		[Test]
		public void GetEventWithoutChallengesTest ()
		{
			var expected = new GetEventResponse {
				Id = 2,
				Title = "Empty event",
				Description = "Nothing to be done.",
				ChallengeSets = new List<ChallengeSetInEvent> ()
			};
			using (var service = new EventsService { Request = new MockHttpRequest () }) {
				var event_ = service.Get (new GetEvent (){ Id = 2 });
				Assert.That (event_.Id, Is.EqualTo (expected.Id));
				Assert.That (event_.Title, Is.EqualTo (expected.Title));
				Assert.That (event_.Description, Is.EqualTo (expected.Description));
				Assert.That (expected.ChallengeSets.SequenceEqual (event_.ChallengeSets));
			}
		}

		[Test]
		public void GetEventOnlyCurrentFilteringTest ()
		{
			var currentChallengeSets = new int[]{ 3 };
			var allChallengeSets = new int[]{ 3, 4, 5 };
			using (var service = new EventsService { Request = new MockHttpRequest () }) {
				var challengeSets = service.Get (new GetEvent () {
					Id = 3,
					OnlyCurrent = true
				}).ChallengeSets.Select (c => c.ChallengeSetId).ToArray ();
				Assert.That (challengeSets.SequenceEqual (currentChallengeSets));
				challengeSets = service.Get (new GetEvent (){ Id = 3, OnlyCurrent = false }).ChallengeSets.Select (c => c.ChallengeSetId).ToArray ();
				Assert.That (challengeSets.SequenceEqual (allChallengeSets));
				//Setting OnlyCurrent and OnlyCurrentAndPast should have the same result as just OnlyCurrent
				challengeSets = service.Get (new GetEvent () {
					Id = 3,
					OnlyCurrent = true,
					OnlyCurrentAndPast = true
				}).ChallengeSets.Select (c => c.ChallengeSetId).ToArray ();
				Assert.That (challengeSets.SequenceEqual (currentChallengeSets));
			}
		}

		[Test]
		public void GetEventOnlyCurrentAndPastFilteringTest ()
		{
			var currentAndPastChallengeSets = new int[]{ 3, 4 };
			var allChallengeSets = new int[]{ 3, 4, 5 };
			using (var service = new EventsService { Request = new MockHttpRequest () }) {
				var challengeSets = service.Get (new GetEvent () {
					Id = 3,
					OnlyCurrentAndPast = true,
				}).ChallengeSets.Select (c => c.ChallengeSetId).ToArray ();
				Assert.That (challengeSets.SequenceEqual (currentAndPastChallengeSets));
				challengeSets = service.Get (new GetEvent () {
					Id = 3,
					OnlyCurrent = false,
					OnlyCurrentAndPast = false
				}).ChallengeSets.Select (c => c.ChallengeSetId).ToArray ();
				Assert.That (challengeSets.SequenceEqual (allChallengeSets));
			}
		}

		[Test]
		public void GetEventNonExistingTest ()
		{
			using (var service = new EventsService { Request = new MockHttpRequest () }) {
				try {
					service.Get (new GetEvent (){ Id = 666 });
					Assert.Fail ("Should throw on lookup up non existing ID");
				} catch (HttpError e) {
					Assert.That (e.StatusCode, Is.EqualTo (System.Net.HttpStatusCode.NotFound));
				}
			}
		}

		[Test]
		public void GetEventUsersTest ()
		{
			var expected = new List<int>{ 1, 3, 4, 5 };
			using (var service = new EventsService { Request = new MockHttpRequest () }) {
				var users = service.Get (new GetEventUsers (){ Id = 1 });
				Assert.That (expected.SequenceEqual (users));
			}
		}

		[Test]
		public void GetEventUsersNonExistingEventTest ()
		{
			using (var service = new EventsService { Request = new MockHttpRequest () }) {
				try {
					service.Get (new GetEventUsers (){ Id = 666 });
					Assert.Fail ("Should throw on lookup up non existing ID");
				} catch (HttpError e) {
					Assert.That (e.StatusCode, Is.EqualTo (System.Net.HttpStatusCode.NotFound));
				}
			}
		}

		[Test]
		public void GetEventUsersAuthenticatedUserNotInEventTest ()
		{
			using (var service = new EventsService { Request = new MockHttpRequest () }) {
				try {
					service.Get (new GetEventUsers (){ Id = 2 });
					Assert.Fail ("Should throw on unatuntehticated access.");
				} catch (HttpError e) {
					Assert.That (e.StatusCode, Is.EqualTo (System.Net.HttpStatusCode.Forbidden));
				}
			}
		}

		[Test ()]
		public void GetRankingTest ()
		{
			//user 1,3,4 and 5 are in event 1 which consits of the challenges 1,2,3 and 42
			using (var service = new EventsService { Request = new MockHttpRequest () }) {
				// Store all solved challenges in the database
				service.Db.Insert<ChallengeSolution> (new ChallengeSolution () {
					UserId = 1,
					ChallengeId = 1,
					Solved = true,
					SolutionTime = DateTime.Now,
					Points = 100,
				});
				service.Db.Insert<ChallengeSolution> (new ChallengeSolution () {
					UserId = 1,
					ChallengeId = 3,
					Solved = true,
					SolutionTime = DateTime.Now,
					Points = 100,
				});
				service.Db.Insert<ChallengeSolution> (new ChallengeSolution () {
					UserId = 1,
					ChallengeId = 42,
					Solved = true,
					SolutionTime = DateTime.Now,
					Points = 100,
				});
					
				service.Db.Insert<ChallengeSolution> (new ChallengeSolution () {
					UserId = 4,
					ChallengeId = 1,
					Solved = true,
					SolutionTime = DateTime.Now,
					Points = 150,
				});
				service.Db.Insert<ChallengeSolution> (new ChallengeSolution () {
					UserId = 4,
					ChallengeId = 3,
					Solved = true,
					SolutionTime = DateTime.Now,
					Points = 160,
				});
				service.Db.Insert<ChallengeSolution> (new ChallengeSolution () {
					UserId = 3,
					ChallengeId = 2,
					Solved = true,
					SolutionTime = DateTime.Now,
					Points = 200,
				});

				int[] challengeIds = new int[]{ 1, 2, 3, 42 };
				int[] eventUsers = new int[]{ 1, 3, 4, 5 };

				var query = service.Db.From<ChallengeSolution> ().Where (s => Sql.In (s.ChallengeId, challengeIds) && Sql.In (s.UserId, eventUsers))   //only the challenges and users from this event
					.LeftJoin<ChallengeSolution, User> ((s, u) => s.UserId == u.Id)   //JOIN with the user table (to get the UserName
					.Select<ChallengeSolution, User> ((s, u) => new {s.UserId, u.UserName, Points = Sql.Sum (s.Points)}).GroupBy (s => s.UserId);  //Select the sum of points
				List<EventRankingResponse> ret = service.Db.SqlList<EventRankingResponse> (query).OrderByDescending (r => r.Points).ToList ();

				var query2 = service.Db.From<User> ()
					.LeftJoin<User, ChallengeSolution> ((u, s) => s.UserId == u.Id && Sql.In (s.ChallengeId, challengeIds))
				             				.Where<User> (u => Sql.In (u.Id, eventUsers))
//				             				.Where<User> (u => Sql.In (u.Id, eventUsers)).And<ChallengeSolution> (s => Sql.In (s.ChallengeId, challengeIds))
//					.Select<User, ChallengeSolution> ((u, s) => new  {UserId = u.Id, u.UserName, Points = Sql.Sum (s.Points)})
					.GroupBy (u => u.Id)  //Select the sum of points
					.Select<User, ChallengeSolution> ((u, s) => new {UserId = u.Id, u.UserName, s.Points})
//					.GroupBy(u => u.Id)  //Select the sum of points
//					.GroupBy ("UserId")  //Select the sum of points


					.OrderBy (u => u.Id);
				List<EventRankingResponse> ret2 = service.Db.SqlList<EventRankingResponse> (query2).OrderByDescending (r => r.Points).ToList ();

				var ranking = service.Get (new GetEventRanking (){ Id = 1 });
				Assert.That (ranking.Count (), Is.EqualTo (4), "The ranking should contain all 4 users");
				Assert.That (ranking.Select (r => r.UserId).Except (new int[] {
					1,
					3,
					4,
					5
				}).Any (), Is.False, "The ranking should contain the correct users");
				EventRankingResponse rankUser1 = ranking.Where (r => r.UserId == 1).First ();
				EventRankingResponse rankUser3 = ranking.Where (r => r.UserId == 3).First ();
				EventRankingResponse rankUser4 = ranking.Where (r => r.UserId == 4).First ();
				EventRankingResponse rankUser5 = ranking.Where (r => r.UserId == 5).First ();

				Assert.That (rankUser1.Points, Is.EqualTo (300), "User 1 should have the correct number of points");
				Assert.That (rankUser1.Rank, Is.EqualTo (2), "User 1 should have the correct rank");
				Assert.That (rankUser3.Points, Is.EqualTo (200), "User 3 should have the correct number of points");
				Assert.That (rankUser3.Rank, Is.EqualTo (3), "User 3 should have the correct rank");
				Assert.That (rankUser4.Points, Is.EqualTo (310), "User 4 should have the correct number of points");
				Assert.That (rankUser4.Rank, Is.EqualTo (1), "User 4 should have the correct rank");
				Assert.That (rankUser5.Points, Is.EqualTo (0), "User 5 should have the correct number of points");
				Assert.That (rankUser5.Rank, Is.EqualTo (4), "User 5 should have the correct rank");
			}
		}
	}
}

