﻿//
//  AttachmentsServiceTest.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Linq;
using NUnit.Framework;
using ServiceStack.Testing;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using ServiceStack.Auth;
using ServiceStack;
using Hackathon.ServiceModel.Types;
using System.Collections.Generic;
using System.Text;
using Hackathon.ServiceModel;
using System.Net;

namespace Hackathon.ServiceInterface.Test
{
	[TestFixture]
	public class AttachmentsServiceTest
	{
		private class TestAppHost : BasicAppHost
		{
			public TestAppHost () : base (typeof(ChallengesService).Assembly)
			{
			}

			public override void Configure (Funq.Container container)
			{
				container.Register<IDbConnectionFactory> (c =>
					new OrmLiteConnectionFactory (":memory:", SqliteDialect.Provider)
				);

				container.Register<IAuthSession> (c => new AuthUserSession {
					UserAuthName = "foobar@mailinator.com",
					UserName = "foobar",
					UserAuthId = "1",
				});

				using (var db = container.TryResolve<IDbConnectionFactory> ().Open ()) {
					db.DropAndCreateTable<Challenge> ();
					db.DropAndCreateTable<Hint> ();
					db.DropAndCreateTable<Attachment> ();
					db.DropAndCreateTable<User> ();
					db.DropAndCreateTable<ChallengeAccess> ();
					db.DropAndCreateTable<HintAccess> ();
					db.DropAndCreateTable<Event> ();
					db.DropAndCreateTable<EventUsers> ();
					db.DropAndCreateTable<ChallengeSet> ();
					db.DropAndCreateTable<EventChallengeSets> ();

					db.Insert (new Attachment {
						Id = 1,
						Title = "More information",
						MimeType = "text/markdown",
						Content = UTF8Encoding.UTF8.GetBytes ("# Information").ToList ()
					});

					db.Insert (new Attachment {
						Id = 2,
						Title = "Image",
						MimeType = "image/png",
						Content = new List<byte>{ 0x01, 0x02, 0x03 }
					});

					db.Insert (new Challenge {
						Id = 1,
						Title = "foobar",
						Description = "Foo bar foo bar.",
						Attachments = new List<int> { 1 },
					});

					db.Insert (new Challenge {
						Id = 2,
						Title = "Challenge without attachments",
						Description = "Foo bar foo bar.",
					});

					db.Insert (new Challenge {
						Id = 3,
						Title = "Challenge no accesible to the user",
					});

					db.Insert (new Hint {
						ChallengeId = 1,
						Text = "Hint #1"
					});

					db.Insert (new Hint {
						ChallengeId = 1,
						Text = "The second hint",
						Attachments = new List<int>{ 2 }							
					});

					db.Insert (new Event {
						Id = 1,
					});

					db.Insert (new ChallengeSet {
						Id = 1,
						Challenges = new List<int>{ 1, 2 },
					});

					db.Insert (new ChallengeSet {
						Id = 2,
						Challenges = new List<int>{ 3 },
					});

					db.Insert (new EventChallengeSets {
						EventId = 1,
						ChallengeSetId = 1,
						StartDate = DateTime.Now.Subtract (new TimeSpan (1, 0, 0)),
						EndDate = DateTime.Now.Add (new TimeSpan (1, 0, 0)),
					});

					db.Insert (new EventChallengeSets {
						EventId = 1,
						ChallengeSetId = 2,
						StartDate = DateTime.Now.Subtract (new TimeSpan (1, 0, 0)),
						EndDate = DateTime.Now.Subtract (new TimeSpan (0, 1, 0)),
					});


					db.Insert (new User {
						Id = 1,
						UserName = "foobar",
						FirstName = "Foo",
						LastName = "bar",
						Avatar = null,
						Salt = "foobar",
						MailAddress = "foobar@mailinator.com",
						//pw=123456, created with echo -n "foobar123456"|sha256sum, then base64 encoded
						Password = @"V7yttgjbKKO8rKp6OTiHqQ8BCSEKqjaGgBtun4lWjfs=",
					});

					db.Insert (new EventUsers { EventId = 1, UserId = 1 });

				}
			}
		}

		private ServiceStackHost appHost;

		[SetUp]
		public void SetupChallengesServiceTest ()
		{
			try {
				appHost = new TestAppHost ();
				appHost.Init ();
			} catch (Exception e) {
				Assert.Fail ("Apphost startup failed: {0}", e.ToString ());
			}
		}

		[TearDown]
		public void TestFixtureTearDown ()
		{
			appHost.Dispose ();
		}

		[Test]
		public void GetChallengeAttachmentsTest ()
		{
			using (var service = new AttachmentsService { Request = new MockHttpRequest () }) {
				//Get the attachments from a challenge with attachments
				var attachments = service.Get (new GetChallengeAttachments { Id = 1 });
				Assert.That (attachments.Count, Is.EqualTo (1));
				Assert.That (attachments [0].Title, Is.EqualTo ("More information"));
				Assert.That (attachments [0].MimeType, Is.EqualTo ("text/markdown"));
				Assert.That (UTF8Encoding.UTF8.GetString (attachments [0].Content.ToArray ()), Is.EqualTo ("# Information"));
				using (var db = service.TryResolve<IDbConnectionFactory> ().Open ()) {
					//Now check if the access time has been set
					DateTime accessTime = db.Select<ChallengeAccess> ().Where (a => (a.ChallengeId == 1 && a.UserId == 1 && a.Type == AccessType.Attachments)).First ().AccessTime;
					Assert.That (DateTime.Now.Subtract (accessTime).TotalSeconds, Is.LessThan (1));
					//Check that the access time does not change on another request
					service.Get (new GetChallengeAttachments { Id = 1 });
					DateTime secondAccessTime = db.Select<ChallengeAccess> ().Where (a => (a.ChallengeId == 1 && a.UserId == 1 && a.Type == AccessType.Attachments)).First ().AccessTime;
					Assert.That (accessTime, Is.EqualTo (secondAccessTime));
				}

				//Get the attachments from a challenge without attachments
				attachments = service.Get (new GetChallengeAttachments { Id = 2 });
				Assert.That (attachments.Count, Is.EqualTo (0));
			}
		}

		[Test]
		public void GetChallengeAttachmentsNonExistingChallengeTest ()
		{
			using (var service = new AttachmentsService { Request = new MockHttpRequest () }) {
				//Get the attachments from a non existing challenge
				try {
					service.Get (new GetChallengeAttachments { Id = 666 });
					Assert.Fail ("Getting attachments from a non-existing challenge should not work.");
				} catch (HttpError e) {
					// Return code is Forbidden, beacause a non-existing challenge is no accesible by the user
					Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.Forbidden));
				}
			}
		}

		[Test]
		public void GetHintAttachmentsTest ()
		{
			using (var service = new AttachmentsService { Request = new MockHttpRequest () }) {
				int hintId;
				using (var challengeService = service.ResolveService<ChallengesService> ())
					hintId = challengeService.Get (new GetChallengeHints{ Id = 1 }).Last (); //attachment is part of the second hint
				
				//Get the attachments from the hint
				var attachments = service.Get (new GetHintAttachments {
					Id = 1,
					HintNumber = hintId
				});
				Assert.That (attachments [0].Title, Is.EqualTo ("Image"));
				Assert.That (attachments [0].MimeType, Is.EqualTo ("image/png"));
				Assert.That (attachments [0].Content.SequenceEqual (new byte[] {
					0x01,
					0x02,
					0x03
				}));
				using (var db = service.TryResolve<IDbConnectionFactory> ().Open ()) {
					//Now check if the access time has been set
					DateTime accessTime = db.Select<HintAccess> ().Where (a => (a.ChallengeId == 1 && a.UserId == 1 && a.HintId == hintId)).First ().AccessTime;
					Assert.That (DateTime.Now.Subtract (accessTime).TotalSeconds, Is.LessThan (1));
					//Check that the access time does not change on another request
					service.Get (new GetChallengeAttachments { Id = 1 });
					DateTime secondAccessTime = db.Select<HintAccess> ().Where (a => (a.ChallengeId == 1 && a.UserId == 1 && a.HintId == hintId)).First ().AccessTime;
					Assert.That (accessTime, Is.EqualTo (secondAccessTime));
				}
			}
		}

		[Test]
		public void GetHintAttachmentsNotAccesibleTest ()
		{
			using (var service = new AttachmentsService { Request = new MockHttpRequest () }) {
				try {
					service.Get (new GetHintAttachments { Id = 3 });
					Assert.Fail ("Should not be alowed to access the hints of an inaccessible challenge.");
				} catch (HttpError e) {
					Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.NotFound));
				}
			}
		}
	}
}

