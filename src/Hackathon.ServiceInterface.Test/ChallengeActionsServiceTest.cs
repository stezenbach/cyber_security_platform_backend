﻿//
//  ChallengeActionsServiceTest.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2017 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using NUnit.Framework;
using System;
using ServiceStack.Testing;
using ServiceStack;
using ServiceStack.Data;
using ServiceStack.Auth;
using ServiceStack.OrmLite;
using Hackathon.ServiceModel.Types;
using System.Collections.Generic;
using Hackathon.ServiceModel;
using System.Security.Cryptography;
using System.Text;
using System.Linq;
using System.Net;
using Moq;
using Hackathon.ServiceInterface.Points;

namespace Hackathon.ServiceInterface.Test
{
	[TestFixture ()]
	public class ChallengeActionsServiceTest
	{
		private class TestAppHost : BasicAppHost
		{
			public TestAppHost () : base (typeof(ChallengeActionsService).Assembly)
			{
			}

			public override void Configure (Funq.Container container)
			{
				container.Register<IDbConnectionFactory> (c =>
					new OrmLiteConnectionFactory (":memory:", SqliteDialect.Provider)
				);

				container.Register<IAuthSession> (c => new AuthUserSession {
					UserAuthName = "foobar@mailinator.com",
					UserName = "foobar",
					UserAuthId = "1",
				});

				container.Register<IPoints> (new StaticPoints ());

				using (var db = container.TryResolve<IDbConnectionFactory> ().Open ()) {
					db.DropAndCreateTable<Challenge> ();
					db.DropAndCreateTable<ChallengeSet> ();
					db.DropAndCreateTable<Event> ();
					db.DropAndCreateTable<EventUsers> ();
					db.DropAndCreateTable<EventChallengeSets> ();
					db.DropAndCreateTable<User> ();
					db.DropAndCreateTable<ChallengeAction> ();
					db.DropAndCreateTable<ChallengeSolution> ();
					db.DropAndCreateTable<ChallengeActionAccess> ();

					db.Insert (new User {
						Id = 1,
						UserName = "foobar",
						FirstName = "Foo",
						LastName = "bar",
						Avatar = null,
						Salt = "foobar",
						MailAddress = "foobar@mailinator.com",
						//pw=123456, created with echo -n "foobar123456"|sha256sum, then base64 encoded
						Password = @"V7yttgjbKKO8rKp6OTiHqQ8BCSEKqjaGgBtun4lWjfs=",
					});
				}
			}
		}

		private ServiceStackHost appHost;

		[SetUp]
		public void SetupChallengesServiceTest ()
		{
			try {
				appHost = new TestAppHost ();
				appHost.Init ();
			} catch (Exception e) {
				Assert.Fail ("Apphost startup failed: {0}", e.ToString ());
			}
		}

		//[TestFixtureTearDown]
		[TearDown]
		public void TestFixtureTearDown ()
		{
			appHost.Dispose ();
		}

		[Test ()]
		public void PostChallengeAction ()
		{
			byte[] key = new byte[]{ 0x01, 0x02, 0x03 };
			UInt64 nonce = (UInt64)new Random ().Next (0, int.MaxValue);

			using (var service = new ChallengeActionsService { Request = new MockHttpRequest () }) {
				int challengeId = (int)service.Db.Insert (new Challenge { }, selectIdentity: true);

				int challengeSetId = (int)service.Db.Insert (new ChallengeSet {
					Challenges = new List<int> { challengeId }
				}, selectIdentity: true);

				int eventId = (int)service.Db.Insert (new Event {
				}, selectIdentity: true);

				service.Db.Insert (new EventChallengeSets {
					ChallengeSetId = challengeSetId,
					EventId = eventId,
					StartDate = DateTime.Now,
					EndDate = DateTime.Now.Add (new TimeSpan (1, 0, 0)),
				});

				service.Db.Insert (new EventUsers {
					EventId = eventId,
					UserId = 1,
				});

				int actionId = (int)service.Db.Insert (new ChallengeAction {
					ChallengeId = challengeId,
					Key = Convert.ToBase64String (key),
				}, selectIdentity: true);

				string mac;
				using (var macCalulator = new HMACSHA256 (key)) {
					string macMessage = new ChallengeActionsService.PostActionAuthenticationData () {
						ActionId = actionId,
						ChallengeId = challengeId,
						Nonce = nonce,
						UserId = 1
					}.ToJson ();
					mac = Convert.ToBase64String (macCalulator.ComputeHash (UTF8Encoding.UTF8.GetBytes (macMessage)));
				}

				service.Post (new PostChallengeActions {
					Id = 1,
					ChallengeId = challengeId,
					ActionId = actionId,
					Nonce = nonce,
					Mac = mac
				});

				//Posting the action request worked. We need to check, if the access has benn logged in the db
				var actionAccesses = service.Db.Select<ChallengeActionAccess> ().Where (caa => caa.ChallengeActionId == actionId && caa.UserId == 1).ToList ();
				Assert.That (actionAccesses.Count, Is.EqualTo (1), "There should be exactly one access for this action and this user");
				var actionAccess = actionAccesses [0];
				Assert.That (actionAccess.Nonce, Is.EqualTo (nonce), "Nonce should be logged as entered.");
				Assert.That (actionAccess.AccessTime.Subtract (DateTime.Now).TotalSeconds, Is.LessThan (1), "The action access timestamp should be within the last second.");
			}
		}

		[Test ()]
		public void PostChallengeActionAfterChallengeWasSolved ()
		{
			Mock<IPoints> pointsMock = new Mock<IPoints> ();
			appHost.Container.Register<IPoints> (pointsMock.Object);

			byte[] key = new byte[]{ 0x01, 0x02, 0x03 };
			UInt64 nonce = (UInt64)new Random ().Next (0, int.MaxValue);

			using (var service = new ChallengeActionsService { Request = new MockHttpRequest () }) {
				int challengeId = (int)service.Db.Insert (new Challenge {
					SolutionType = SolutionType.Static,
					Solution = "solution"
				}, selectIdentity: true);

				int challengeSetId = (int)service.Db.Insert (new ChallengeSet {
					Challenges = new List<int> { challengeId }
				}, selectIdentity: true);

				int eventId = (int)service.Db.Insert (new Event {
				}, selectIdentity: true);

				service.Db.Insert (new EventChallengeSets {
					ChallengeSetId = challengeSetId,
					EventId = eventId,
					StartDate = DateTime.Now,
					EndDate = DateTime.Now.Add (new TimeSpan (1, 0, 0)),
				});

				service.Db.Insert (new EventUsers {
					EventId = eventId,
					UserId = 1,
				});

				int actionId = (int)service.Db.Insert (new ChallengeAction {
					ChallengeId = challengeId,
					Key = Convert.ToBase64String (key),
					PointReduction = 50,
				}, selectIdentity: true);

				//post the correct solution
				using (var challengeService = new ChallengesService { Request = new MockHttpRequest () }) {
					challengeService.Post (new PostSolution () {
						Id = challengeId,
						SolutionType = SolutionType.Static,
						Solution = "solution"
					});
					//Point calculation should have been called once during the posting of the solution
					pointsMock.Verify (p => p.GetPoints (It.IsAny<Service> (), 1, challengeId), Times.Once ());
				}


				string mac;
				using (var macCalulator = new HMACSHA256 (key)) {
					string macMessage = new ChallengeActionsService.PostActionAuthenticationData () {
						ActionId = actionId,
						ChallengeId = challengeId,
						Nonce = nonce,
						UserId = 1
					}.ToJson ();
					mac = Convert.ToBase64String (macCalulator.ComputeHash (UTF8Encoding.UTF8.GetBytes (macMessage)));
				}

				service.Post (new PostChallengeActions {
					Id = 1,
					ChallengeId = challengeId,
					ActionId = actionId,
					Nonce = nonce,
					Mac = mac
				});

				//Point calculation should have been called again, after triggering an action on a solved challenge
				pointsMock.Verify (p => p.GetPoints (It.IsAny<Service> (), 1, challengeId), Times.Exactly (2));
			}
		}

		[Test ()]
		public void PostChallengeActionTwice ()
		{
			byte[] key = new byte[]{ 0x01, 0x02, 0x03 };
			UInt64 nonce = (UInt64)new Random ().Next (0, int.MaxValue);

			using (var service = new ChallengeActionsService { Request = new MockHttpRequest () }) {
				int challengeId = (int)service.Db.Insert (new Challenge { }, selectIdentity: true);

				int challengeSetId = (int)service.Db.Insert (new ChallengeSet {
					Challenges = new List<int> { challengeId }
				}, selectIdentity: true);

				int eventId = (int)service.Db.Insert (new Event {
				}, selectIdentity: true);

				service.Db.Insert (new EventChallengeSets {
					ChallengeSetId = challengeSetId,
					EventId = eventId,
					StartDate = DateTime.Now,
					EndDate = DateTime.Now.Add (new TimeSpan (1, 0, 0)),
				});

				service.Db.Insert (new EventUsers {
					EventId = eventId,
					UserId = 1,
				});

				int actionId = (int)service.Db.Insert (new ChallengeAction {
					ChallengeId = challengeId,
					Key = Convert.ToBase64String (key),
				}, selectIdentity: true);

				string mac;
				using (var macCalulator = new HMACSHA256 (key)) {
					string macMessage = new ChallengeActionsService.PostActionAuthenticationData () {
						ActionId = actionId,
						ChallengeId = challengeId,
						Nonce = nonce,
						UserId = 1
					}.ToJson ();
					mac = Convert.ToBase64String (macCalulator.ComputeHash (UTF8Encoding.UTF8.GetBytes (macMessage)));
				}

				service.Post (new PostChallengeActions {
					Id = 1,
					ChallengeId = challengeId,
					ActionId = actionId,
					Nonce = nonce,
					Mac = mac
				});

				//Posting the action request worked. We need to check, if the access has benn logged in the db
				var actionAccesses = service.Db.Select<ChallengeActionAccess> ().Where (caa => caa.ChallengeActionId == actionId && caa.UserId == 1).ToList ();
				Assert.That (actionAccesses.Count, Is.EqualTo (1), "There should be exactly one access for this action and this user");
				//Try posting the same action again
				nonce = (UInt64)new Random ().Next (0, int.MaxValue);
				using (var macCalulator = new HMACSHA256 (key)) {
					string macMessage = new ChallengeActionsService.PostActionAuthenticationData () {
						ActionId = actionId,
						ChallengeId = challengeId,
						Nonce = nonce,
						UserId = 1
					}.ToJson ();
					mac = Convert.ToBase64String (macCalulator.ComputeHash (UTF8Encoding.UTF8.GetBytes (macMessage)));
				}


				try {
					service.Post (new PostChallengeActions {
						Id = 1,
						ChallengeId = challengeId,
						ActionId = actionId,
						Nonce = nonce,
						Mac = mac
					});	
					Assert.Fail ("Triggering the same action twice should fail.");
				} catch (HttpError e) {
					Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.Conflict));
					Assert.That (e.ErrorCode, Is.EqualTo (ChallengeActionsService.PostChallengeActionErrorCode.ActionAlreadyTriggered.ToString ()));
				}
			}
		}

		[Test ()]
		public void PostChallengeActionUnknownUser ()
		{
			byte[] key = new byte[]{ 0x01, 0x02, 0x03 };
			UInt64 nonce = (UInt64)new Random ().Next (0, int.MaxValue);

			using (var service = new ChallengeActionsService { Request = new MockHttpRequest () }) {
				int challengeId = (int)service.Db.Insert (new Challenge { }, selectIdentity: true);

				int unknownUserId = (int)service.Db.Insert (new User (), selectIdentity: true);
				service.Db.DeleteByIds<User> (new int[]{ unknownUserId });

				int actionId = (int)service.Db.Insert (new ChallengeAction {
					ChallengeId = challengeId,
					Key = Convert.ToBase64String (key),
				}, selectIdentity: true);


				string mac;
				using (var macCalculator = HMACSHA256.Create ()) {
					macCalculator.Key = key;
					string macMessage = new ChallengeActionsService.PostActionAuthenticationData () {
						ActionId = actionId,
						ChallengeId = challengeId,
						Nonce = nonce,
						UserId = unknownUserId
					}.ToJson ();
					mac = Convert.ToBase64String (macCalculator.ComputeHash (UTF8Encoding.UTF8.GetBytes (macMessage)));
				}

				try {
					service.Post (new PostChallengeActions {
						Id = unknownUserId,
						ChallengeId = challengeId,
						ActionId = actionId,
						Nonce = nonce,
						Mac = mac
					});
					Assert.Fail ("Posting an action with an unknown user should not be possible.");
				} catch (HttpError e) {
					Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.NotFound));
				}
			}
		}

		[Test ()]
		public void PostChallengeActionUnknownChallenge ()
		{
			byte[] key = new byte[]{ 0x01, 0x02, 0x03 };
			UInt64 nonce = (UInt64)new Random ().Next (0, int.MaxValue);

			using (var service = new ChallengeActionsService { Request = new MockHttpRequest () }) {

				int unknownChallengeId = (int)service.Db.Insert (new Challenge { }, selectIdentity: true);
				service.Db.DeleteByIds<Challenge> (new int[]{ unknownChallengeId });

				int actionId = (int)service.Db.Insert (new ChallengeAction {
					ChallengeId = unknownChallengeId,
					Key = Convert.ToBase64String (key),
				}, selectIdentity: true);


				string mac;
				using (var macCalulator = HMACSHA256.Create ()) {
					macCalulator.Key = key;
					string macMessage = new ChallengeActionsService.PostActionAuthenticationData () {
						ActionId = actionId,
						ChallengeId = unknownChallengeId,
						Nonce = nonce,
						UserId = 1,
					}.ToJson ();
					mac = Convert.ToBase64String (macCalulator.ComputeHash (UTF8Encoding.UTF8.GetBytes (macMessage)));
				}

				try {
					service.Post (new PostChallengeActions {
						Id = 1,
						ChallengeId = unknownChallengeId,
						ActionId = actionId,
						Nonce = nonce,
						Mac = mac
					});
					Assert.Fail ("Posting an action with an unknown challenge should not be possible.");
				} catch (HttpError e) {
					Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.NotFound));
				}
			}
		}

		[Test ()]
		public void PostChallengeInactiveChallengeAction ()
		{
			byte[] key = new byte[]{ 0x01, 0x02, 0x03 };
			UInt64 nonce = (UInt64)new Random ().Next (0, int.MaxValue);

			using (var service = new ChallengeActionsService { Request = new MockHttpRequest () }) {
				int challengeId = (int)service.Db.Insert (new Challenge { }, selectIdentity: true);

				int challengeSetId = (int)service.Db.Insert (new ChallengeSet {
					Challenges = new List<int> { challengeId }
				}, selectIdentity: true);

				int eventId = (int)service.Db.Insert (new Event {
				}, selectIdentity: true);

				service.Db.Insert (new EventChallengeSets {
					ChallengeSetId = challengeSetId,
					EventId = eventId,
					StartDate = DateTime.Now.Subtract (TimeSpan.FromHours (2)),
					EndDate = DateTime.Now.Subtract (TimeSpan.FromHours (1)),
				});

				service.Db.Insert (new EventUsers {
					EventId = eventId,
					UserId = 1,
				});

				int actionId = (int)service.Db.Insert (new ChallengeAction {
					ChallengeId = challengeId,
					Key = Convert.ToBase64String (key),
				}, selectIdentity: true);

				string mac;
				using (var macCalulator = HMACSHA256.Create ()) {
					macCalulator.Key = key;
					string macMessage = new ChallengeActionsService.PostActionAuthenticationData () {
						ActionId = actionId,
						ChallengeId = challengeId,
						Nonce = nonce,
						UserId = 1
					}.ToJson ();
					mac = Convert.ToBase64String (macCalulator.ComputeHash (UTF8Encoding.UTF8.GetBytes (macMessage)));
				}


				try {
					service.Post (new PostChallengeActions {
						Id = 1,
						ChallengeId = challengeId,
						ActionId = actionId,
						Nonce = nonce,
						Mac = mac
					});
					Assert.Fail ("Posting an action with an inactive challenge should not be possible.");
				} catch (HttpError e) {
					Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.Forbidden));
					Assert.That (e.ErrorCode, Is.EqualTo (ChallengeActionsService.PostChallengeActionErrorCode.ChallengeNotActiveForUser.ToString ()));
				}
			}
		}

		[Test ()]
		public void PostChallengeUserNotInChallengeAction ()
		{
			byte[] key = new byte[]{ 0x01, 0x02, 0x03 };
			UInt64 nonce = (UInt64)new Random ().Next (0, int.MaxValue);

			using (var service = new ChallengeActionsService { Request = new MockHttpRequest () }) {
				int challengeId = (int)service.Db.Insert (new Challenge { }, selectIdentity: true);

				int challengeSetId = (int)service.Db.Insert (new ChallengeSet {
					Challenges = new List<int> { challengeId }
				}, selectIdentity: true);

				int eventId = (int)service.Db.Insert (new Event {
				}, selectIdentity: true);

				service.Db.Insert (new EventChallengeSets {
					ChallengeSetId = challengeSetId,
					EventId = eventId,
					StartDate = DateTime.Now.Subtract (TimeSpan.FromHours (2)),
					EndDate = DateTime.Now.Add (TimeSpan.FromHours (1)),
				});

				int actionId = (int)service.Db.Insert (new ChallengeAction {
					ChallengeId = challengeId,
					Key = Convert.ToBase64String (key),
				}, selectIdentity: true);

				string mac;
				using (var macCalulator = HMACSHA256.Create ()) {
					macCalulator.Key = key;
					string macMessage = new ChallengeActionsService.PostActionAuthenticationData () {
						ActionId = actionId,
						ChallengeId = challengeId,
						Nonce = nonce,
						UserId = 1
					}.ToJson ();
					mac = Convert.ToBase64String (macCalulator.ComputeHash (UTF8Encoding.UTF8.GetBytes (macMessage)));
				}

				try {
					service.Post (new PostChallengeActions {
						Id = 1,
						ChallengeId = challengeId,
						ActionId = actionId,
						Nonce = nonce,
						Mac = mac
					});
					Assert.Fail ("Posting an action with a challenge not available to the user should not be possible.");
				} catch (HttpError e) {
					Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.Forbidden));
					Assert.That (e.ErrorCode, Is.EqualTo (ChallengeActionsService.PostChallengeActionErrorCode.ChallengeNotActiveForUser.ToString ()));
				}
			}
		}

		[Test ()]
		[Ignore ("Ignored until timezone used when posting an action is fixed")]
		public void GetChallengeAction ()
		{
			int userId = 1;
			byte[] key = new byte[]{ 0x01, 0x02, 0x03 };
			UInt64 nonce = (UInt64)new Random ().Next (0, int.MaxValue);

			using (var service = new ChallengeActionsService { Request = new MockHttpRequest () }) {
				int challengeId = (int)service.Db.Insert (new Challenge { }, selectIdentity: true);

				int challengeSetId = (int)service.Db.Insert (new ChallengeSet {
					Challenges = new List<int> { challengeId }
				}, selectIdentity: true);

				int eventId = (int)service.Db.Insert (new Event {
				}, selectIdentity: true);

				service.Db.Insert (new EventChallengeSets {
					ChallengeSetId = challengeSetId,
					EventId = eventId,
					StartDate = DateTime.Now,
					EndDate = DateTime.Now.Add (new TimeSpan (1, 0, 0)),
				});

				service.Db.Insert (new EventUsers {
					EventId = eventId,
					UserId = userId,
				});

				int action1Id = (int)service.Db.Insert (new ChallengeAction {
					ChallengeId = challengeId,
					Key = Convert.ToBase64String (key),
					Description = "Action 1",
					LockChallegenge = true, 
					RemoveBonus = false,
					PointReduction = 50,
				}, selectIdentity: true);

				int action2Id = (int)service.Db.Insert (new ChallengeAction {
					ChallengeId = challengeId,
					Key = Convert.ToBase64String (key),
					Description = "Action 2",
					LockChallegenge = false, 
					RemoveBonus = true,
					PointReduction = 0,
				}, selectIdentity: true);

				//Read all actions, none should be available
				Assert.That (service.Get (new GetChallengeActions{ ChallengeId = challengeId }).Actions.Any (), Is.False);

				//trigger action1
				string mac;
				using (var macCalulator = new HMACSHA256 (key)) {
					string macMessage = new ChallengeActionsService.PostActionAuthenticationData () {
						ActionId = action1Id,
						ChallengeId = challengeId,
						Nonce = nonce,
						UserId = userId
					}.ToJson ();
					mac = Convert.ToBase64String (macCalulator.ComputeHash (UTF8Encoding.UTF8.GetBytes (macMessage)));
				}

				service.Post (new PostChallengeActions {
					Id = userId,
					ChallengeId = challengeId,
					ActionId = action1Id,
					Nonce = nonce,
					Mac = mac
				});
				DateTime action1Time = DateTime.Now;
				var actions = service.Get (new GetChallengeActions  { ChallengeId = challengeId }).Actions;
				Assert.That (actions.Count, Is.EqualTo (1));
				Assert.That (actions [0].Description, Is.EqualTo ("Action 1"));
				Assert.That (action1Time.Subtract (actions [0].AccessTime.FromEcmaScriptString ()).TotalMilliseconds, Is.LessThan (500));
				Assert.That (actions [0].LockChallenge, Is.True);
				Assert.That (actions [0].RemoveBonus, Is.False);
				Assert.That (actions [0].PointReduction, Is.EqualTo (50));

				//trigger action2
				System.Threading.Thread.Sleep (1000);
				nonce = (UInt64)new Random ().Next (0, int.MaxValue);
				using (var macCalulator = new HMACSHA256 (key)) {
					string macMessage = new ChallengeActionsService.PostActionAuthenticationData () {
						ActionId = action2Id,
						ChallengeId = challengeId,
						Nonce = nonce,
						UserId = userId
					}.ToJson ();
					mac = Convert.ToBase64String (macCalulator.ComputeHash (UTF8Encoding.UTF8.GetBytes (macMessage)));
				}

				service.Post (new PostChallengeActions {
					Id = userId,
					ChallengeId = challengeId,
					ActionId = action2Id,
					Nonce = nonce,
					Mac = mac
				});
				DateTime action2Time = DateTime.Now;
				actions = service.Get (new GetChallengeActions  { ChallengeId = challengeId }).Actions;
				Assert.That (actions.Count, Is.EqualTo (2));
				var action1 = actions.Where (a => a.Description == "Action 1").First ();
				var action2 = actions.Where (a => a.Description == "Action 2").First ();
				Assert.That (action1Time.Subtract (action1.AccessTime.FromEcmaScriptString ()).TotalMilliseconds, Is.LessThan (500));
				Assert.That (action1.LockChallenge, Is.True);
				Assert.That (action1.RemoveBonus, Is.False);
				Assert.That (action1.PointReduction, Is.EqualTo (50));
				Assert.That (action2Time.Subtract (action2.AccessTime.FromEcmaScriptString ()).TotalMilliseconds, Is.LessThan (500));
				Assert.That (action2.LockChallenge, Is.False);
				Assert.That (action2.RemoveBonus, Is.True);
				Assert.That (action2.PointReduction, Is.EqualTo (0));

			}
		}

		[Test ()]
		public void GetChallengeActionMultipleUsers ()
		{
			int userId = 1;
			byte[] key = new byte[]{ 0x01, 0x02, 0x03 };
			UInt64 nonce = (UInt64)new Random ().Next (0, int.MaxValue);

			using (var service = new ChallengeActionsService { Request = new MockHttpRequest () }) {
				int otherUserId = (int)service.Db.Insert (new User (), true);
				int challengeId = (int)service.Db.Insert (new Challenge { }, selectIdentity: true);

				int challengeSetId = (int)service.Db.Insert (new ChallengeSet {
					Challenges = new List<int> { challengeId }
				}, selectIdentity: true);

				int eventId = (int)service.Db.Insert (new Event {
				}, selectIdentity: true);

				service.Db.Insert (new EventChallengeSets {
					ChallengeSetId = challengeSetId,
					EventId = eventId,
					StartDate = DateTime.Now,
					EndDate = DateTime.Now.Add (new TimeSpan (1, 0, 0)),
				});

				service.Db.Insert (new EventUsers {
					EventId = eventId,
					UserId = userId,
				});

				service.Db.Insert (new EventUsers {
					EventId = eventId,
					UserId = otherUserId,
				});

				int action1Id = (int)service.Db.Insert (new ChallengeAction {
					ChallengeId = challengeId,
					Key = Convert.ToBase64String (key),
					Description = "Action 1",
					LockChallegenge = true, 
					RemoveBonus = false,
					PointReduction = 50,
				}, selectIdentity: true);

				int action2Id = (int)service.Db.Insert (new ChallengeAction {
					ChallengeId = challengeId,
					Key = Convert.ToBase64String (key),
					Description = "Action 2",
					LockChallegenge = false, 
					RemoveBonus = true,
					PointReduction = 0,
				}, selectIdentity: true);

				//Read all actions, none should be available
				Assert.That (service.Get (new GetChallengeActions{ ChallengeId = challengeId }).Actions.Any (), Is.False);

				//trigger action1
				string mac;
				using (var macCalulator = new HMACSHA256 (key)) {
					string macMessage = new ChallengeActionsService.PostActionAuthenticationData () {
						ActionId = action1Id,
						ChallengeId = challengeId,
						Nonce = nonce,
						UserId = userId
					}.ToJson ();
					mac = Convert.ToBase64String (macCalulator.ComputeHash (UTF8Encoding.UTF8.GetBytes (macMessage)));
				}

				service.Post (new PostChallengeActions {
					Id = userId,
					ChallengeId = challengeId,
					ActionId = action1Id,
					Nonce = nonce,
					Mac = mac
				});
				DateTime action1Time = DateTime.Now;
				var actions = service.Get (new GetChallengeActions  { ChallengeId = challengeId }).Actions;
				Assert.That (actions.Count, Is.EqualTo (1));
				Assert.That (actions [0].Description, Is.EqualTo ("Action 1"));


				//trigger action2 for other User
				service.Db.Insert (new ChallengeActionAccess {
					ChallengeActionId = action2Id,
					Nonce = 42,
					UserId = otherUserId
				});
				//We should still only get action1 for the logged in user (userId)
				actions = service.Get (new GetChallengeActions  { ChallengeId = challengeId }).Actions;
				Assert.That (actions.Count, Is.EqualTo (1));
				Assert.That (actions [0].Description, Is.EqualTo ("Action 1"));

			}
		}

		[Test ()]
		public void GetChallengeActionDifferentChallenge ()
		{
			int userId = 1;
			byte[] key = new byte[]{ 0x01, 0x02, 0x03 };
			UInt64 nonce = (UInt64)new Random ().Next (0, int.MaxValue);

			using (var service = new ChallengeActionsService { Request = new MockHttpRequest () }) {
				int challenge1Id = (int)service.Db.Insert (new Challenge { }, selectIdentity: true);
				int challenge2Id = (int)service.Db.Insert (new Challenge { }, selectIdentity: true);

				int challengeSetId = (int)service.Db.Insert (new ChallengeSet {
					Challenges = new List<int> { challenge1Id, challenge2Id }
				}, selectIdentity: true);

				int eventId = (int)service.Db.Insert (new Event {
				}, selectIdentity: true);

				service.Db.Insert (new EventChallengeSets {
					ChallengeSetId = challengeSetId,
					EventId = eventId,
					StartDate = DateTime.Now,
					EndDate = DateTime.Now.Add (new TimeSpan (1, 0, 0)),
				});

				service.Db.Insert (new EventUsers {
					EventId = eventId,
					UserId = userId,
				});

				int action1Id = (int)service.Db.Insert (new ChallengeAction {
					ChallengeId = challenge1Id,
					Key = Convert.ToBase64String (key),
					Description = "Action 1",
					LockChallegenge = true, 
					RemoveBonus = false,
					PointReduction = 50,
				}, selectIdentity: true);

				int action2Id = (int)service.Db.Insert (new ChallengeAction {
					ChallengeId = challenge2Id,
					Key = Convert.ToBase64String (key),
					Description = "Action 2",
					LockChallegenge = false, 
					RemoveBonus = true,
					PointReduction = 0,
				}, selectIdentity: true);

				//trigger action1
				string mac;
				using (var macCalulator = new HMACSHA256 (key)) {
					string macMessage = new ChallengeActionsService.PostActionAuthenticationData () {
						ActionId = action1Id,
						ChallengeId = challenge1Id,
						Nonce = nonce,
						UserId = userId
					}.ToJson ();
					mac = Convert.ToBase64String (macCalulator.ComputeHash (UTF8Encoding.UTF8.GetBytes (macMessage)));
				}

				service.Post (new PostChallengeActions {
					Id = userId,
					ChallengeId = challenge1Id,
					ActionId = action1Id,
					Nonce = nonce,
					Mac = mac
				});

				Assert.That (service.Get (new GetChallengeActions { ChallengeId = challenge2Id }).Actions.Any (), Is.False,
					"No Action should be stored for challenge2");
				
				//trigger action2
				nonce = (UInt64)new Random ().Next (0, int.MaxValue);
				using (var macCalulator = new HMACSHA256 (key)) {
					string macMessage = new ChallengeActionsService.PostActionAuthenticationData () {
						ActionId = action2Id,
						ChallengeId = challenge2Id,
						Nonce = nonce,
						UserId = userId
					}.ToJson ();
					mac = Convert.ToBase64String (macCalulator.ComputeHash (UTF8Encoding.UTF8.GetBytes (macMessage)));
				}

				service.Post (new PostChallengeActions {
					Id = userId,
					ChallengeId = challenge2Id,
					ActionId = action2Id,
					Nonce = nonce,
					Mac = mac
				});
				var actions = service.Get (new GetChallengeActions  { ChallengeId = challenge2Id }).Actions;
				Assert.That (actions.Count, Is.EqualTo (1));
				Assert.That (actions.Where (a => a.Description == "Action 2").Any (), Is.True, "Check that the right action is returned");
			}
		}

	}
}

