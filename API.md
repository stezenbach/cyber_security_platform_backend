:ballot_box_with_check: GET /challenges/{id}

    Response: {"id","title", "BasePoints", "SolutionType", "SolutionInfo"}

    SolutionType specifies the way a solution has to submitted (see POST /challenges/{id}/solution)
    SolutionInfo may hold some additional information about how to solve the question, depending on the SolutionType
    SolutionType.Static: SolutionInfo is empty
    SolutionType.MultipleChoice: SolutionInfo holds a JSON encoded List<string> with the text for the multiple choice questions
    SolutionType.MultiPart: SolutionInfo probably holds a JSON encoded Dictionary<string,string>, but the backend doen't really care


:ballot_box_with_check: GET /challenges/{id}/description

    Response: {"description"}

    Tracks the time at which the user saw the challegenge for the first time.
    Will return an error if the challenge is not in a currently running event of the user.
    The string `{{{UserToken-abcdef}}}` is replaced by BASE64(HMAC-SHA256(K, {{{UserToken-abcdef}})), where the key K is configured in web.config ("UserTokenKey"). The characters /+= are removed from the token
    The string `{{{UserIdToken-abcdef}}` is replaced by UrlEncode(BASE64(AES256-ECB(K, userId) || HMAC-SHA56(K, encryptedId))) where K = HMAC-SHA256({{{UserIdToken-abcdef}}, UTF8(challengeId)) and PKCS#7 padding is used.


:ballot_box_with_check: GET /challenges/{id}/attachments

    Response: {"title", "mimeType", "content"}

:ballot_box_with_check: GET /challenges/{id}/hints

    Response: [hintId]

:ballot_box_with_check: GET /challenges/{id}/hints/{hint-number}

    Response: {"hintNumber", "text"}

GET /challenges/{id}/hints/{hintnumber}/pointreduction

    Response: {"Points"}

:ballot_box_with_check: : GET /challenges/{id}/hints/{hint-number}/attachments

     Response: {"title", "mimeType", "content"}

:ballot_box_with_check: POST /challenges/{id}/solution

    Request: {"SolutionType", "Solution"}
    Response: Empty

    The SolutionType holds a value of that enum (see below) which must match the SolutionType of the challenge.
    *Solution* holds a string whose format is determined by the SolutionType
    SolutionType.Static: Solution is just a plain string holding the Solution
    SolutionType.MultipleChoice: Solution holds a JSON encoded List<bool> holding the checked answers. You have only one try to submit a solution of this type.
                                 Number of elements in the list must be equal to the expected number of solutions (HttpError.Conflict otwerwise)
    SolutionType.MultiPart: Solution holds a JSON encoded Dictionary<string, string>> holding the solution identifiers (keys) and the corresponding answers (values). You have only one try to submit a solution of this type.
                            Number of elements in the list must be equal to the expected number of solutions and the identifiers have to match that of the correct solution (HttpError.Conflict otwerwise)

:ballot_box_with_check: GET /challenges/{id}/solution

    Response: {"SolutionType", "Solution", "SolutionExplanation"}

    Will return *Forbidden*, if the challenge is not available to the user and it has not expired, yet.
    Access will be tracked.
    The string returned in *Solution* will have the same format as the one to be posted when submitting a solution.

:ballot_box_with_check: GET /challenges/{id}/solution/entered

    Response: {"SolutionType", "SolutionEntered"}

    Returns the last solution entered by the user.
    Will return *Forbidden*, if the challenge is not available to the user.

:ballot_box_with_check: GET /challenges/{id}/solution/mayenter

        Response: {"MayEnterSolution", "Solved", "Locked"}

        Returns if a new solution for this challenge may be entered by the current user.

:ballot_box_with_check: GET /challengesets

    Request: {"challengeId"}
    Response: ["id"]
    If challengeId is set on the request, only challengesets which contain that challenge are returned

:ballot_box_with_check: GET /challengesets/{id}

    Response: {"id", "title", "description", "challenges": ["challengeId"]}

:ballot_box_with_check: GET /events

    Request: {"ChallengeId", "ChallengeSetId"}
    Response: ["id"]
    If ChallengeId or ChallengeSetId is set on the request, only events which contain that Challenge/ChallengeSet are returned

:ballot_box_with_check: GET /events/{id}

    Request: {"OnlyCurrent", "OnlyCurrentAndPast"}
    Response: {"id", "title", "description", "challengesets": [ {"challengesetId", "startDate", "endDate"} ] }
    If OnlyCurrent is set to TRUE, only currently active ChallengeSets are returned
    If OnlyCurrentAndPast is set to TRUE, on currently acitve or expired ChallengeSets are returned.

:ballot_box_with_check: GET /events/{id}/users

    Response: ["UserId"]

Only if the user is part of the event

:ballot_box_with_check: GET /events/{id}/ranking

    Response: [{"UserId", "UserName", "Rank", "Points"}]

:ballot_box_with_check: GET /users/{id}

    Response: {"id","username", "firstName", "lastName", "avatar"}

:ballot_box_with_check: POST /user/password

    Request: {"OldPassword", "NewPassword"}
    Return:  {}, 401 if the old password is not correct

:ballot_box_with_check: :unlock:  POST /users/createresetcode

    Request: {"MailAddress"}
    Response: Empty, sends an e-mail with a reset code to the user. Does not return an error in case of an errornous mail address

:ballot_box_with_check: :unlock: POST /users/resetpassword

    Request: {"MailAddress", "ResetCode", "NewPassword"}
    Response: Empty, Returns always *Forbidden* in case of an error


:ballot_box_with_check: :unlock:  GET /users/{userId}/challenges/{challengeId}

    Response:  {"AccessDate", "solvedDate", "solved", "tryCount", [{"hintId", "accessTime"}], "BasePoints"}

    Checks, if the  user logged in shares an event with the queried user.

:ballot_box_with_check: :unlock:  GET /users/{userId}/challenges/{challengeId}/points

    Request: {"ForceCalculation"}
    Response: {"Points"}

:ballot_box_with_check: :unlock:  GET /users/{userId}/challenges/{challengeId}/pointsifsolvednow

    Request: {"Count"}
    Response: [{"Time", "Points"}]

    Returns an array conatining some points in time (starting with now) and the points the user would get if he would solve the challenge at that time.
    The size of this array is determined by *Count* and defaults to 3.

:ballot_box_with_check: POST /users/{userId}/challenges/{challengeId}/actions/{actionId}

        Request: {"Nonce", "Mac"}
        Response: Empty

        Tries to execute the specified *action* for the selected user. The request must be authenticated my a MAC, whose key is stored in the database.
        Any action must only be triggered once per user.
        The value of *Mac* needs to be BASE64(HMAC-SHA256(Key, {"UserId", "ChallengeId", "ActionId", "Nonce"})), where the message is a JSON encoded type.
        Nonce is a unique unsigned 64 bit value.
        In case of an error, the *ResponseStatus -> ErrorCode* field of the response DTO is popoulated with a specific error code. The currently defined error codes are: ActionAlreadyTriggered,	BadChallengeIdForAction, NonceReuse, ChallengeNotActiveForUser, MacValidationFailed

:ballot_box_with_check: GET /user

    Response: {"id","username", "firstName", "lastName", "avatar"}

:ballot_box_with_check: GET /user/events

    Response: ["eventId"]

:ballot_box_with_check: GET /user/challenges/{challengeId}/actions/

            Response: {"Actions": ["Description", "LockChallenge", "AccessTime", "RemoveBonus", "PointReduction"]}

            Returns information about all actions triggered for this user for this challenge.

:ballot_box_with_check: PUT /user

    Request: {"FirstName", "LastName", "Avatar"}
    Send null, if you wish not to change a value. Empty values for FirstName and LastName are not allowed. Send an empty value for Avatar to delete it. Avatar must satisfy the RegEx "^data:image/[a-z]+;base64, [a-zA-Z0-9\+/=\s]*$"



:ballot_box_with_check: :unlock: POST /auth/credentials

    Request: {"UserName", "Password"} ("UserName" has to contain the e-mail address of the user. Beware: Urlencoding, @=%40)
    Return:  {"SessionId", "UserName", "DisplayName", "ResponseStatus"}

    Performs login for the user, password sent in the clear


:ballot_box_with_check: POST /auth/logout

    Request: {}
    Response: Empty

    Performs a logout operation
